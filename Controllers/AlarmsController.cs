﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProtoMES.Controllers.Models;
using ProtoMES.Models;
using System.Xml;
using System.Linq;
using System.IO;
using Microsoft.ML.Trainers;

namespace ProtoMES.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlarmsController : ControllerBase
    {
        public AlarmsController()
        {
        }

        [HttpGet("GetAlarms/{tags}/{datefrom}/{dateto}")]
        public async Task<IActionResult> GetAlarms([ModelBinder(BinderType = typeof(CustomArrayModelBinder))] string[] tags,
                [FromRoute] DateTime dateFrom, [FromRoute] DateTime dateTo)
        {
            Dictionary<string, List<Models.Alarm>> ret = new Dictionary<string, List<Models.Alarm>>();
            ret = Historian.Instance.GetAlarms(tags, dateFrom, dateTo);

            foreach(string tag in ret.Keys)
            {
                foreach(Models.Alarm alarm in ret[tag])
                {
                    if(alarm.type.Equals("Lo"))
                    {
                        alarm.message = $"{alarm.comment} {alarm.value} ниже предельного значения {alarm.limit}";
                    }
                    else if (alarm.type.Equals("Hi"))
                    {
                        alarm.message = $"{alarm.comment} {alarm.value} выше предельного значения {alarm.limit}";
                    }
                    else { }
                }
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (ret == null)
            {
                return NotFound();
            }

            List<Models.Alarm> ret2 = new List<Models.Alarm>();
            foreach (List<Models.Alarm> alarms in ret.Values)
            {
                ret2.AddRange(alarms);
            }
            return Ok(ret2);

        }

        [HttpGet("GetAlarms3/{date}")]
        public async Task<IActionResult> GetAlarms3([FromRoute] DateTime date)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            List<SMSMessage> ret = new List<SMSMessage>();
            try
            {
                string file = @"Y:\\sms_a.txt";
                if (System.IO.File.Exists(file))
                {
                    using (StreamReader sr = new StreamReader(file))
                    {
                        string line = null;
                        while ((line = sr.ReadLine()) != null)
                        {
                            if(line.Contains("+79123113244 "))
                            {
                                string temp = line.Substring(17);
                                string dt = temp.Substring(0, 10);
                                string tm = temp.Substring(13, 10);
                                tm = tm.Replace(" ", "");
                                tm = tm.Replace("-", "");
                                string msg = temp.Substring(23);

                                string[] d = dt.Split(".");
                                string[] t = tm.Split(":");
                                DateTime time = new DateTime(System.Convert.ToInt32(d[2]),
                                                            System.Convert.ToInt32(d[1]),
                                                            System.Convert.ToInt32(d[0]),
                                                            System.Convert.ToInt32(t[0]),
                                                            System.Convert.ToInt32(t[1]),
                                                            System.Convert.ToInt32(t[2]));
                                SMSMessage message = new SMSMessage();
                                message.eventTime = time;
                                message.message = msg;
                                if(date.Equals(time.Date))
                                    ret.Add(message);
                            }
                        }
                    }
                    return Ok(ret);
                }
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetAlarms2/{date}")]
        public async Task<IActionResult> GetAlarms2([FromRoute] DateTime date)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            List<SMSMessage> ret = new List<SMSMessage>();
            try
            {
                Dictionary<DateTime, List<Models.Alarm>> results = new Dictionary<DateTime, List<Models.Alarm>>();
                DateTime dateFrom = date.Date;
                DateTime dateTo = date.Date.AddDays(1);
                results = Historian.Instance.GetAlarms2(dateFrom, dateTo);

                Dictionary<DateTime, List<Models.AckAlarm>> results2 = new Dictionary<DateTime, List<Models.AckAlarm>>();
                results2 = ZincDBExt.Instance.GetAckAlarms(dateFrom, dateTo);

                foreach (DateTime dt in results2.Keys)
                {
                    foreach (Models.AckAlarm alarm in results2[dt])
                    {
                        SMSMessage sms = new SMSMessage() { eventTime = alarm.messageAt, message = alarm.message,
                            ack = alarm.ack, ackAt = alarm.ackAt, ackBy = alarm.ackBy, translate = ZincDBExt.Instance.TranslateMessage(alarm.message) };
                        ret.Add(sms);
                    }
                }
                foreach (DateTime dt in results.Keys)
                {
                    foreach (Models.Alarm alarm in results[dt])
                    {
                        bool exists = ret.Where(x => x.eventTime == alarm.eventTime && x.message == alarm.comment).ToList().Count() > 0 ? true : false;
                        if (!exists)
                        {
                            SMSMessage sms = new SMSMessage() { eventTime = alarm.eventTime, message = alarm.comment, translate = ZincDBExt.Instance.TranslateMessage(alarm.comment) };
                            ret.Add(sms);
                        }
                    }
                }
                
                if (ret.Count == 0)
                    return NotFound();
                else return Ok(ret.OrderBy(x => x.eventTime));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("SetAcknowledgement/{message}/{date}")]
        public async Task<IActionResult> SetAcknowledgement([FromRoute] string message, [FromRoute] DateTime date)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                string curUser = HttpContext.User.Identity.Name;
                ZincDBExt.Instance.SetAcknowledgment(message, date, DateTime.Now, curUser);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
