﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProtoMES.Controllers.Models;
using ProtoMES.Models;
using System.Xml;
using GalaxySvcRef;
using OptionsSvcRef;
using PredictionSvcRef;
using System.ServiceModel;

namespace ProtoMES.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParametersController : ControllerBase
    {
        //private GalaxySvcClient grSvc = new GalaxySvcClient();
        //private OptionsSvcClient opsSvc = new OptionsSvcClient();
        //private PredictionSvcClient predSvc = new PredictionSvcClient();

        public ParametersController()
        {
        }

        [HttpGet("GetParametersByArea/{id}")]
        public async Task<IActionResult> GetParametersByArea([FromRoute] string id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            GalaxySvcClient grSvc = null;

            try
            {
                grSvc = new GalaxySvcClient();

                grSvc.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential("ZN-WWSP-01\\Administrator", "Qwerty1!");
                var parameters = await grSvc.GetParametersAsync(id);

                if (parameters == null)
                    return NotFound();
                return Ok(parameters);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            finally
            {
                if (grSvc != null)
                {
                    if (grSvc.State == CommunicationState.Faulted)
                        grSvc.Abort();
                    else
                        grSvc.Close();
                }
            }
        }

        /*[HttpGet("GetTags/{tags}")]
        public async Task<IActionResult> GetTags([FromQuery] string[] tags)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var parameters = await grSvc.GetParametersAsync(tags[0]);

            if (parameters == null)
                return NotFound();
            return Ok(parameters);
        }*/

        // POST: api/Parameters
        [HttpPost("CreateOptionsParameter")]
        public async Task<IActionResult> CreateOptionsParameter([FromBody]OptionsParameter param)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            /*OptionsSvcRef.MxDataType typ = OptionsSvcRef.MxDataType.MxString;
            if (param.type == "Integer")
                typ = OptionsSvcRef.MxDataType.MxInteger;
            else if(param.type == "Float")
                typ = OptionsSvcRef.MxDataType.MxFloat;
            else if (param.type == "Double")
                typ = OptionsSvcRef.MxDataType.MxDouble;*/
            ArchestrA.GRAccess.MxDataType typ = ArchestrA.GRAccess.MxDataType.MxString;
            if (param.type == "Integer")
                typ = ArchestrA.GRAccess.MxDataType.MxInteger;
            else if (param.type == "Float")
                typ = ArchestrA.GRAccess.MxDataType.MxFloat;
            else if (param.type == "Double")
                typ = ArchestrA.GRAccess.MxDataType.MxDouble;

            OptionsSvcClient opsSvc = null;
            try
            {
                opsSvc = new OptionsSvcClient();
                opsSvc.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential("ZN-WWSP-01\\Administrator", "Qwerty1!");

                await opsSvc.CreateParameterAsync(param.objName, param.paramName, param.paramDesc, typ, param.values[0], param.engUnits, param.trendHigh, param.trendLow, param.values);
                
                return CreatedAtAction("CreateOptionsParameter", new { id = param.objName }, $"{param.objName}.{param.paramName}");
            }
            catch (Exception e)
            {
                if(e.InnerException != null)
                    return NotFound($"Exception : {e.Message}, InnerException: {e.InnerException.Message}");
                else return NotFound($"Exception : {e.Message}");
            }
            finally
            {
                if (opsSvc != null)
                {
                    if (opsSvc.State == CommunicationState.Faulted)
                        opsSvc.Abort();
                    else
                        opsSvc.Close();
                }
            }
        }

        /*[HttpPost("CreateOptionsParameter")]
        public async Task<IActionResult> CreateOptionsParameter([FromBody]string objName, [FromBody]string paramName, [FromBody]string paramDesc,
            [FromBody]string type, [FromBody]string engUnits, [FromBody]string trendHigh, [FromBody]string trendLow, [FromBody]List<string> values)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            OptionsSvcRef.MxDataType typ = OptionsSvcRef.MxDataType.MxString;
            if (type == "Integer")
                typ = OptionsSvcRef.MxDataType.MxInteger;
            else if (type == "Float")
                typ = OptionsSvcRef.MxDataType.MxFloat;
            else if (type == "Double")
                typ = OptionsSvcRef.MxDataType.MxDouble;
            await opsSvc.CreateParameterAsync(objName, paramName, paramDesc, typ, values[0], engUnits, trendHigh, trendLow, values);

            return CreatedAtAction("CreateOptionsParameter", new { id = objName }, $"{objName}.{paramName}");
        }*/

        [HttpGet("GetOptionsParameter/{objName}/{paramName}")]
        public async Task<IActionResult> GetOptionsParameter([FromRoute] string objName, [FromRoute] string paramName)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            OptionsSvcClient opsSvc = null;
            try
            {
                opsSvc = new OptionsSvcClient();
                opsSvc.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential("ZN-WWSP-01\\Administrator", "Qwerty1!");

                var parameter = await opsSvc.GetOptionsAttrAsync(objName, paramName);

                if (parameter == null)
                    return NotFound();
                return Ok(parameter);
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                    return BadRequest($"Exception : {e.Message}, InnerException: {e.InnerException.Message}");
                else return BadRequest($"Exception : {e.Message}");
            }
            finally
            {
                if (opsSvc != null)
                {
                    if (opsSvc.State == CommunicationState.Faulted)
                        opsSvc.Abort();
                    else
                        opsSvc.Close();
                }
            }
        }

        [HttpGet("GetEngUnits")]
        public async Task<IActionResult> GetEngUnits()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            OptionsSvcClient opsSvc = null;
            try
            {
                opsSvc = new OptionsSvcClient();
                opsSvc.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential("ZN-WWSP-01\\Administrator", "Qwerty1!");

                var parameter = await opsSvc.GetEngUnitsAsync();

                if (parameter == null)
                    return NotFound();
                return Ok(parameter);
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                    return BadRequest($"Exception : {e.Message}, InnerException: {e.InnerException.Message}");
                else return BadRequest($"Exception : {e.Message}");
            }
            finally
            {
                if (opsSvc != null)
                {
                    if (opsSvc.State == CommunicationState.Faulted)
                        opsSvc.Abort();
                    else
                        opsSvc.Close();
                }
            }
        }

        [HttpGet("GetPredictValues/{tags}/{datefrom}/{dateto}/{steps}")]
        public async Task<IActionResult> GetPredictValues([ModelBinder(BinderType = typeof(CustomArrayModelBinder))] string[] tags,
            [FromRoute] DateTime dateFrom, [FromRoute] DateTime dateTo, [FromRoute] int steps)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            PredictionSvcClient predSvc = null;
            try
            {
                predSvc = new PredictionSvcClient();
                predSvc.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential("ZN-WWSP-01\\Administrator", "Qwerty1!");

                var values = await predSvc.PredictValuesAsync(tags.ToList(), dateFrom, dateTo, steps);
                //var values = Prediction.Instance.PredictValues(tags.ToList(), dateFrom, dateTo, 30);

                if (values != null)
                {
                    Dictionary<DateTime, List<double>> dic = values as Dictionary<DateTime, List<double>>;
                }

                if (values == null)
                    return NotFound();
                return Ok(values);
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                    return BadRequest($"Exception : {e.Message}, InnerException: {e.InnerException.Message}");
                else return BadRequest($"Exception : {e.Message}");
            }
            finally
            {
                if (predSvc != null)
                {
                    if (predSvc.State == CommunicationState.Faulted)
                        predSvc.Abort();
                    else
                        predSvc.Close();
                }
            }
        }

        [HttpGet]
        public async Task<IActionResult> TestSecurity()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            GalaxySvcClient grSvc = null;

            try
            {
                grSvc = new GalaxySvcClient();

                string curUser = HttpContext.User.Identity.Name;
                var ret = await grSvc.TestAsync();

                if (ret == null)
                    return NotFound();
                return Ok(ret);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            finally
            {
                if (grSvc != null)
                {
                    if (grSvc.State == CommunicationState.Faulted)
                        grSvc.Abort();
                    else
                        grSvc.Close();
                }
            }

        }

        [HttpPost("CreateOptionsValue")]
        public async Task<IActionResult> CreateOptionsValue([FromBody]StringTag parameter)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            OptionsSvcClient opsSvc = null;
            try
            {
                opsSvc = new OptionsSvcClient();
                opsSvc.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential("ZN-WWSP-01\\Administrator", "Qwerty1!");

                List<string> parts = parameter.Split();

                foreach (string part in parts)
                {
                    opsSvc.SetOptionsValueAsync(parameter.name, "", part).Wait();
                    System.Threading.Thread.Sleep(1000);
                }

                return CreatedAtAction("CreateOptionsValue", new { id = parameter.name }, $"{parameter.name}");
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                    return NotFound($"Exception : {e.Message}, InnerException: {e.InnerException.Message}");
                else return NotFound($"Exception : {e.Message}");
            }
            finally
            {
                if (opsSvc != null)
                {
                    if (opsSvc.State == CommunicationState.Faulted)
                        opsSvc.Abort();
                    else
                        opsSvc.Close();
                }
            }
        }

        [HttpGet("SetTagValue/{tagname}/{value}")]
        public async Task<IActionResult> SetTagValue([FromRoute] string tagname, [FromRoute] string value)
        {
            /*if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                MXServer.Instance.SetValue(tagname, value);
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Ошибка изменения значения тега {0}, {1}", tagname, e.Message));
            }
            return Ok();*/
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            OptionsSvcClient opsSvc = null;
            try
            {
                opsSvc = new OptionsSvcClient();
                opsSvc.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential("ZN-WWSP-01\\Administrator", "Qwerty1!");

                string[] parts = tagname.Split('.');
                opsSvc.SetOptionsValueAsync(parts[0], parts[1], value).Wait();

                return CreatedAtAction("SetTagValue", new { id = tagname }, $"{value}");
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                    return NotFound($"Exception : {e.Message}, InnerException: {e.InnerException.Message}");
                else return NotFound($"Exception : {e.Message}");
            }
            finally
            {
                if (opsSvc != null)
                {
                    if (opsSvc.State == CommunicationState.Faulted)
                        opsSvc.Abort();
                    else
                        opsSvc.Close();
                }
            }

        }

        [HttpGet("SetTagValueDT/{tagname}/{value}/{timestamp}")]
        public async Task<IActionResult> SetTagValueDT([FromRoute] string tagname, [FromRoute] string value, [FromRoute] DateTime timestamp)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            OptionsSvcClient opsSvc = null;
            try
            {
                opsSvc = new OptionsSvcClient();
                opsSvc.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential("ZN-WWSP-01\\Administrator", "Qwerty1!");

                string[] parts = tagname.Split('.');
                opsSvc.SetTagValueDTAsync(parts[0], parts[1], value, timestamp).Wait();

                return CreatedAtAction("SetTagValue", new { id = tagname }, $"{value}");
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                    return NotFound($"Exception : {e.Message}, InnerException: {e.InnerException.Message}");
                else return NotFound($"Exception : {e.Message}");
            }
            finally
            {
                if (opsSvc != null)
                {
                    if (opsSvc.State == CommunicationState.Faulted)
                        opsSvc.Abort();
                    else
                        opsSvc.Close();
                }
            }
        }

    }
}