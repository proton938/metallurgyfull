﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProtoMES.Controllers.Models;
using ProtoMES.Models;
using GalaxySvcRef;
using System.Security.Principal;
using ArchestrA.GRAccess;
using System.ServiceModel;

namespace ProtoMES.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EquipmentsController : ControllerBase
    {
        //private GalaxySvcClient grSvc = new GalaxySvcClient();

        public EquipmentsController()
        {
        }

        [HttpGet("GetAreas")]
        public async Task<IActionResult> GetAreas()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //string curUser = HttpContext.User.Identity.Name;
            GalaxySvcClient grSvc = null;

            try
            {
                grSvc = new GalaxySvcClient();

                grSvc.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential("ZN-WWSP-01\\Administrator", "Qwerty1!");
                var equipment = await grSvc.GetAreasAsync();

                if (equipment == null)
                    return NotFound();
                return Ok(equipment);
            }
            catch (Exception e)
            {
                return BadRequest($"Ошибка в GetAreas, {e.Message}");
            }
            finally
            {
                if (grSvc != null)
                {
                    if (grSvc.State == CommunicationState.Faulted)
                        grSvc.Abort();
                    else
                        grSvc.Close();
                }
            }
        }
    }
}