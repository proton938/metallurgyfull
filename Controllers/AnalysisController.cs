﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProtoMES.Controllers.Models;
using ProtoMES.Models;
using System.Xml;
using System.IO;

namespace ProtoMES.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnalysisController : ControllerBase
    {
        public AnalysisController()
        {
        }

        [HttpGet("GetSampleNames")]
        public async Task<IActionResult> GetSampleNames()
        {
            Dictionary<int, string> ret = new Dictionary<int, string>();
            ret = Analysis.Instance.GetSampleNames();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (ret == null)
            {
                return NotFound();
            }

            return Ok(ret);
        }

        [HttpGet("GetSampleNames2/{datefrom}/{dateto}")]
        public async Task<IActionResult> GetSampleNames2([FromRoute] DateTime dateFrom, [FromRoute] DateTime dateTo)
        {
            Dictionary<int, string> ret = new Dictionary<int, string>();
            ret = Analysis.Instance.GetSampleNames2(dateFrom, dateTo);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (ret == null)
            {
                return NotFound();
            }

            return Ok(ret);
        }

        [HttpGet("GetSamples/{code}/{datefrom}/{dateto}")]
        public async Task<IActionResult> GetSamples([FromRoute] int code, [FromRoute] DateTime dateFrom, [FromRoute] DateTime dateTo)
        {
            List<Sample> ret = new List<Sample>();
            ret = Analysis.Instance.GetSamples(code, dateFrom, dateTo);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (ret == null)
            {
                return NotFound();
            }

            return Ok(ret);
        }

        /*[HttpGet("GetSamples2/{code}/{datefrom}/{dateto}")]
        public async Task<IActionResult> GetSamples2([FromRoute] int code, [FromRoute] DateTime dateFrom, [FromRoute] DateTime dateTo)
        {
            List<Sample> ret = new List<Sample>();
            ret = Analysis.Instance.GetSamples(code, dateFrom, dateTo);

            List<DateTime> stamps = ret.Where(x => x.date != null)
                .Select(x => new DateTime(x.date.Value.Year, x.date.Value.Month, x.date.Value.Day))
                .Distinct()
                .ToList();

            Dictionary<int, SampleResult> results = new Dictionary<int, SampleResult>();
            foreach(Sample sample in ret)
            {
                if (!results.ContainsKey(sample.id))
                {
                    SampleResult sampleRes = new SampleResult();
                    sampleRes.id = sample.id;
                    sampleRes.name = sample.name;
                    sampleRes.results = new Dictionary<string, ChemResult>();
                    ChemResult res = new ChemResult()
                    {
                        name = sample.comp,
                        min = sample.min,
                        max = sample.max,
                        eu = sample.eu,
                        values = new Dictionary<DateTime, double?> { { sample.date.Value.Date, sample.val } },
                        shift = sample.shift,
                        furnance = sample.furnance
                    };
                    string key = $"{sample.comp}_{sample.shift}_{sample.furnance}";
                    sampleRes.results.Add(key, res);
                    results.Add(sample.id, sampleRes);
                }
                else
                {
                    SampleResult sampleRes = results[sample.id];
                    string key = $"{sample.comp}_{sample.shift}_{sample.furnance}";
                    if (!sampleRes.results.ContainsKey(key))
                    {
                        ChemResult res = new ChemResult()
                        {
                            name = sample.comp,
                            min = sample.min,
                            max = sample.max,
                            eu = sample.eu,
                            values = new Dictionary<DateTime, double?> { { sample.date.Value.Date, sample.val } },
                            shift = sample.shift,
                            furnance = sample.furnance
                        };
                        //string key = $"{sample.comp}_{sample.shift}_{sample.furnance}";
                        sampleRes.results.Add(key, res);
                    }
                    else
                    {
                        //string key = $"{sample.comp}_{sample.shift}_{sample.furnance}";
                        ChemResult res = sampleRes.results[key];
                        res.values.Add(sample.date.Value.Date, sample.val);
                    }

                }
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (ret == null)
            {
                return NotFound();
            }

            return Ok(results);
        }*/

        [HttpGet("GetCompValues/{sample}/{comp}/{shift}/{furnance}/{datefrom}/{dateto}")]
        public async Task<IActionResult> GetCompValues([FromRoute] int sample, [FromRoute] string comp,
                                                        [FromRoute] int shift, [FromRoute] int furnance,
                                                        [FromRoute] DateTime dateFrom, [FromRoute] DateTime dateTo)
        {
            List<Tag> ret = new List<Tag>();
            string component = comp.Replace("&gt;", ">");//Uri.UnescapeDataString(comp);
            component = component.Replace("&lt;", "<");
            var dic = Analysis.Instance.GetCompValues(sample, component, shift, furnance, dateFrom, dateTo);
            foreach(KeyValuePair<DateTime, double> pair in dic)
            {
                ret.Add(new Tag() { dt = pair.Key, val = pair.Value });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (ret == null)
            {
                return NotFound();
            }

            return Ok(ret);
        }

        [HttpGet("GetCompValues2/{code}/{datefrom}/{dateto}")]
        public async Task<IActionResult> GetCompValues2([FromRoute] int code, [FromRoute] DateTime dateFrom, [FromRoute] DateTime dateTo)
        {
            List<Sample> ret = new List<Sample>();
            ret = Analysis.Instance.GetSamples(code, dateFrom, dateTo);

            List<DateTime> stamps = ret.Where(x => x.date != null)
                .Select(x => new DateTime(x.date.Value.Year, x.date.Value.Month, x.date.Value.Day))
                .Distinct()
                .OrderBy(x => x.Date)
                .ToList();

            Dictionary<int, SampleResult> results = new Dictionary<int, SampleResult>();
            foreach (Sample sample in ret)
            {
                if (!results.ContainsKey(sample.id))
                {
                    SampleResult sampleRes = new SampleResult();
                    sampleRes.id = sample.id;
                    sampleRes.name = sample.name;
                    sampleRes.results = new Dictionary<string, ChemResult>();
                    ChemResult res = new ChemResult()
                    {
                        name = sample.comp,
                        min = sample.min,
                        max = sample.max,
                        eu = sample.eu,
                        //values = new Dictionary<DateTime, double?> { { sample.date.Value.Date, sample.val } },
                        values = new List<Tag2>(),// { { new Tag() { dt = sample.date.Value.Date, val = sample.val.Value } } },
                        shift = sample.shift,
                        furnance = sample.furnance
                    };
                    foreach(DateTime dt in stamps)
                    {
                        res.values.Add(new Tag2() { dt = dt, val = null });
                    }
                    res.values.Where(x => x.dt == sample.date.Value.Date).FirstOrDefault().val = sample.val.Value;
                    string key = $"{sample.comp}_{sample.shift}_{sample.furnance}";
                    sampleRes.results.Add(key, res);
                    results.Add(sample.id, sampleRes);
                }
                else
                {
                    SampleResult sampleRes = results[sample.id];
                    string key = $"{sample.comp}_{sample.shift}_{sample.furnance}";
                    if (!sampleRes.results.ContainsKey(key))
                    {
                        ChemResult res = new ChemResult()
                        {
                            name = sample.comp,
                            min = sample.min,
                            max = sample.max,
                            eu = sample.eu,
                            //values = new Dictionary<DateTime, double?> { { sample.date.Value.Date, sample.val } },
                            values = new List<Tag2>(),// { { new Tag() { dt = sample.date.Value.Date, val = sample.val.Value } } },
                            shift = sample.shift,
                            furnance = sample.furnance
                        };
                        foreach (DateTime dt in stamps)
                        {
                            res.values.Add(new Tag2() { dt = dt, val = null });
                        }
                        res.values.Where(x => x.dt == sample.date.Value.Date).FirstOrDefault().val = sample.val.Value;
                        //string key = $"{sample.comp}_{sample.shift}_{sample.furnance}";
                        sampleRes.results.Add(key, res);
                    }
                    else
                    {
                        //string key = $"{sample.comp}_{sample.shift}_{sample.furnance}";
                        ChemResult res = sampleRes.results[key];
                        //res.values.Add(sample.date.Value.Date, sample.val);
                        //res.values.Add(new Tag() { dt = sample.date.Value.Date, val = sample.val.Value });
                        res.values.Where(x => x.dt == sample.date.Value.Date).FirstOrDefault().val = sample.val.Value;
                    }

                }
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (ret == null)
            {
                return NotFound();
            }

            List<ChemResult> ret_vals = new List<ChemResult>();
            foreach (SampleResult r in results.Values)
            {
                foreach(List<Tag2> t in r.results.Values.Select(x => x.values).ToList())
                {
                    InterpolateArray(t);
                }
                /*for(int i = 0; i < r.results.Values.Select(x => x.values).Count(); i++)
                {
                    List<Tag2> t = r.results.Values.Select(x => x.values).ToArray()[i];
                    InterpolateArray(t);
                }*/
                ret_vals.AddRange(r.results.Values.ToList());
            }

            return Ok(ret_vals);
        }

        private void InterpolateArray(List<Tag2> t)
        {
            try
            {
                for (int i = 0; i < t.Count(); i++)
                {
                    IEnumerable<double?> q1 = t.Skip(i).TakeWhile(x => x.val != null).Select(z => z.val);
                    int c1 = q1.Count();
                    if (c1 > 0)
                    {
                        IEnumerable<double?> q2 = t.Skip(i + c1).TakeWhile(x => x.val == null).Select(z => z.val);
                        int c2 = q2.Count();
                        if (t.Count() != i + c1 + c2)
                        {
                            for (int j = 0; j < c2; j++)
                            {
                                t[i + c1 + j].val = t[i + c1 - 1].val + (j + 1) * (t[i + c1 + c2].val - t[i + c1 - 1].val) / (c2 + 1);
                            }
                        }
                        else
                        {
                            for (int k = 0; k < c2; k++)
                            {
                                double? delta = t[i + c1 - 1].val - t[i + c1 - 2].val;
                                t[i + c1 + k].val = t[i + c1 + k - 1].val + delta;
                            }
                            break;
                        }
                    }
                }
                if (t[0].val == null)
                {
                    IEnumerable<double?> q3 = t.TakeWhile(x => x.val == null).Select(z => z.val);
                    int c3 = q3.Count();
                    for (int k = c3; k > 0; k--)
                    {
                        double? delta = t[k + 1].val - t[k].val;
                        t[k - 1].val = t[k].val - delta;
                    }
                }
            }
            catch (Exception e)
            {

            }
        }

        [HttpGet("GetAnalysisTemplates")]
        public async Task<IActionResult> GetAnalysisTemplates()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                List<Models.AnalysisTemplate> templates = ZincDBExt.Instance.GetAnalysisTemplates();

                return Ok(templates);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("UpdateAnalysisTemplate")]
        public async Task<IActionResult> UpdateAnalysisTemplate([FromBody]Models.AnalysisTemplate template)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                ZincDBExt.Instance.UpdateAnalysisTemplate(template, HttpContext.User.Identity.Name);
                return CreatedAtAction("UpdateAnalysisTemplate", new { id = template.name });
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                    return NotFound($"Exception : {e.Message}, InnerException: {e.InnerException.Message}");
                else return NotFound($"Exception : {e.Message}");
            }
            finally
            {
            }
        }

        [HttpPost("DeleteAnalysisTemplate")]
        public async Task<IActionResult> DeleteAnalysisTemplate([FromBody]Models.AnalysisTemplate template)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                string user = HttpContext.User.Identity.Name;
                //if (user != template.createdBy)
                //    throw new Exception($"Пользователь {user} не может удалить {template.name}");
                ZincDBExt.Instance.DeleteAnalysisTemplate(template, user);
                return CreatedAtAction("DeleteAnalysisTemplate", new { id = template.name });
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                    return NotFound($"Exception : {e.Message}, InnerException: {e.InnerException.Message}");
                else return NotFound($"Exception : {e.Message}");
            }
            finally
            {
            }
        }

        [HttpGet("GetCompValuesToExcel/{code}/{datefrom}/{dateto}")]
        public async Task<IActionResult> GetCompValuesToExcel([FromRoute] int code, [FromRoute] DateTime dateFrom, [FromRoute] DateTime dateTo)
        {
            List<Sample> ret = new List<Sample>();
            ret = Analysis.Instance.GetSamples(code, dateFrom, dateTo);

            List<DateTime> stamps = ret.Where(x => x.date != null)
                .Select(x => new DateTime(x.date.Value.Year, x.date.Value.Month, x.date.Value.Day))
                .Distinct()
                .OrderBy(x => x.Date)
                .ToList();

            Dictionary<int, SampleResult> results = new Dictionary<int, SampleResult>();
            foreach (Sample sample in ret)
            {
                if (!results.ContainsKey(sample.id))
                {
                    SampleResult sampleRes = new SampleResult();
                    sampleRes.id = sample.id;
                    sampleRes.name = sample.name;
                    sampleRes.results = new Dictionary<string, ChemResult>();
                    ChemResult res = new ChemResult()
                    {
                        name = sample.comp,
                        min = sample.min,
                        max = sample.max,
                        eu = sample.eu,
                        values = new List<Tag2>(),
                        shift = sample.shift,
                        furnance = sample.furnance
                    };
                    foreach (DateTime dt in stamps)
                    {
                        res.values.Add(new Tag2() { dt = dt, val = null });
                    }
                    res.values.Where(x => x.dt == sample.date.Value.Date).FirstOrDefault().val = sample.val.Value;
                    string key = $"{sample.comp}_{sample.shift}_{sample.furnance}";
                    sampleRes.results.Add(key, res);
                    results.Add(sample.id, sampleRes);
                }
                else
                {
                    SampleResult sampleRes = results[sample.id];
                    string key = $"{sample.comp}_{sample.shift}_{sample.furnance}";
                    if (!sampleRes.results.ContainsKey(key))
                    {
                        ChemResult res = new ChemResult()
                        {
                            name = sample.comp,
                            min = sample.min,
                            max = sample.max,
                            eu = sample.eu,
                            values = new List<Tag2>(),
                            shift = sample.shift,
                            furnance = sample.furnance
                        };
                        foreach (DateTime dt in stamps)
                        {
                            res.values.Add(new Tag2() { dt = dt, val = null });
                        }
                        res.values.Where(x => x.dt == sample.date.Value.Date).FirstOrDefault().val = sample.val.Value;
                        sampleRes.results.Add(key, res);
                    }
                    else
                    {
                        ChemResult res = sampleRes.results[key];
                        res.values.Where(x => x.dt == sample.date.Value.Date).FirstOrDefault().val = sample.val.Value;
                    }

                }
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (ret == null)
            {
                return NotFound();
            }

            List<ChemResult> ret_vals = new List<ChemResult>();
            foreach (SampleResult r in results.Values)
            {
                foreach (List<Tag2> t in r.results.Values.Select(x => x.values).ToList())
                {
                    InterpolateArray(t);
                }
                ret_vals.AddRange(r.results.Values.ToList());
            }

            //string XlsContentType = "application/vnd.ms-excel";
            string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            Stream ms = ExportToExcel.Instance.AnalysisToExcel(ret_vals, dateFrom, dateTo);
            return File(ms, XlsxContentType, "export.xlsx");
        }
    }
}
