﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.ML;
using Microsoft.ML.Data;

namespace ProtoMES.Controllers
{
    public class DataPoint : ICloneable
    {
        [LoadColumn(0)]
        [ColumnName("TimeStamp")]
        public string TimeStamp { get; set; }

        #region present time
        [LoadColumn(1)]
        [ColumnName("X1")]
        public float X1 { get; set; }

        [LoadColumn(2)]
        [ColumnName("X2")]
        public float X2 { get; set; }

        [LoadColumn(3)]
        [ColumnName("X3")]
        public float X3 { get; set; }

        [LoadColumn(4)]
        [ColumnName("X4")]
        public float X4 { get; set; }

        [LoadColumn(5)]
        [ColumnName("X5")]
        public float X5 { get; set; }

        [LoadColumn(6)]
        [ColumnName("X6")]
        public float X6 { get; set; }
        #endregion

        #region preset - 1 lag
        [LoadColumn(7)]
        [ColumnName("X11")]
        public float X11 { get; set; }

        [LoadColumn(8)]
        [ColumnName("X21")]
        public float X21 { get; set; }

        [LoadColumn(9)]
        [ColumnName("X31")]
        public float X31 { get; set; }

        [LoadColumn(10)]
        [ColumnName("X41")]
        public float X41 { get; set; }

        [LoadColumn(11)]
        [ColumnName("X51")]
        public float X51 { get; set; }

        [LoadColumn(12)]
        [ColumnName("X61")]
        public float X61 { get; set; }
        #endregion

        #region preset - 2 lags
        [LoadColumn(13)]
        [ColumnName("X12")]
        public float X12 { get; set; }

        [LoadColumn(14)]
        [ColumnName("X22")]
        public float X22 { get; set; }

        [LoadColumn(15)]
        [ColumnName("X32")]
        public float X32 { get; set; }

        [LoadColumn(16)]
        [ColumnName("X42")]
        public float X42 { get; set; }

        [LoadColumn(17)]
        [ColumnName("X52")]
        public float X52 { get; set; }

        [LoadColumn(18)]
        [ColumnName("X62")]
        public float X62 { get; set; }
        #endregion

        #region preset - 3 lags
        [LoadColumn(19)]
        [ColumnName("X13")]
        public float X13 { get; set; }

        [LoadColumn(20)]
        [ColumnName("X23")]
        public float X23 { get; set; }

        [LoadColumn(21)]
        [ColumnName("X33")]
        public float X33 { get; set; }

        [LoadColumn(22)]
        [ColumnName("X43")]
        public float X43 { get; set; }

        [LoadColumn(23)]
        [ColumnName("X53")]
        public float X53 { get; set; }

        [LoadColumn(24)]
        [ColumnName("X63")]
        public float X63 { get; set; }
        #endregion

        #region preset - 4 lags
        [LoadColumn(25)]
        [ColumnName("X14")]
        public float X14 { get; set; }

        [LoadColumn(26)]
        [ColumnName("X24")]
        public float X24 { get; set; }

        [LoadColumn(27)]
        [ColumnName("X34")]
        public float X34 { get; set; }

        [LoadColumn(28)]
        [ColumnName("X44")]
        public float X44 { get; set; }

        [LoadColumn(29)]
        [ColumnName("X54")]
        public float X54 { get; set; }

        [LoadColumn(30)]
        [ColumnName("X64")]
        public float X64 { get; set; }
        #endregion

        #region preset - 5 lags
        [LoadColumn(31)]
        [ColumnName("X15")]
        public float X15 { get; set; }

        [LoadColumn(32)]
        [ColumnName("X25")]
        public float X25 { get; set; }

        [LoadColumn(33)]
        [ColumnName("X35")]
        public float X35 { get; set; }

        [LoadColumn(34)]
        [ColumnName("X45")]
        public float X45 { get; set; }

        [LoadColumn(35)]
        [ColumnName("X55")]
        public float X55 { get; set; }

        [LoadColumn(36)]
        [ColumnName("X65")]
        public float X65 { get; set; }
        #endregion

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class ModelOutput
    {
        [ColumnName("Score")] public float y { get; set; }
    }
    public class Prediction : Singleton<Prediction>
    {
        private static readonly object _loc_session = new object();

        private Prediction()
        {
        }

        private List<string> models = new List<string>() { "X1", "X2", "X3", "X4", "X5", "X6" };
        private List<string> parameters = new List<string>() { "X1", "X2", "X3", "X4", "X5", "X6",
                                                               "X11", "X21", "X31", "X41", "X51", "X61",
                                                               "X12", "X22", "X32", "X42", "X52", "X62",
                                                               "X13", "X23", "X33", "X43", "X53", "X63",
                                                               "X14", "X24", "X34", "X44", "X54", "X64",
                                                               "X15", "X25", "X35", "X45", "X55", "X65" };
        private Dictionary<string, PredictionEngine<DataPoint, ModelOutput>> predEngines =
            new Dictionary<string, PredictionEngine<DataPoint, ModelOutput>>();

        public Dictionary<DateTime, List<double>> PredictValues(List<string> inParams, DateTime from, DateTime to, int steps)
        {
            try
            {
                List<Models.Tag> tags = Historian.Instance.GetHistoryValuesForPrediction(inParams.ToArray(), from, to);
                if (tags.Count == 0)
                    throw new Exception($"Не найдены теги");

                var results = tags.GroupBy(
                    p => p.dt,
                    (key, g) => new { TimeStamp = key, Tags = g.ToList() });

                List<DataPoint> data = results
                    .Select(e => new DataPoint
                    {
                        TimeStamp = e.TimeStamp.ToString(),
                        X1 = e.Tags.Count > 0 ? Convert.ToSingle(e.Tags[0].val, System.Globalization.CultureInfo.InvariantCulture) : 0,
                        X2 = e.Tags.Count > 1 ? Convert.ToSingle(e.Tags[1].val, System.Globalization.CultureInfo.InvariantCulture) : 0,
                        X3 = e.Tags.Count > 2 ? Convert.ToSingle(e.Tags[2].val, System.Globalization.CultureInfo.InvariantCulture) : 0,
                        X4 = e.Tags.Count > 3 ? Convert.ToSingle(e.Tags[3].val, System.Globalization.CultureInfo.InvariantCulture) : 0,
                        X5 = e.Tags.Count > 4 ? Convert.ToSingle(e.Tags[4].val, System.Globalization.CultureInfo.InvariantCulture) : 0,
                        X6 = e.Tags.Count > 5 ? Convert.ToSingle(e.Tags[5].val, System.Globalization.CultureInfo.InvariantCulture) : 0
                    }).ToList();

                for (int i = data.Count - 1; i > 5; i--)
                {
                    data[i].X11 = data[i - 1].X1;
                    data[i].X21 = data[i - 1].X2;
                    data[i].X31 = data[i - 1].X3;
                    data[i].X41 = data[i - 1].X4;
                    data[i].X51 = data[i - 1].X5;
                    data[i].X61 = data[i - 1].X6;

                    data[i].X12 = data[i - 2].X1;
                    data[i].X22 = data[i - 2].X2;
                    data[i].X32 = data[i - 2].X3;
                    data[i].X42 = data[i - 2].X4;
                    data[i].X52 = data[i - 2].X5;
                    data[i].X62 = data[i - 2].X6;

                    data[i].X13 = data[i - 3].X1;
                    data[i].X23 = data[i - 3].X2;
                    data[i].X33 = data[i - 3].X3;
                    data[i].X43 = data[i - 3].X4;
                    data[i].X53 = data[i - 3].X5;
                    data[i].X63 = data[i - 3].X6;

                    data[i].X14 = data[i - 4].X1;
                    data[i].X24 = data[i - 4].X2;
                    data[i].X34 = data[i - 4].X3;
                    data[i].X44 = data[i - 4].X4;
                    data[i].X54 = data[i - 4].X5;
                    data[i].X64 = data[i - 4].X6;

                    data[i].X15 = data[i - 5].X1;
                    data[i].X25 = data[i - 5].X2;
                    data[i].X35 = data[i - 5].X3;
                    data[i].X45 = data[i - 5].X4;
                    data[i].X55 = data[i - 5].X5;
                    data[i].X65 = data[i - 5].X6;
                }

                List<Task> tasks = new List<Task>();
                foreach (string model in models)
                {
                    CreatePredictionEngine(model, data);
                    //Task task = new Task(() => CreatePredictionEngine(model, data), TaskCreationOptions.LongRunning);
                    //tasks.Add(task);
                    //task.Start();
                }
                //Task.WaitAll(tasks.ToArray());

                for (int j = 0; j < steps; j++)
                {
                    DataPoint newData = new DataPoint();
                    DataPoint sampleData = data.Last();
                    newData = (DataPoint)sampleData.Clone();
                    newData.TimeStamp = null;
                    foreach (string model in models)
                    {
                        PredictionEngine<DataPoint, ModelOutput> engine = predEngines[model];
                        ModelOutput result = engine.Predict(sampleData);
                        switch (model)
                        {
                            case "X1":
                                newData.X1 = result.y;
                                newData.X11 = sampleData.X1;
                                newData.X12 = sampleData.X11;
                                newData.X13 = sampleData.X12;
                                newData.X14 = sampleData.X13;
                                newData.X15 = sampleData.X14;
                                break;
                            case "X2":
                                newData.X2 = result.y;
                                newData.X21 = sampleData.X2;
                                newData.X22 = sampleData.X21;
                                newData.X23 = sampleData.X22;
                                newData.X24 = sampleData.X23;
                                newData.X25 = sampleData.X24;
                                break;
                            case "X3":
                                newData.X3 = result.y;
                                newData.X31 = sampleData.X3;
                                newData.X32 = sampleData.X31;
                                newData.X33 = sampleData.X32;
                                newData.X34 = sampleData.X33;
                                newData.X35 = sampleData.X34;
                                break;
                            case "X4":
                                newData.X4 = result.y;
                                newData.X41 = sampleData.X4;
                                newData.X42 = sampleData.X41;
                                newData.X43 = sampleData.X42;
                                newData.X44 = sampleData.X43;
                                newData.X45 = sampleData.X44;
                                break;
                            case "X5":
                                newData.X5 = result.y;
                                newData.X51 = sampleData.X5;
                                newData.X52 = sampleData.X51;
                                newData.X53 = sampleData.X52;
                                newData.X54 = sampleData.X53;
                                newData.X55 = sampleData.X54;
                                break;
                            case "X6":
                                newData.X6 = result.y;
                                newData.X61 = sampleData.X6;
                                newData.X62 = sampleData.X61;
                                newData.X63 = sampleData.X62;
                                newData.X64 = sampleData.X63;
                                newData.X65 = sampleData.X64;
                                break;
                        }
                    }
                    DateTime dt = Convert.ToDateTime(sampleData.TimeStamp).AddSeconds(100);
                    newData.TimeStamp = dt.ToString("yyyy-MM-dd HH:mm:ss");
                    data.Add(newData);
                }

                DateTime last = default(DateTime);
                Dictionary<DateTime, List<double>> ret = new Dictionary<DateTime, List<double>>();
                foreach (DataPoint dp in data)
                {
                    List<double> values = new List<double>();
                    values.Add(dp.X1);
                    values.Add(dp.X2);
                    values.Add(dp.X3);
                    values.Add(dp.X4);
                    values.Add(dp.X5);
                    values.Add(dp.X6);

                    //if (dp.TimeStamp == null)
                    //    dp.TimeStamp = last.AddSeconds(100).ToString("yyyy-MM-dd HH:mm:ss");

                    //last = Convert.ToDateTime(dp.TimeStamp);//, System.Globalization.CultureInfo.InvariantCulture

                    if (!ret.ContainsKey(Convert.ToDateTime(dp.TimeStamp)))
                        ret.Add(Convert.ToDateTime(dp.TimeStamp), values);
                    else ret.Add(DateTime.Now, values);
                }
                return ret;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private void CreatePredictionEngine(string param, List<DataPoint> data)
        {
            List<string> inParameters = new List<string>(parameters);
            inParameters.Remove(param);

            MLContext context = new MLContext(seed: 1);
            IDataView dv = context.Data.LoadFromEnumerable(data);

            IEstimator<ITransformer> pipeline = BuildTrainingPipelineWithParam(context, inParameters, param);

            //Evaluate2(context, dv, pipeline, param);
            //var crossValidationResults = mlContext.Regression.CrossValidate(trainingDataView, trainingPipeline, numberOfFolds: 5, labelColumnName: param);

            ITransformer model = TrainModel(context, dv, pipeline);

            //DataPoint sampleData = data.Last();

            var predEngine = context.Model.CreatePredictionEngine<DataPoint, ModelOutput>(model);
            if (!predEngines.ContainsKey(param))
                predEngines.Add(param, predEngine);
            else predEngines[param] = predEngine;
        }

        private IEstimator<ITransformer> BuildTrainingPipelineWithParam(MLContext mlContext, List<string> inParams, string outParam)
        {
            var dataProcessPipeline = mlContext.Transforms.Text.FeaturizeText("stamp", "TimeStamp")
                                      .Append(mlContext.Transforms.Concatenate("Features", inParams.ToArray()));
            var trainer = mlContext.Regression.Trainers.FastForest/*.LightGbm*/(labelColumnName: outParam, featureColumnName: "Features");
            var trainingPipeline = dataProcessPipeline.Append(trainer);

            return trainingPipeline;
        }

        private ITransformer TrainModel(MLContext mlContext, IDataView trainingDataView, IEstimator<ITransformer> trainingPipeline)
        {
            ITransformer model = trainingPipeline.Fit(trainingDataView);
            return model;
        }
    }
}
