﻿using System;
using Microsoft.AspNetCore.Mvc;
using ProtoMES.Controllers.Models;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using ReportsSvcRef;
using System.Threading.Tasks;
using System.Linq;
using System.IO;

namespace ProtoMES.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        //private ReportsSvcClient repSvc = new ReportsSvcClient();

        /*private static string[] Summaries = new[]
        {
            "ЖУРНАЛ ВЫПУСКА и ОТГРУЗКИ",
            "ТЕХНОЛОГИЧЕСКИЙ ЖУРНАЛ",
            "Рапорт Обжигового цеха",
            "Рапорт Выщелачивательного цеха",
            "Рапорт Сернокислотного цеха",
            "Рапорт Вельц-цеха",
            "Рапорт Электролиза",
            "Рапорт КЭЦ ПО",
            "Рапорт Гидрометаллургического цеха",
            "Энергоресурсы"
        };*/

        // GET: api/Reports
        /*[HttpGet("[action]")]
        public IEnumerable<ProtoMES.Controllers.Models.Report> Reports()
        {
            int i = 0;
            return Enumerable.Range(0, 9).Select(index => new Report
            {
                name = Summaries[index],
                id = (++i).ToString(),//System.Guid.NewGuid().ToString()
                date = DateTime.Now.Date.ToString(),
                shift = 1
            });
        }*/

        [HttpGet("Reports")]
        public async Task<IActionResult> GetReports()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            ReportsSvcClient repSvc = null;
            try
            {
                string curUser = HttpContext.User.Identity.Name;
                //WindowsIdentity currentUser = WindowsIdentity.GetCurrent();
                //string s = currentUser.Name;
                //var user = await grSvc.TestAsync();

                repSvc = new ReportsSvcClient();
                repSvc.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential("ZN-WWSP-01\\Administrator", "Qwerty1!");
                var reports = await repSvc.GetReportsFolderAsync(@"\");

                if (reports == null)
                    return NotFound();

                List<Models.Report> ret = new List<Models.Report>();
                foreach (ReportsSvcRef.Report report in reports)
                {
                    if (report.type == "Report")
                    {
                        ret.Add(
                                        new Models.Report
                                        {
                                            name = report.name,
                                            id = report.id,
                                            url = $"http://zn-wwsp-02/Reports/Pages/Report.aspx?ItemPath={report.path}",
                                            description = report.description,
                                        });
                    }
                }
                return Ok(ret);
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                    return BadRequest($"Exception : {e.Message}, InnerException: {e.InnerException.Message}");
                else return BadRequest($"Exception : {e.Message}");
            }
            finally
            {
                if (repSvc != null)
                {
                    if (repSvc.State == CommunicationState.Faulted)
                        repSvc.Abort();
                    else
                        repSvc.Close();
                }
            }
        }

        [HttpGet("Reports2")]
        public async Task<IActionResult> GetReports2()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            ReportsSvcClient repSvc = null;
            try
            {
                repSvc = new ReportsSvcClient();

                repSvc.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential("ZN-WWSP-01\\Administrator", "Qwerty1!");
                var reports = await repSvc.GetReportsFolderAsync(@"\");

                if (reports == null)
                    return NotFound();

                //List<Models.Report> ret = new List<Models.Report>();
                List<Models.Folder> ret = new List<Models.Folder>();
                foreach (ReportsSvcRef.Report report in reports.Where(x => x.type == "Folder"))
                {
                    ret.Add(
                                    new Models.Folder
                                    {
                                        name = report.name,
                                        id = report.id,
                                        url = $"http://zn-wwsp-02/Reports/Pages/Report.aspx?ItemPath={report.path}",
                                        description = report.description,
                                    });
                }
                foreach (ReportsSvcRef.Report report in reports.Where(x => x.type == "Report"))
                {
                    var a = report.path.Split(@"/");
                    Folder f = ret.Where(x => x.name == report.path.Split(@"/")[1]).FirstOrDefault();
                    if (f.reports == null)
                        f.reports = new List<Models.Report>();
                    f.reports.Add(
                                    new Models.Report
                                    {
                                        name = report.name,
                                        id = report.id,
                                        url = $"http://zn-wwsp-02/Reports/Pages/Report.aspx?ItemPath={report.path}",
                                        description = report.description,
                                    });
                }
                return Ok(ret);
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                    return BadRequest($"Exception : {e.Message}, InnerException: {e.InnerException.Message}");
                else return BadRequest($"Exception : {e.Message}");
            }
            finally
            {
                if (repSvc != null)
                {
                    if (repSvc.State == CommunicationState.Faulted)
                        repSvc.Abort();
                    else
                        repSvc.Close();
                }
            }
        }

        // GET: api/Reports/5
        /*[HttpGet("{id}", Name = "GetReport")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Reports
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Reports/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/

        [HttpGet("ShopReports/{shop}/{datefrom}/{dateto}")]
        public async Task<IActionResult> GetShopReports([FromRoute] string shop, [FromRoute] DateTime dateFrom, [FromRoute] DateTime dateTo)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            List<ShopReport> reports = new List<ShopReport>();

            reports = Historian.Instance.GetShopReports(shop, dateFrom, dateTo);

            if (reports == null)
                return NotFound();

            return Ok(reports);
        }

        [HttpGet("GetTemplateReport/{shop}")]
        public async Task<IActionResult> GetTemplateReport([FromRoute] string shop)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                TemplateReport ret = new TemplateReport();
                switch(shop)
                {
                    case "1":
                        ret.text = "<p><span>H<label style='font - size:0.6em; vertical - align:-0.5em'>2</label>SO<label style='font - size:0.6em; vertical - align:-0.5em'>4</label></span>За смену поступило:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>8</strong> п/в Учалы;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4 п/в к.м. (Губаха);</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>5</strong> п/в Актюбинск; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 14 ц.</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>3</strong> п/в Орск;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>S 20 ед.</strong></p><p style='text - align:justify;'>За смену отгружено:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5 ц&nbsp;H2SO4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>S 5 ед.</strong></p><p>За смену МП:&nbsp;2 п/в СОК (в/ц 102,3);1 п/в + 1 д к.м. (1в/ц 26,15, 1шихт 54,15);</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1 п/в + 1 д СОК (ГМЦ → шихт 100,15); 1 п/в ПСЦМ (шихт 44,7);</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1 п/в К/окись (в/ц 73,25);</p><p><strong>Грузы:&nbsp;&nbsp; 23</strong> п/в Учалы (р/д);&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6 п/в к.м. (2зач, 4р/д);</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>17</strong> п/в Актюбинск (4зач, 4скл, 9р/д);&nbsp;&nbsp;&nbsp; 2 п/в Святогор (стр.склад);</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>5</strong> п/в Сибай Башмедь (р/д);&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2 п/в сода (1стр.склад, 1р/д);</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>4</strong> п/в Сибай Башзолото (1зач, 3скл);&nbsp;&nbsp;&nbsp;&nbsp; 1 п/в К/окись (стр.склад).</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>3</strong> п/в Орск (брус);</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>2</strong> п/в Акжал (скл);&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>S 65 ед.</strong></p><p><i><strong>Всего подвижного состава:</strong></i><strong> </strong><i><strong>223 ед</strong></i>. (3 в, 128 п/в, 79 ц, 13 пл.)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i>За смену сдали: <strong>10 ед.</strong></i></p><p style='text - align:justify;'>Под&nbsp;Zn вагонов: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + груженые: &nbsp;&nbsp;&nbsp;&nbsp; <strong>2&nbsp;</strong>в&nbsp;ZnAl + 1 в&nbsp;HgSe шлам.</p><p style='text - align:justify;'>Под погрузку п/в: &nbsp; 19&nbsp;&nbsp; + груженые:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6 п/в&nbsp;Pb кек, 2 п/в&nbsp;ZnSO4, 3 п/в&nbsp;CuCl кек, 2 п/в клинкер.</p><p>Под H2SO4: 65 ц + 4 пл. т/к&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + налив: 1 ц + налитые: 8 ц (II сорт) + 1 ц (по документам).&nbsp;</p><p style='text - align:justify;'>Подход: на ст. «Электростанция» 15 ед.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>S 15 ед.</strong></p>";
                        break;
                    case "2":
                        ret.text = "<p style='text - align:justify;'><strong>&sum;Электролиз:&nbsp;</strong>нагрузка по заданию (по наличию ОР):&nbsp;</p><p style='text - align:justify;'>с 1900 - <strong>240 </strong>(120-120) <strong>кА;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>с 000 - <strong>250&nbsp;</strong>(125-125) <strong>кА.</strong></p><p style='text - align:justify;'>По циркуляции, по качеству Znкат замечаний нет<i>&nbsp;</i>(«Sb» ~ 40 мл/подъем; катодов на растворение за сдирку <strong>6</strong>). Ванны: А-43 на протоке, С-38 на ремонте. Продолжали сдирку&nbsp;IIй серии, на 600&nbsp;осталось сдирать 33% (~ <strong>8,8</strong> п/ч). Анодный кран и машина в работе. Почистили: ванны В-6, 7, 8, 9/А-14, 15, 16, 17; анодов: 142/204 на машине, 0/20 вручную, 0/20 заменили.</p><p>Мn шлама 188/282 м3. «К/з»: обнаружено: 0; устранено: 0; осталось:&nbsp;</p><p><strong>0</strong></p><p>.</p>";
                        break;
                    case "3":
                        ret.text = "<p style='text - align:justify;'><strong>ГМО</strong>: поток <strong>340-320-330&nbsp;</strong>м3/ч (по фильтрации&nbsp;Iй стадии). По качеству ВСС-6 замечаний нет. Фильтрация на&nbsp;I и&nbsp;II ст. на трех ф/п. По качеству очистки удовлетворительно. В работе ТК510А/В и ТК702А/В. ТК702С/D с о/э (~ 62/112 м3). Реактора в работе: ТК501А/В и ТК502А/В/С (ТК501С ~ 85 м3 о/э, промывка&nbsp;FP501х; ТК502D&nbsp;~ пустой, в резерве). В работе т/о Е501В. Гипс 20 м3. Б/м желтые на реакторах очистки.</p>";
                        break;
                    case "4":
                        ret.text = "<p style='text - align:justify;'><strong>Плавильное отд.</strong>: <strong>наплавлено:&nbsp;</strong>см.№<strong>3</strong> (сутки):<strong> 163,915&nbsp;</strong>(<strong>561,790</strong>)<strong> т</strong>.; см.№<strong>1</strong>: <strong>204,642 т</strong>.</p><p><strong>F801&nbsp;</strong>-<i>спл. ЦА04</i>;&nbsp;<strong>F802&nbsp;</strong>-<i>Zn чушковой</i>;&nbsp;<strong>F803</strong> -<i> &nbsp;спл.№5</i>. Распылительная: <span style='color:#323232;'>с 600 остановили обе линий распыления по заданию – на ремонт вентилятора К904А.</span></p>";
                        break;
                    case "5":
                        ret.text = "<p style='text - align:justify;'><strong>Cd</strong> <strong>отделение</strong>: нагрузка <strong>10,8&nbsp;</strong>(3,6-3,6-3,6) <strong>кА.</strong> Замечаний по работе нет.</p>";
                        break;
                    case "6":
                        ret.text = "<p style='text - align:justify;'><strong>Загружено:</strong> см. №<strong>3</strong> (сутки): <strong>343&nbsp;</strong>(<strong>1143</strong>) <strong>т</strong>.; см. №<strong>1</strong>:<strong> 428 т</strong>.</p><p>В работе печи <strong>КС-2, 3</strong>, <strong>5</strong> (через забрасыватели). Печи управляются удовлетворительно, режимы не меняли.&nbsp;Сделали по <i>две</i> донные выгрузки, огарок нормальный. Шихта: <i><strong>20%&nbsp;</strong></i>Актюбинск, <i><strong>20%&nbsp;</strong></i>Акжал,&nbsp;<i><strong>15%&nbsp;</strong></i>Учалы, <i><strong>10%</strong></i> Кр.Урал, <i><strong>10%</strong></i> Орск,&nbsp;<i><strong>10%</strong></i> Александринка, <i><strong>10%</strong></i> зачистки,<i><strong> 5%&nbsp;</strong></i>Сибай (р/д) + <i>1 гр.</i> ППФ. На газоочистке в работе секции №3, 5/6, 7. Разрежение в «чистом» коллекторе -30&nbsp;¸ -34 мм.&nbsp;Насосы исправны, циркуляция высокая/неравномерная (песков нет, на ситах пена). По складу: без замечаний (три крановщика). Выгрузка (см. №<strong>3</strong>): 3 п/в Кр.Урал, 2 п/в Акжал (порезали 68 МКР), 2 п/в Учалы, 1 п/в Актюбинск, 1 д Александринка, 1 д зачистки, занимались МП,(см. №<strong>1</strong>) по наличию места:&nbsp;4 п/в Сибай, 4 п/в Актюбинск, 1 д Александринка,&nbsp;занимались МП.</p>";
                        break;
                    case "7":
                        ret.text = "<p style='text - align:justify;'><strong>Загружено:</strong> см. №<strong>3</strong> (сутки): <strong>343&nbsp;</strong>(<strong>1143</strong>) <strong>т</strong>.; см. №<strong>1</strong>:<strong> 428 т</strong>.</p><p style='text - align:justify;'>В работе <strong>5</strong> нейтральных и<strong> 4</strong> кислых сгустителя.&nbsp;</p><p style='text - align:justify;'>Сг.№<strong>9</strong> в схеме сг. №<strong>6</strong>. Р-ра №6,10 с о/э (~ 80 м3 ), №7 с&nbsp;CuCl пульпой (~ 20 м3), ведут откачку&nbsp;CuCl пульпы в ГМЦ. Прием НР ограничивали по&nbsp;V ТК510х. На классификации в работе&nbsp;II «нитка» (песков 15 - 20 см, с классификатора ~ 2,0 т., пенообразование среднее). Распылительная в резерве. НЧ: работали через «Средний» и «Нейтр». «О/э» с ГВК + о/э + сбрасывали ВСНС (~ ↑7,2 м),&nbsp;),&nbsp;сбрасывали излишки ВСНС по&nbsp;V&nbsp;ТК510А/В.&nbsp;</p><p style='text - align:justify;'><i>«CuCl»</i>: установки с 1900 в работе, с 200 в резерве по&nbsp;V, операций: 6 + 2 на р-рах ГМЦ (б/м в норме).</p><p style='text - align:justify;'><i>«Карбонизация»</i>: <span style='color: black;'>ф/п № <strong>1</strong>, <strong>2</strong> в работе.&nbsp;</span>Циклов <strong>12</strong> + <strong>12</strong>&nbsp;Þ <strong>54,0&nbsp;</strong>т. (~ 2,0/2,5 т/ц).&nbsp;</p><p style='text - align:justify;'><i>«Флотация»</i>: поток 50 м3/ч. НСС-2 на промбак.</p><p style='text - align:justify;'><i>«Отмывка ВВ»</i>: циклов 5+5, приняли: 7,3 т. (переработали Владикавказ/Проминдустрия (мкр): <strong>0</strong>/<strong>0</strong>, с н.м. <strong>178</strong>/<strong>0</strong>).&nbsp;</p><p style='text - align:justify;'><i>«Ввод воды</i>»: воду нигде не дают, по цехам запрет.</p>";
                        break;
                    case "8":
                        ret.text = "<p><strong>Переработано, в т.ч. кек/ВЦС (т.): 452/363/81,5.</strong></p><p style='text - align:justify;'>В работе в/п № <strong>3, 4.&nbsp;</strong>Переработано<strong>&nbsp;</strong>Zn содержащего сырья: <strong>101&nbsp;</strong>т. (48+53), в т.ч. <strong>70</strong> т. кека, <strong>6&nbsp;</strong>т. промпродуктов; <strong>15&nbsp;</strong>т. К/окиси, <strong>10</strong> т. изгари, <strong>2</strong> т. доломит (получено окиси ~ <strong>30&nbsp;</strong>т.). Простой в/п №3 – 30 мин. чистка загрузочной трубы.&nbsp;</p><p style='text - align:justify;'><i><strong>РФГ</strong></i>: текущая в/о и с КВП-5,6 на прокалку. Через вибромельницу в выщ.цех подкул. возгоны подали 2 МКР (с н.м 26 МКР). <i><strong>ПСО</strong></i>:<i><strong>&nbsp;</strong></i>«В» ~ <strong>4</strong> м; «З» ~ <strong>1</strong> м.&nbsp;</p><p style='text - align:justify;'>&nbsp;</p><p><i><strong>Прокалка №1</strong></i>: переработано <strong>82</strong> т. (~7,0 т/ч), в ГМЦ откачали <strong>74&nbsp;</strong>т.&nbsp;</p><p><i><strong>Прокалка №2</strong></i>: переработано <strong>84&nbsp;</strong>т. (~ 7,0 т/ч). В ГМЦ откачали <strong>76&nbsp;</strong>т.</p><p>Переработали Владикавказ/Проминдустрия (т.): <strong>10</strong>/<strong>0</strong>; с н.м. <strong>859/0</strong>.</p><p style='text - align:justify;'><i><strong>Узел ВЦС</strong></i><strong>:</strong> растарили 15 мкр Святогора, циклов 13 (11,7 т.), остаток 144 мкр.</p><p style='text - align:justify;'>&nbsp;</p><p style='text - align:justify;'><i><strong>«Larox»</strong></i>: в работе <strong>2&nbsp;</strong>п/ф. Циклов <strong>42</strong> (21 + 21)&nbsp;Þ <strong>200&nbsp;</strong>т. (~ 4,76 т/ц). На КВП-5:<strong> 200&nbsp;</strong>т.&nbsp;</p><p>&nbsp;</p><p>УР - резерв, БГ- на чистке /ревизия ножей с утра/. Подача шихты ч/з питатель №32, прошихтовали: <strong>223</strong> т. кека, в т.ч. <strong>90&nbsp;</strong>т. из запаса;<strong> 15&nbsp;</strong>т. К/окиси (11/4),<strong> 15&nbsp;</strong>т. Святогор (8/7),<strong> 20&nbsp;</strong>т.Изгарь (14/6), <strong>5&nbsp;</strong>т. ПСЦМ (3/2),<strong> 5&nbsp;</strong>мкр. зола «СУМЗ» (~ 1,5 т.),<strong> 15&nbsp;</strong>т. доломит (7/8) + 10% нефтекокс (Ле Марк) от ТВС.&nbsp;Wшихты (с питателей 5/6)&nbsp;= 19,89/18,69%; 19,29/19,02%.</p>";
                        break;
                    case "9":
                        ret.text = "<p><i><strong>КВП-5:</strong></i> Переработано&nbsp;Zn содержащего сырья <strong>201&nbsp;</strong>т. (~ 19 т/ч по шихте). Получено окиси<strong> 60&nbsp;</strong>т. Простой по загрузке – 020&nbsp;чистка бункеров загрузки, загрузочной трубы. Материал в в/п удовлетворительный.&nbsp;t0 ~ 640 ÷ 700 0С; пар ~ 7,2 ÷ 10 т/ч. Оборот с бункеров на ТГ, окатали <strong>22&nbsp;</strong>т. МИО в работе (20 точек).</p><p>&nbsp;</p><p><i><strong>КВП-6:</strong></i> Переработано&nbsp;Zn содержащего сырья <span style='color:#0D0D0D;'><strong>150</strong></span><strong>&nbsp;</strong>т. (~ 19 т/ч&nbsp; по шихте). Получено окиси <span style='color:#0D0D0D;'><strong>45</strong></span><strong>&nbsp;</strong>т. Простой по загрузке – 030 чистка загрузочной трубы, шихтового бункера. Материал в в/п вязкий. t ~ 770 ÷ 880 0С, пар ~ 11,0 ÷ 17,0 т/ч. Оборот бункеров №1-10 на «Eirich» №1, (окатали 49 т.).&nbsp;Eirich №1: <strong>45</strong> ц (<strong>135&nbsp;</strong>т., в т. ч.&nbsp;Zn-содержащего сырья <strong>77&nbsp;</strong>т.&nbsp;W = 14,29 %). Простой 330 (уборка приямка, чистка затвора и короба скипа №3; чистка течек на конвейере №29; бункеров №4, 33.1; весового бункера). МИО в работе (52 точки). Скип №3 без замечаний. Чистка ширм К.-У.</p><p>Клинкера к отгрузке (смт): в/ц ~ 1 п/в (66); КВП-5/6 ~ 2 п/в (132).</p>";
                        break;
                    case "10":
                        ret.text = "<p style='text - align:justify;'><strong>Загрузили</strong>:<strong> 795 м3</strong>; <strong>откачали</strong>: <strong>795</strong> <strong>м3</strong>. По наличию в/о.</p><p style='text - align:justify;'>Принято непрок. в/о: 0,0 т. Установки с 1900 в работе, с 200 в резерве по&nbsp;V, (б/м в норме).&nbsp;</p><p style='text - align:justify;'><i><strong>СПО</strong></i><strong>:</strong> прогнали <strong>85&nbsp;</strong>т., остаток <strong>0&nbsp;</strong>т., песков 3,2 т.&nbsp;</p><p style='text - align:justify;'>В работе КС-1 и <i>три&nbsp;</i>Mn мельницы. <i><strong>РМО</strong></i>: поток <strong>8</strong> м3/ч.&nbsp;H2SO4 ~ 13 т.&nbsp;</p><p style='text - align:justify;'>Переработано Владикавказ/Проминдустрия /изгарь (МКР): <strong>0/0/8</strong>; с н.м. <strong>574/7/111</strong>.</p><p><i><strong>«Larox»</strong></i> (<i><strong>Zn кек</strong></i> + <i><strong>гетит</strong></i>): циклов <strong>15&nbsp;</strong>Þ <strong>30,0</strong> т. (~ 2,0 т<strong>/</strong>ц). Простои: 100 ремонт маслостанции по эл. части.</p><p style='text - align:justify;'><i><strong>«МС-100» </strong></i><strong>№1, 2: (</strong><i><strong>CuCl кек</strong></i><strong>)</strong>:<strong>&nbsp;</strong>циклов <strong>15 + 15&nbsp;</strong>Þ <strong>60,0</strong> т. (~ 2,0 т<strong>/</strong>ц).&nbsp;</p><p style='text - align:justify;'><i><strong>«FB-1200»</strong></i><strong> (</strong><i><strong>ППФ</strong></i><strong>)</strong>: циклов<strong> 2</strong>&nbsp;Þ <strong>4,0</strong> т. (~ 2,0 т<strong>/</strong>ц).</p><p style='text - align:justify;'><i><strong>«Diefenbach»</strong></i><strong>№14.1/14.2&nbsp;</strong>(<i><strong>Pb карб. кек</strong></i>): циклов <strong>7</strong> + <strong>8</strong>&nbsp;Þ <strong>64,5</strong> т. (~ 4,3/4,3 т/ц).</p>";
                        break;
                    case "11":
                        ret.text = "<p style='text - align:justify;'><strong>СКЦ. Получено кислоты</strong>: <strong>501 т.&nbsp;</strong></p><p style='text - align:justify;'>Газовая нагрузка <strong>94,0&nbsp;</strong>(32,0-33,0-29,0) <strong>тыс. м3/ч</strong>. Газ: 8,0-8,8-8,8%. К/а в режимах.</p><p style='text - align:justify;'>Свободный объем <strong>4913 </strong>т. (под&nbsp;II сорт <strong>2106</strong>). Прирост промывных кислот незначительный.</p><p style='text - align:justify;'>&nbsp;</p><p style='text - align:justify;'>&nbsp;</p><p style='text - align:justify;'>УОС в норме. ВКУ: в работе 5 аппаратов (конденсата ~ 14,1 т/ч).</p><p style='text - align:justify;'>&nbsp;</p><p style='text - align:justify;'>Кислородная станция:&nbsp;в работе блоки с 2120 1, 2, 4→1, 3, 4 (1930/2130/2250&nbsp;нм3/ч); №2 в резерве.</p><p style='text - align:justify;'>&nbsp;</p><p>Объем растворов по заводу: 21001 м3 (+ 51).</p>";
                        break;
                    default:
                        ret.text = "";
                        break;
                }
                ret.name = $"Шаблон рапорта {shop}";
                ret.dt = DateTime.Now;
                /*using (StreamReader sr = new StreamReader(@"D:\My projects\UGMK\ZnDisp1\Controllers\Templates\template_auto_shop.html"))
                {
                    ret.text = sr.ReadToEnd();
                    ret.name = $"Шаблон рапорта {shop}";
                    ret.dt = DateTime.Now;
                }*/

                if (ret == null)
                    return NotFound();

                return Ok(ret);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("SetTemplateReport")]
        public async Task<IActionResult> SetTemplateReport([FromBody]TemplateReport template)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                using (StreamWriter sw = new StreamWriter($"c:\\Reports_test\\rep_{template.name}.html", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(template.text);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetDispReport")]
        public async Task<IActionResult> GetDispReport()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                ShopReport ret = new ShopReport();

                ret.name = $"Рапорт диспетчера";
                ret.dt = DateTime.Now;
                foreach (string file in System.IO.Directory.GetFiles(@"C:\Reports_test\").OrderBy(x => x.Substring(21,2)))
                {
                    using (StreamReader sr = new StreamReader(file))
                    {
                        ret.text += sr.ReadToEnd();
                        ret.text += "<br>";
                    }
                }

                if (ret == null)
                    return NotFound();

                return Ok(ret);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("SetTemplateReport2")]
        public async Task<IActionResult> SetTemplateReport2([FromBody]TemplateReport template)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                string folder1 = $"c:\\Reports_test\\{template.dt.ToString("yyyy-MM-dd")}";
                if (!System.IO.Directory.Exists(folder1))
                {
                    System.IO.Directory.CreateDirectory(folder1);
                }
                string folder2 = $"{folder1}\\{template.shift}";
                if (!System.IO.Directory.Exists(folder2))
                {
                    System.IO.Directory.CreateDirectory(folder2);
                }

                using (StreamWriter sw = new StreamWriter($"{folder2}\\rep_{template.name}.html", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(template.text);
                }

                string folder3 = $"c:\\Reports_test_shop\\{template.dt.ToString("yyyy-MM-dd")}";
                if (!System.IO.Directory.Exists(folder3))
                {
                    System.IO.Directory.CreateDirectory(folder3);
                }
                string folder4 = $"{folder3}\\{template.shift}";
                if (!System.IO.Directory.Exists(folder4))
                {
                    System.IO.Directory.CreateDirectory(folder4);
                }

                using (StreamWriter sw = new StreamWriter($"{folder4}\\rep_{template.name}.html", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(template.text);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetDispReport2/{date}/{shift}")]
        public async Task<IActionResult> GetDispReport2([FromRoute] DateTime date, [FromRoute] int shift)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                ShopReport ret = new ShopReport();

                ret.name = $"Рапорт диспетчера";
                ret.dt = DateTime.Now;
                string folder = $"C:\\Reports_test\\{date.ToString("yyy-MM-dd")}\\{shift}";

                
                foreach (string file in System.IO.Directory.GetFiles(folder).OrderBy(x => System.Convert.ToInt32(x.Substring(33, 2).Replace(".", ""))))
                {
                    using (StreamReader sr = new StreamReader(file))
                    {
                        ret.text += sr.ReadToEnd();
                        ret.text += "<br>";
                    }
                }

                if (ret == null)
                    return NotFound();

                return Ok(ret);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetTemplateReport2/{shop}/{date}/{shift}")]
        public async Task<IActionResult> GetTemplateReport2([FromRoute] string shop, [FromRoute] DateTime date, [FromRoute] int shift)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                TemplateReport ret = new TemplateReport();
                string folder = $"c:\\Reports_test_shop\\{date.ToString("yyyy-MM-dd")}\\{shift}";
                if (!System.IO.Directory.Exists(folder))
                {
                    Task<IActionResult> task = Task.Run<IActionResult>(async () => await GetTemplateReport(shop));
                    task.Wait();
                    return Ok(task.Result);
                }
                else
                {
                    string file = $"c:\\Reports_test_shop\\{date.ToString("yyyy-MM-dd")}\\{shift}\\rep_{shop}.html";
                    ret.name = $"";
                    ret.dt = DateTime.Now;
                    if (System.IO.File.Exists(file))
                    {
                        using (StreamReader sr = new StreamReader(file))
                        {
                            ret.text = sr.ReadToEnd();
                        }
                        return Ok(ret);
                    }
                    else
                    {
                        Task<IActionResult> task = Task.Run<IActionResult>(async () => await GetTemplateReport(shop));
                        task.Wait();
                        return Ok(task.Result);
                    }
                }

                if (ret == null)
                    return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetReadyReports/{date}/{shift}")]
        public async Task<IActionResult> GetReadyReports([FromRoute] DateTime date, [FromRoute] int shift)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                List<int> ret = new List<int>();
                string folder = $"C:\\Reports_test\\{date.ToString("yyyy-MM-dd")}\\{shift}";
                //ret = System.IO.Directory.GetFiles(folder).Select(x => System.Convert.ToInt32(x.Substring(33, 2).Replace(".", ""))).ToList();
                List<string> files = System.IO.Directory.GetFiles(folder).ToList();
                foreach (string file in files)
                {
                    try
                    {
                        int rep = System.Convert.ToInt32(file.Substring(33, 2).Replace(".", ""));
                        ret.Add(rep);
                    }
                    catch { }
                }
                if (ret.Count == 0)
                    return NotFound();

                ret = ret.OrderBy(x => x).ToList();
                return Ok(ret);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
