﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtoMES.Controllers.Models
{
    public class Sample
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime? date { get; set; }
        public string comp { get; set; }
        public double? val { get; set; }
        public double? min { get; set; }
        public double? max { get; set; }
        public string eu { get; set; }
        public int? shift { get; set; }
        public int? furnance { get; set; }
    }

    public class SampleResult
    {
        public int id { get; set; }
        public string name { get; set; }
        public Dictionary<string, ChemResult> results { get; set; }
    }

    public class ChemResult
    {
        public string name { get; set; }
        //public Dictionary<DateTime, double?> values { get; set; }
        public List<Tag2> values { get; set; }
        public double? min { get; set; }
        public double? max { get; set; }
        public string eu { get; set; }
        public int? shift { get; set; }
        public int? furnance { get; set; }
    }
}
