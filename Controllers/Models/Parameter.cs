﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtoMES.Controllers.Models
{
    public class Parameter
    {
        public Parameter()
        {

        }

        public Parameter(int id, string name, string description, string type, int groupId)//, List<Parameter> children)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.type = type;
            this.groupId = groupId;
            //this.children = children;

            //this.values = new Dictionary<DateTime, object>();
        }

        public Parameter(string name, string description, string tag)
        {
            this.name = name;
            this.description = description;
            this.tag = tag;

            this.children = new List<Parameter>();
        }

        public int id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string tag { get; set; }
        public int groupId { get; set; }
        public List<Parameter> children { get; set; }

        //public Dictionary<DateTime, object> values { get; set; }
    }
}
