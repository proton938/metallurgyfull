﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtoMES.Controllers.Models
{
    public class Tag
    {
        public string name { get; set; }
        public DateTime dt { get; set; }
        public double val { get; set; }
        public ushort qual { get; set; }
    }

    public class Tag2
    {
        public string name { get; set; }
        public DateTime dt { get; set; }
        public double? val { get; set; }
        public ushort qual { get; set; }
    }

    public class StringTag
    {
        public string name { get; set; }
        public DateTime dt { get; set; }
        public string val { get; set; }
        public ushort qual { get; set; }

        List<string> parts = null;
        public List<string> Parts
        {
            get
            {
                if (parts == null)
                    parts = new List<string>();
                if(string.IsNullOrEmpty(val))
                    parts = Split(val).ToList();
                return parts;
            }
            set
            {
                parts = value;
                val = string.Join("", parts);
            }
        }

        static IEnumerable<string> Split(string s, int length = 512)
        {
            int i;
            for (i = 0; i + length < s.Length; i += length)
                yield return s.Substring(i, length);
            if (i != s.Length)
                yield return s.Substring(i, s.Length - i);
        }

        public List<string> Split()
        {
            if (parts == null)
                parts = new List<string>();
            if (!string.IsNullOrEmpty(val))
                parts = Split(val).ToList();
            return parts;
        }

        public string Join()
        {
            if (parts != null)
                val = string.Join("", parts);
            return val;
        }
    }

    public class AnalysisTemplate
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<TagPrm> tags { get; set; }
        public string createdBy { get; set; }
    }

    public class TagPrm
    {
        public int id { get; set; }
        public int id_templ { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
        public string eu { get; set; }
    }
}
