﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtoMES.Controllers.Models
{
    public class InputParameter
    {
        public InputParameter()
        {

        }

        public int id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string tag { get; set; }
        public int groupId { get; set; }
        public List<Parameter> children { get; set; }
        public double value { get; set; }
        public double min { get; set; }
        public double max { get; set; }
    }
}
