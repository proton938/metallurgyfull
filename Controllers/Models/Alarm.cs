﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtoMES.Controllers.Models
{
    public class Alarm
    {
        public string id { get; set; }
        public DateTime eventTime { get; set; }
        public string sourceProcess { get; set; }
        public string condition { get; set; }
        public string limit { get; set; }
        public string value { get; set; }
        public string type { get; set; }
        public string comment { get; set; }
        public string sourceCondition { get; set; }
        public string message { get; set; }
    }

    public class SMSMessage
    {
        public DateTime eventTime { get; set; }
        public string message { get; set; }
        public string translate { get; set; }
        public DateTime ackAt { get; set; }
        public string ackBy { get; set; }
        public bool ack { get; set; }
    }

    public class AckAlarm
    {
        public int id { get; set; }
        public DateTime messageAt { get; set; }
        public string message { get; set; }
        public DateTime ackAt { get; set; }
        public string ackBy { get; set; }
        public bool ack { get; set; }
    }
}
