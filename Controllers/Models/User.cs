﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtoMES.Controllers.Models
{
    public class User
    {
        public string name { get; set; }
        public string displayName { get; set; }

    }
}
