﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using ProtoMES.Controllers.Models;

namespace ProtoMES.Controllers
{
    public class ZincDBExt : Singleton<ZincDBExt>
    {
        private SqlConnection conn = new SqlConnection(@"server=ZN-WWSP-02;uid=soupUser;pwd=Qwerty1!;database=ZincExt");

        private Dictionary<string, string> translate = new Dictionary<string, string>();
        private ZincDBExt()
        {
            translate.Add("ostanovka", "остановка");
            translate.Add("eksgaustera", "эксгаустера");
            translate.Add("Zatopleniye", "Затопление");
            translate.Add("nasosnoy", "насосной");
            translate.Add("obzhigovogo", "обжигового");
            translate.Add("tsekha", "цеха");
            translate.Add("Razrezheniye", "Разрежение");
            translate.Add("v", "в");
            translate.Add("kollektore", "коллекторе");
            translate.Add("chistogo", "чистого");
            translate.Add("gaza", "газа");
            translate.Add("nizkoye", "низкое");
            translate.Add("Nagruzka", "Нагрузка");
            translate.Add("na", "на");
            translate.Add("Snizheniye", "Снижение");
            translate.Add("rastvora", "раствора");
            translate.Add("nizhe", "ниже");
            translate.Add("Obryv", "Обрыв");
            translate.Add("peregrebayushchikh", "перегребающих");
            translate.Add("sgustitele", "сгустителе");
            translate.Add("Uroven", "Уровень");
            translate.Add("zheloba", "желоба");
            translate.Add("vysokiy", "высокий");
            translate.Add("nizkiy", "низкий");
            translate.Add("Zumpf", "Зумпф");
            translate.Add("avariynyy", "аварийный");
            translate.Add("uroven", "уровень");
            translate.Add("Ostanovka", "Остановка");
            translate.Add("privoda", "привода");
            translate.Add("peregrebayushchego", "перегребающего");
            translate.Add("mekhanizma", "механизма");
            translate.Add("sgustitelya", "сгустителя");
            translate.Add("glavnogo", "главного");
            translate.Add("privodavelts-pechi", "привода вельц-печи");
            translate.Add("boleye", "более");
            translate.Add("minut", "минут");
            translate.Add("Neytralnyy", "Нейтральный");
            translate.Add("sgustitel", "сгуститель");
            translate.Add("raskhod", "расход");
            translate.Add("net", "нет");
            translate.Add("zagruzki", "загрузки");
        }

        public bool SetAcknowledgment(string message, DateTime msg_at, DateTime ack_at, string user)
        {
            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            try
            {
                // check existing ack of alarm in DB
                string SQL = $"Select ack_at From alarms_ack" +
                    $" Where message = @msg And message_at = @msg_at";
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    SqlParameter prmMsg = new SqlParameter("@msg", message);
                    SqlParameter prmMsgAt = new SqlParameter("@msg_at", msg_at);

                    (cmd as SqlCommand).Parameters.AddRange(new SqlParameter[] { prmMsg, prmMsgAt });
                    object check = cmd.ExecuteScalar();
                    if (check != null)
                        throw new Exception($"Аларм уже квитировался в {System.Convert.ToDateTime(check)}");
                }

                SQL = $"Insert Into alarms_ack (message, message_at, ack, ack_at, ack_by)" +
                    $" Values (@message, @message_at, @ack, @ack_at, @ack_by)";

                using (var cmd = new SqlCommand(SQL, conn))
                {
                    SqlParameter prmMessage = new SqlParameter("@message", message);
                    SqlParameter prmMessageAt = new SqlParameter("@message_at", msg_at);
                    SqlParameter prmAck = new SqlParameter("@ack", true);
                    SqlParameter prmAckDt = new SqlParameter("@ack_at", ack_at);
                    SqlParameter prmAckBy = new SqlParameter("@ack_by", user);

                    (cmd as SqlCommand).Parameters.AddRange(new SqlParameter[] { prmMessage, prmMessageAt, prmAck, prmAckDt, prmAckBy });

                    int rows = cmd.ExecuteNonQuery();
                    if (rows > 0)
                        return true;
                    else return false;
                }
            }
            catch (Exception e)
            {
                throw new Exception($"Ошибка квитирования аларма, {e.Message}");
            }
        }

        public Dictionary<DateTime, List<Models.AckAlarm>> GetAckAlarms(DateTime dateFrom, DateTime dateTo)
        {
            Dictionary<DateTime, List<Models.AckAlarm>> results = new Dictionary<DateTime, List<Models.AckAlarm>>();

            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            string SQL = $"Select * From alarms_ack" +
                $" Where message_at >= '{dateFrom.ToString("yyyy-MM-dd HH:mm:ss")}' And message_at <= '{dateTo.ToString("yyyy-MM-dd HH:mm:ss")}'";
            try
            {
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DateTime dt = System.Convert.ToDateTime(reader.GetValue(2), System.Globalization.CultureInfo.InvariantCulture);
                            Models.AckAlarm ack = new Models.AckAlarm()
                            {
                                id = System.Convert.ToInt32(reader.GetValue(0)),
                                message = reader.GetValue(1).ToString(),
                                messageAt = dt,
                                ack = System.Convert.ToBoolean(reader.GetValue(3)),
                                ackAt = System.Convert.ToDateTime(reader.GetValue(4), System.Globalization.CultureInfo.InvariantCulture),
                                ackBy = reader.GetValue(5).ToString(),
                            };
                            if (!results.ContainsKey(dt.Date))
                            {
                                results.Add(dt.Date, new List<Models.AckAlarm>() { ack });
                            }
                            else
                            {
                                results[dt.Date].Add(ack);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception($"Ошибка получения квитированных алармов, {e.Message}");
            }
            finally
            {
            }

            return results;
        }

        public List<Models.AnalysisTemplate> GetAnalysisTemplates()
        {
            Dictionary<string, Models.AnalysisTemplate> results = new Dictionary<string, Models.AnalysisTemplate>();

            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            string SQL = $"Select at.*, att.tagname, att.tagdesc, att.eu, att.id as pos From ana_templates at" +
                $" Inner Join ana_template_tags att On at.id = att.id_templ";
            try
            {
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string name = reader.GetValue(1).ToString();
                            if (!results.ContainsKey(name))
                            {
                                Models.AnalysisTemplate templ = new Models.AnalysisTemplate()
                                {
                                    id = System.Convert.ToInt32(reader.GetValue(0)),
                                    name = name,
                                    createdBy = reader.GetValue(2).ToString(),
                                    tags = new List<Models.TagPrm>() { new Models.TagPrm() { id_templ = System.Convert.ToInt32(reader.GetValue(0)), name = reader.GetValue(3).ToString(), desc = reader.GetValue(4).ToString(), eu = reader.GetValue(5).ToString(), id = System.Convert.ToInt32(reader.GetValue(6)) } }
                                };
                                results.Add(name, templ);
                            }
                            else
                            {
                                results[name].tags.Add(new Models.TagPrm() { id_templ = System.Convert.ToInt32(reader.GetValue(0)), name = reader.GetValue(3).ToString(), desc = reader.GetValue(4).ToString(), eu = reader.GetValue(5).ToString(), id = System.Convert.ToInt32(reader.GetValue(6)) });
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception($"Ошибка получения квитированных алармов, {e.Message}");
            }
            finally
            {
            }

            return results.Values.ToList();
        }

        public string TranslateMessage(string in_message)
        {
            if (string.IsNullOrEmpty(in_message))
                return null;

            string out_message = "";
            List<string> words = new List<string>();
            words = in_message.Split(new char[] { ' ' }).ToList();
            foreach (string word in words)
            {
                if (translate.ContainsKey(word))
                {
                    out_message += translate[word];
                }
                else
                {
                    out_message += word;
                }
                out_message += " ";
            }
            out_message = out_message.TrimEnd();
            return out_message;
        }

        public void UpdateAnalysisTemplate(Models.AnalysisTemplate template, string user)
        {
            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            int id_template = 0;
            try
            {
                object id = null;
                // check existing ack of alarm in DB
                string SQL = $"Select id From ana_templates" +
                    $" Where id = @id And name = @name";
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    SqlParameter prmId = new SqlParameter("@id", template.id);
                    SqlParameter prmName = new SqlParameter("@name", template.name);

                    (cmd as SqlCommand).Parameters.AddRange(new SqlParameter[] { prmId, prmName });
                    id = cmd.ExecuteScalar();
                }
                if (id == null)
                {
                    SQL = $"Insert Into ana_templates (name, created_by)" +
                    $" Values (@name, @user); SELECT SCOPE_IDENTITY()";
                    using (var cmd = new SqlCommand(SQL, conn))
                    {
                        SqlParameter prmName = new SqlParameter("@name", template.name);
                        SqlParameter prmUser = new SqlParameter("@user", user);

                        (cmd as SqlCommand).Parameters.AddRange(new SqlParameter[] { prmUser, prmName });
                        id = cmd.ExecuteScalar();
                        if(id == null)
                            throw new Exception($"Профиль {template.name} не создан");
                        else
                            id_template = System.Convert.ToInt32(id);
                    }
                }
                else
                {
                    id_template = System.Convert.ToInt32(id);
                }

                SQL = $"Insert Into ana_template_tags (id_templ, tagname, tagdesc, eu)" +
                    $" Values (@templ, @tagname, @tagdesc, @eu)";

                foreach (TagPrm tag in template.tags)
                {
                    using (var cmd = new SqlCommand(SQL, conn))
                    {
                        SqlParameter prmTempl = new SqlParameter("@templ", id_template);
                        SqlParameter prmTag = new SqlParameter("@tagname", tag.name);
                        SqlParameter prmDesc = new SqlParameter("@tagdesc", tag.desc);
                        SqlParameter prmEU = new SqlParameter("@eu", tag.eu);

                        (cmd as SqlCommand).Parameters.AddRange(new SqlParameter[] { prmTempl, prmTag, prmDesc, prmEU });

                        int rows = cmd.ExecuteNonQuery();
                        if (rows <= 0)
                            throw new Exception($"Параметр {tag.name} не добавлен в шаблон {template.name}");
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception($"Ошибка создания/обновления шаблона, {e.Message}");
            }
        }

        public void DeleteAnalysisTemplate(Models.AnalysisTemplate template, string user)
        {
            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            try
            {
                string SQL = null;

                if (template.tags.Count != 0)
                {
                    string ids = null;
                    ids = string.Join(',', template.tags.Select(x => x.id));
                    SQL = $"Delete From ana_template_tags Where id In ({ids})";
                    using (var cmd = new SqlCommand(SQL, conn))
                    {
                        int rows = cmd.ExecuteNonQuery();
                        if (rows <= 0)
                            throw new Exception($"Из шаблона {template.name} не были удалены позиции {ids}");
                    }
                }

                int tags = 0;
                SQL = $"Select Count(*) From ana_template_tags Where id_templ = {template.id}";
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    object cnt = cmd.ExecuteScalar();
                    tags = System.Convert.ToInt32(cnt);
                }

                if (tags == 0)
                {
                    SQL = $"Delete From ana_templates Where id In ({template.id})";
                    using (var cmd = new SqlCommand(SQL, conn))
                    {
                        int rows = cmd.ExecuteNonQuery();
                        if (rows <= 0)
                            throw new Exception($"Шаблон {template.name} не был удален");
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception($"Ошибка удаления шаблона, {e.Message}");
            }
        }
    }
}
