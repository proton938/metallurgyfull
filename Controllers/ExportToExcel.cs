﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;
using NPOI.XSSF.UserModel;

namespace ProtoMES.Controllers
{
    public enum FileFormat
    {
        XLS,
        XLSX
    }
    /// <summary>
    /// Модифицированный MemoryStream для обхода бага в NPOI
    /// <remarks>NPOI любит закрывать Stream когда надо и когда не надо...</remarks>
    /// </summary>
    public class NpoiMemoryStream : MemoryStream
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public NpoiMemoryStream()
        {
            AllowClose = false;
        }
        /// <summary>
        /// Разрешить закрытие потока
        /// </summary>
        public bool AllowClose { get; set; }

        /// <inheritdoc />
        public override void Close()
        {
            if (AllowClose)
                base.Close();
        }
    }

    public class ExportToExcel : Singleton<ExportToExcel>
    {
        private ExportToExcel()
        {
        }

        private Stream ExportToStream(FileFormat format)
        {
            try
            {
                var ms = new NpoiMemoryStream();
                //var oper = new ExportExcelOperation(this, ms, format);
                //oper.Execute();
                ms.AllowClose = true;
                ms.Seek(0, SeekOrigin.Begin);
                return ms;
            }
            catch (Exception ex)
            {
                //SysLog.Error("Excel export exception", ex);
                throw;
            }
        }

        public Stream ParametersToExcel(Dictionary<string, List<Models.Tag>> tags)
        {
            var book = new XSSFWorkbook();
            book.CreateSheet("Выгрузка");
            ISheet sheet = book.GetSheet("Выгрузка");

            IRow headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("DateTime");
            int columnNo = 1;
            foreach (var name in tags.Keys)
            {
                ICell newCell = headerRow.CreateCell(columnNo);
                newCell.SetCellValue(name);

                columnNo++;
            }

            int maxRow = 1;
            DateTime minDate = tags.FirstOrDefault().Value.Min(y => y.dt);
            DateTime maxDate = tags.FirstOrDefault().Value.Max(y => y.dt);
            for (DateTime d = minDate; d <= maxDate; d = d.AddSeconds(100))
            {
                IRow newRow = sheet.CreateRow(maxRow);
                ICell dateCell = newRow.CreateCell(0);
                dateCell.SetCellValue(d.ToString());
                columnNo = 1;
                foreach (var tag in tags)
                {
                    ICell newCell = newRow.CreateCell(columnNo);
                    newCell.SetCellValue(tag.Value.Where(x => x.dt == d).FirstOrDefault().val);

                    columnNo++;
                }

                maxRow++;
            }
            for (int i = 0; i < tags.Count + 1; i++)
            {
                try
                {
                    sheet.AutoSizeColumn(i);
                }
                catch (Exception)
                {
                    try
                    {
                        sheet.SetColumnWidth(i, 7000);
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            /*using (var fileStream = new FileStream(@"c:\test.xls", FileMode.Create, FileAccess.Write))
            {
                book.Write(fileStream);
            }*/

            NpoiMemoryStream ms = new NpoiMemoryStream();
            book.Write(ms);
            ms.AllowClose = true;
            ms.Seek(0, SeekOrigin.Begin);
            return ms;
        }

        public Stream AnalysisToExcel(List<Models.ChemResult> results, DateTime from, DateTime to)
        {
            var book = new XSSFWorkbook();
            book.CreateSheet("Выгрузка");
            ISheet sheet = book.GetSheet("Выгрузка");

            IRow headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("DateTime");
            int columnNo = 1;
            foreach (var name in results.Select(x => x.name).ToList())
            {
                ICell newCell = headerRow.CreateCell(columnNo);
                newCell.SetCellValue(name);

                columnNo++;
            }

            int maxRow = 1;
            DateTime minDate = from;// results.FirstOrDefault().values.Min(y => y.dt);
            DateTime maxDate = to;// results.FirstOrDefault().values.Max(y => y.dt);
            for (DateTime d = minDate; d <= maxDate; d = d.AddDays(1))
            {
                IRow newRow = sheet.CreateRow(maxRow);
                ICell dateCell = newRow.CreateCell(0);
                dateCell.SetCellValue(d.ToString());
                columnNo = 1;
                foreach (var result in results)
                {
                    ICell newCell = newRow.CreateCell(columnNo);
                    if (result.values.Where(x => x.dt == d).FirstOrDefault() == null)
                        newCell.SetCellValue(false);
                    else
                    {
                        double? val = result.values.Where(x => x.dt == d).FirstOrDefault().val;
                        if(val.HasValue)
                            newCell.SetCellValue(result.values.Where(x => x.dt == d).FirstOrDefault().val.Value);
                    }
                    columnNo++;
                }

                maxRow++;
            }
            for (int i = 0; i < results.Count + 1; i++)
            {
                try
                {
                    sheet.AutoSizeColumn(i);
                }
                catch (Exception)
                {
                    try
                    {
                        sheet.SetColumnWidth(i, 7000);
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            NpoiMemoryStream ms = new NpoiMemoryStream();
            book.Write(ms);
            ms.AllowClose = true;
            ms.Seek(0, SeekOrigin.Begin);
            return ms;
        }
    }
}
