﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using ProtoMES.Controllers.Models;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace ProtoMES.Controllers
{
    public class Analysis : Singleton<Analysis>
    {
        private static readonly object _loc_session = new object();

        private SqlConnection conn = new SqlConnection("server=LION;uid=r_ofxma;pwd=Analiz123;database=ANALYSIS");
        //private SqlConnection conn = new SqlConnection("Server=(local);Database=ANALYSIS;Trusted_Connection=True");
        private Analysis()
        {
        }

        public Dictionary<int, string> GetSampleNames()
        {
            Dictionary<int, string> results = new Dictionary<int, string>();

            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            string SQL = $"Select t.kod_pok, t.naim_pok From n127 t Where t.udalena = 0 And t.pokaz = 0";
            try
            {
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string name = reader.GetValue(1).ToString().Trim();
                            int id = System.Convert.ToInt32(reader.GetValue(0));
                            if (!results.ContainsKey(id))
                            {
                                results.Add(id, name);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
            }

            return results;
        }

        public Dictionary<int, string> GetSampleNames2(DateTime dateFrom, DateTime dateTo)
        {
            Dictionary<int, string> results = new Dictionary<int, string>();

            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            SqlParameter fromPrm = new SqlParameter("@begin", dateFrom);
            SqlParameter toPrm = new SqlParameter("@end", dateTo);

            string SQL = $"Select t.kod_pok, t.naim_pok From n127 t Where t.udalena = 0 And t.pokaz = 0" +
                $" And Exists ( Select m.date1 From m183 m" +
                $" inner join m183_ana ma On ma.nom_ana = m.nom" +
                $" inner join komponen k On k.kod = ma.cod_comp" +
                $" inner Join n127_kom nk On nk.kod_kom = k.kod And nk.kod_pok = m.kod_pok" +
                $" Where m.date1 Between @begin And @end And nk.kod_pok = t.kod_pok)";
            try
            {
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    (cmd as SqlCommand).Parameters.AddRange(new SqlParameter[] { fromPrm, toPrm });

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string name = reader.GetValue(1).ToString().Trim();
                            int id = System.Convert.ToInt32(reader.GetValue(0));
                            if (!results.ContainsKey(id))
                            {
                                results.Add(id, name);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
            }

            return results;
        }

        public List<Sample> GetSamples(int code, DateTime from, DateTime to)
        {
            List<Sample> results = new List<Sample>();

            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            SqlParameter fromPrm = new SqlParameter("@begin", from);
            SqlParameter toPrm = new SqlParameter("@end", to);
            SqlParameter codePrm = new SqlParameter("@code", code);

            /*string SQL = $"Select m.date1, m.smena, m.pech, k.komponen, Avg(ma.val_ana) as val_ana, nk.min, nk.max, n.nei, p.naim_pok, p.kod_pok From m183_ana ma" +
                $" Inner Join m183 m On m.nom = ma.nom_ana" +
                $" Inner Join komponen k On k.kod = ma.cod_comp" +
                $" Inner Join n127_kom nk On nk.kod_kom = k.kod" +
                $" Inner Join n03 n On n.kei = nk.kei" +
                $" Inner Join n127 p On p.kod_pok = nk.kod_pok " +
                $" Where m.date1 Between @begin And @to" +
                $" And p.kod_pok = @code" +
                $" Group By  m.date1, m.smena, m.pech, k.komponen, nk.min, nk.max, n.nei, p.naim_pok, p.kod_pok" +
                $" Order By k.komponen, m.date1";*/
            string SQL = $"Select m.date1, m.smena, m.pech, k.komponen, ma.val_ana, nk.min, nk.max, n.nei, p.naim_pok, p.kod_pok From m183 m" +
                $" inner join m183_ana ma On ma.nom_ana = m.nom" +
                $" inner join komponen k On k.kod = ma.cod_comp" +
                $" inner Join n127_kom nk On nk.kod_kom = k.kod And nk.kod_pok = m.kod_pok" +
                $" inner Join n03 n On n.kei = nk.kei" +
                $" inner Join n127 p On p.kod_pok = nk.kod_pok " +
                $" Where m.date1 Between @begin And @end" +
                $" And p.kod_pok = @code";
            try
            {
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    (cmd as SqlCommand).Parameters.AddRange(new SqlParameter[] { fromPrm, toPrm, codePrm });

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Sample sample = new Sample()
                            {
                                date = !reader.IsDBNull(0) ? Convert.ToDateTime(reader.GetValue(0), System.Globalization.CultureInfo.InvariantCulture) : (DateTime?)null,
                                shift = !reader.IsDBNull(1) ? Convert.ToInt32(reader.GetValue(1), System.Globalization.CultureInfo.InvariantCulture) : (int?)null,
                                furnance = !reader.IsDBNull(2) ? Convert.ToInt32(reader.GetValue(2), System.Globalization.CultureInfo.InvariantCulture) : (int?)null,
                                comp = reader.GetValue(3).ToString().Trim(),
                                val = !reader.IsDBNull(4) ? Convert.ToDouble(reader.GetValue(4), System.Globalization.CultureInfo.InvariantCulture) : (double?)null,
                                min = !reader.IsDBNull(5) ? Convert.ToDouble(reader.GetValue(5), System.Globalization.CultureInfo.InvariantCulture) : (double ?)null,
                                max = !reader.IsDBNull(6) ? Convert.ToDouble(reader.GetValue(6), System.Globalization.CultureInfo.InvariantCulture) : (double?)null,
                                eu = reader.GetValue(7).ToString().Trim(),
                                name = reader.GetValue(8).ToString().Trim()
                            };
                            results.Add(sample);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            finally
            {
            }

            return results;
        }

        public Dictionary<DateTime, double> GetCompValues(int sample, string comp, int shift, int furnance, DateTime from, DateTime to)
        {
            Dictionary<DateTime, double> results = new Dictionary<DateTime, double>();

            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            SqlParameter fromPrm = new SqlParameter("@begin", from);
            SqlParameter toPrm = new SqlParameter("@to", to);
            SqlParameter samplePrm = new SqlParameter("@sample", sample);
            SqlParameter compPrm = new SqlParameter("@comp", comp);
            SqlParameter shiftPrm = new SqlParameter("@shift", shift);
            SqlParameter furnPrm = new SqlParameter("@furn", furnance);

            string SQL = $"Select m.date1, Avg(ma.val_ana) as val_ana From m183_ana ma" +
                $" Inner Join m183 m On m.nom = ma.nom_ana" +
                $" Inner Join komponen k On k.kod = ma.cod_comp" +
                $" Inner Join n127_kom nk On nk.kod_kom = k.kod" +
                $" Inner Join n03 n On n.kei = nk.kei" +
                $" Inner Join n127 p On p.kod_pok = nk.kod_pok " +
                $" Where m.date1 Between @begin And @to" +
                $" And p.kod_pok = @sample" +
                $" And m.smena = @shift" +
                $" And m.pech = @furn" +
                $" And k.komponen = @comp" +
                $" Group By m.date1" +
                $" Order By m.date1";
            try
            {
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    (cmd as SqlCommand).Parameters.AddRange(new SqlParameter[] { fromPrm, toPrm, samplePrm, compPrm, shiftPrm, furnPrm });

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DateTime? date = !reader.IsDBNull(0) ? Convert.ToDateTime(reader.GetValue(0), System.Globalization.CultureInfo.InvariantCulture) : (DateTime?)null;
                            double? val = !reader.IsDBNull(1) ? Convert.ToDouble(reader.GetValue(1), System.Globalization.CultureInfo.InvariantCulture) : (double?)null;
                            results.Add(date.Value, val.Value);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            finally
            {
            }

            return results;
        }

        /*Select t.kod_pok, t.naim_pok, n.nsp From ANALYSIS.ANALYSIS.dbo.n127 t
Inner Join ANALYSIS.ANALYSIS.dbo.n127_ksp k On k.kod_pok = t.kod_pok
Inner Join ANALYSIS.ANALYSIS.dbo.n08 n On n.ksp = k.ksp
Where n.ksp = 100 */
    }
}
