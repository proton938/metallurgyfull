﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace ProtoMES.Controllers
{
    public class Historian : Singleton<Historian>
    {
        private static readonly object _loc_session = new object();

        private SqlConnection conn = new SqlConnection("server=ZN-WWSP-01;uid=apcUser;pwd=1234qwerASDF;database=Runtime");
        private Historian()
        {
        }

        public Dictionary<string, List<Models.Tag>> GetHistoryValues(string[] tags, DateTime dateFrom, DateTime dateTo)
        {
            Dictionary<string, List<Models.Tag>> results = new Dictionary<string, List<Models.Tag>>();

            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            string SQL = $"Select TagName, DateTime, Value, Quality From History Where TagName In('{string.Join("','", tags)}')" +
                $" And DateTime >= '{dateFrom.ToString("yyyy-MM-dd HH:mm:ss")}' And DateTime <= '{dateTo.ToString("yyyy-MM-dd HH:mm:ss")}'" +
                $" AND wwRetrievalMode = 'Interpolated' AND wwResolution = '100000'";
            try
            {
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string tagname = reader.GetValue(0).ToString();
                            double val = 0;
                            if(!DBNull.Value.Equals(reader.GetValue(2)))
                                val = System.Convert.ToDouble(reader.GetValue(2), System.Globalization.CultureInfo.InvariantCulture);
                            DateTime dt = System.Convert.ToDateTime(reader.GetValue(1));
                            ushort qual = System.Convert.ToUInt16(reader.GetValue(3));
                            if (!results.ContainsKey(tagname))
                            {
                                Models.Tag t = new Models.Tag()
                                {
                                    dt = dt,
                                    qual = qual,
                                    val = val
                                };
                                results.Add(tagname, new List<Models.Tag>() { t });
                            }
                            else
                            {
                                Models.Tag t = new Models.Tag()
                                {
                                    dt = dt,
                                    qual = qual,
                                    val = val
                                };
                                results[tagname].Add(t);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
            }

            return results;
        }

        public List<Models.Tag> GetHistoryValuesForPrediction(string[] tags, DateTime dateFrom, DateTime dateTo)
        {
            List<Models.Tag> results = new List<Models.Tag>();

            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            string SQL = $"Select TagName, DateTime, Value From History Where TagName In('{string.Join("','", tags)}')" +
                $" And DateTime >= '{dateFrom.ToString("yyyy-MM-dd HH:mm:ss")}' And DateTime <= '{dateTo.ToString("yyyy-MM-dd HH:mm:ss")}'" +
                $" AND wwRetrievalMode = 'Interpolated' AND wwResolution = '100000'";
            try
            {
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            /*string tagname = reader.GetValue("TagName").ToString();
                            double val = System.Convert.ToDouble(reader.GetValue("Value"), System.Globalization.CultureInfo.InvariantCulture);
                            DateTime dt = System.Convert.ToDateTime(reader.GetValue("DateTime"));*/

                            Models.Tag tag = new Models.Tag()
                            {
                                name = reader.GetValue(0).ToString(),
                                dt = System.Convert.ToDateTime(reader.GetValue(1)),
                                val = System.Convert.ToDouble(reader.GetValue(2), System.Globalization.CultureInfo.InvariantCulture)
                            };
                            results.Add(tag);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
            }

            return results;
        }

        public Dictionary<string, List<Models.Alarm>> GetAlarms(string[] tags, DateTime dateFrom, DateTime dateTo)
        {
            Dictionary<string, List<Models.Alarm>> results = new Dictionary<string, List<Models.Alarm>>();

            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            string SQL = $"Select ID, EventTime, Source_ProcessVariable, Alarm_Condition, Alarm_LimitString, ValueString," +
                $"Alarm_Type, Comment, Source_ConditionVariable From Events Where Source_ProcessVariable In('{string.Join("','", tags)}')" +
                $" And EventTime >= '{dateFrom.ToString("yyyy-MM-dd HH:mm:ss")}' And EventTime <= '{dateTo.ToString("yyyy-MM-dd HH:mm:ss")}' " +
                $"And [Type] In ('Alarm.Set')";
            try
            {
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string id = reader.GetValue(0).ToString();
                            DateTime dt = System.Convert.ToDateTime(reader.GetValue(1), System.Globalization.CultureInfo.InvariantCulture);
                            string proc = reader.GetValue(2).ToString();
                            string cond = reader.GetValue(3).ToString();
                            string limit = reader.GetValue(4).ToString();
                            string val = reader.GetValue(5).ToString();
                            string type = reader.GetValue(6).ToString();
                            string comment = reader.GetValue(7).ToString();
                            string source = reader.GetValue(8).ToString();
                            if (!results.ContainsKey(proc))
                            {
                                Models.Alarm t = new Models.Alarm()
                                {
                                    id = id,
                                    eventTime = dt,
                                    sourceProcess = proc,
                                    condition = cond,
                                    limit = limit,
                                    value = val,
                                    type = type,
                                    comment = comment,
                                    sourceCondition = source
                                };
                                results.Add(proc, new List<Models.Alarm>() { t });
                            }
                            else
                            {
                                Models.Alarm t = new Models.Alarm()
                                {
                                    id = id,
                                    eventTime = dt,
                                    sourceProcess = proc,
                                    condition = cond,
                                    limit = limit,
                                    value = val,
                                    type = type,
                                    comment = comment,
                                    sourceCondition = source
                                };
                                results[proc].Add(t);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
            }

            return results;
        }

        public List<Models.ShopReport> GetShopReports(string shop, DateTime dateFrom, DateTime dateTo)
        {
            List<Models.ShopReport> results = new List<Models.ShopReport>();

            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            string SQL = $"Select TagName, FORMAT(DateTime, 'yyyy-MM-dd hh:00:00') as DateTime, vValue From History Where TagName = '{shop}.Options'" + 
                $" And DateTime >= '{dateFrom.ToString("yyyy-MM-dd HH:mm:ss")}' And DateTime <= '{dateTo.ToString("yyyy-MM-dd HH:mm:ss")}'" +
                $" And wwQualityRule = 'OPTIMISTIC'" +
                $" Order By DateTime";
            try
            {
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Models.ShopReport report = new Models.ShopReport()
                            {
                                name = reader.GetValue(0).ToString(),
                                dt = System.Convert.ToDateTime(reader.GetValue(1)),
                                text = reader.GetValue(2).ToString()
                            };
                            results.Add(report);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
            }
            var ret = results.GroupBy(
                p => p.dt,
                (key, g) => new Models.ShopReport()
                {
                    dt = key,
                    name = g.Select(x => x.name).FirstOrDefault(),
                    text = string.Join("", g.Select(x => x.text))
                }).ToList<Models.ShopReport>();
            return ret;
        }

        public Dictionary<DateTime, List<Models.Alarm>> GetAlarms2(DateTime dateFrom, DateTime dateTo)
        {
            Dictionary<DateTime, List<Models.Alarm>> results = new Dictionary<DateTime, List<Models.Alarm>>();

            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            string SQL = $"Select DateTime, vValue From History Where TagName ='DispMessages.SMS_text' AND wwQualityRule = 'OPTIMISTIC'" +
                $" And DateTime Between '{dateFrom.ToString("yyyy-MM-dd HH:mm:ss")}' And '{dateTo.ToString("yyyy-MM-dd HH:mm:ss")}'";
            try
            {
                using (var cmd = new SqlCommand(SQL, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DateTime dt = System.Convert.ToDateTime(reader.GetValue(0), System.Globalization.CultureInfo.InvariantCulture);
                            string text = reader.GetValue(1).ToString();
                            if (string.IsNullOrEmpty(text))
                                continue;
                            DateTime msg_dt = System.Convert.ToDateTime(text.Substring(0, 19), System.Globalization.CultureInfo.InvariantCulture);
                            string msg_text = text.Substring(24);
                            if (msg_dt.Date != dt.Date)
                                continue;
                            if (!results.ContainsKey(dt.Date))
                            {
                                Models.Alarm t = new Models.Alarm()
                                {
                                    eventTime = msg_dt,
                                    comment = msg_text
                                };
                                results.Add(dt.Date, new List<Models.Alarm>() { t });
                            }
                            else
                            {
                                Models.Alarm t = new Models.Alarm()
                                {
                                    eventTime = msg_dt,
                                    comment = msg_text
                                };
                                results[dt.Date].Add(t);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
            }

            return results;
        }
    }
}
