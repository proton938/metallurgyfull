﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtoMES.Controllers.GalaxyObjects
{
    public class Area
    {
        int id = 0;
        string tagName = null;
        string shortDesc = null;
        string securityGroup = null;
        string area = null;         // ref
        string container = null;    // ref
        string basedOn = null;
        string containedName = null;
        string hierarchicalName = null;
        string abbreviation = null;

        string mnemoDesc = null;
        string mnemoType = null;
        string mnemoRef = null;

        List<Area> areas = new List<Area>();
        List<Attribute> attributes = new List<Attribute>();

        #region Properties

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string TagName
        {
            get { return tagName; }
            set { tagName = value; }
        }

        public string ShortDesc
        {
            get { return shortDesc; }
            set { shortDesc = value; }
        }

        public string SecurityGroup
        {
            get { return securityGroup; }
            set { securityGroup = value; }
        }

        public string Parent
        {
            get { return area; }
            set { area = value; }
        }

        public string Container
        {
            get { return container; }
            set { container = value; }
        }

        public string BasedOn
        {
            get { return basedOn; }
            set { basedOn = value; }
        }

        public string ContainedName
        {
            get { return containedName; }
            set { containedName = value; }
        }

        public string HierarchicalName
        {
            get { return hierarchicalName; }
            set { hierarchicalName = value; }
        }

        public string Abbreviation
        {
            get { return abbreviation; }
            set { abbreviation = value; }
        }

        public string MnemoDesc
        {
            get { return mnemoDesc; }
            set { mnemoDesc = value; }
        }

        public string MnemoType
        {
            get { return mnemoType; }
            set { mnemoType = value; }
        }

        public string MnemoRef
        {
            get { return mnemoRef; }
            set { mnemoRef = value; }
        }

        #endregion

        public List<Area> Areas
        {
            get
            {
                return areas;
            }
            set
            {
                if (areas == null)
                    areas = new List<Area>();
                areas = value;
            }
        }

        public List<Attribute> Attributes
        {
            get
            {
                return attributes;
            }
            set
            {
                if (attributes == null)
                    attributes = new List<Attribute>();
                attributes = value;
            }
        }
    }
}
