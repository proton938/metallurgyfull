﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using ArchestrA.GRAccess;
using System.ServiceModel;

namespace ProtoMES.Controllers
{
    /// <summary>
    /// Pattern Singleton
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Singleton<T> where T : class
    {
        protected Singleton() { }

        private sealed class SingletonCreator<S> where S : class
        {
            private static readonly S instance = (S)typeof(S).GetConstructor(
                        BindingFlags.Instance | BindingFlags.NonPublic,
                        null,
                        new Type[0],
                        new ParameterModifier[0]).Invoke(null);

            public static S CreatorInstance
            {
                get { return instance; }
            }
        }

        public static T Instance
        {
            get { return SingletonCreator<T>.CreatorInstance; }
        }
    }

    public class Galaxy : Singleton<Galaxy>
    {
        private static readonly object _loc_session = new object();

        private Galaxy() { }

        private IGalaxy galaxy = null;

        public IGalaxy IGalaxy
        {
            get
            {
                if (galaxy == null)
                {
                    if (Environment.MachineName == "DESKTOP-2HK92V0")
                    {
                        nodeName = Environment.MachineName;
                        user = @"DESKTOP-2HK92V0\intma"; //DESKTOP-2HK92V0\ "";//Administrator
                        password = "intma";// "";
                    }
                    galaxy = LogonGalaxy(nodeName, galaxyName, user, password);
                }
                return galaxy;
            }
        }

        string nodeName = "ZN-WWSP-01";
        string galaxyName = "Zinc";
        string user = @"ZN-WWSP-01\Administrator";
        string password = "Qwerty1!";

        private Dictionary<string, List<string>> userGroups = new Dictionary<string, List<string>>();

        public Dictionary<string, List<string>> UserGroups
        {
            get
            {
                return userGroups;
            }
        }

        public IGalaxy LogonGalaxy(string node, string name, string userId, string password)
        {
            try
            {
                // create GRAccessAppClass object
                //GRAccess grAccess = new GRAccess();
                var grAccess = new GRAccessAppClass();

                // try to get galaxy
                IGalaxies gals = grAccess.QueryGalaxies(node);
                if (gals == null || grAccess.CommandResult.Successful == false)
                {
                    throw new Exception(grAccess.CommandResult.CustomMessage + grAccess.CommandResult.Text);
                }
                galaxy = gals[name];
                galaxy.LoginEx(userId, password, true);
                //string s = grAccess.CommandResult.CustomMessage + " " + grAccess.CommandResult.Text;
                //galaxy.LoginEx("", "", true);

                /*IgObjects objs = ProtoMES.Controllers.Galaxy.Instance.IGalaxy.QueryObjects(EgObjectIsTemplateOrInstance.gObjectIsInstance, EConditionType.basedOn,
    "$Area", EMatch.MatchCondition);
                foreach (IgObject obj in objs)
                {
                    ProtoMES.Controllers.GalaxyObjects.Area area = new ProtoMES.Controllers.GalaxyObjects.Area();

                    IAttributes attrs = obj.Attributes;
                    foreach (IAttribute attr in attrs)
                    {
                        string a = attr.Name;
                    }
                }*/

                        // security
                        //GetGalaxySecurity();
                    }
            catch (Exception e)
            {
                throw new Exception(string.Format("Ошибка получения галактики объектов, {0}", e.Message));
            }

            return galaxy;
        }

        public void LogoffGalaxy(string userId)
        {
            if (galaxy != null)
                galaxy.Logout();
        }

        public void GetGalaxySecurity()
        {
            try
            {
                IGalaxySecurity security = galaxy.GetReadOnlySecurity();
                if (security.UsersAvailable != null)
                    foreach (IGalaxyUser user in security.UsersAvailable)
                    {
                        List<string> groups = new List<string>();
                        int i = 1;
                        if (user.AssociatedRoles != null)
                            foreach (IGalaxyRole role in user.AssociatedRoles)
                            {
                                /*if(role.Permissions != null)
                                    foreach (IPermission permission in role.Permissions)
                                        groups.Add(permission.SecurityGroup.GroupName);*/
                                if (role.OperationalPermissions != null)
                                    foreach (IPermission permission in role.OperationalPermissions)
                                        groups.Add(permission.SecurityGroup.GroupName);
                            }
                        groups = groups.Where(x => x != "Default").ToList();
                        if (groups.Count != 0)
                            userGroups.Add(user.FullName, groups);
                    }
            }
            catch (Exception ex)
            {
                userGroups.Add(ex.Message, null);
            }
        }
    }
}
