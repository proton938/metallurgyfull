import { Injectable } from '@angular/core';

@Injectable()

export class TreeParams {

    public getTreeParametersUrl: string = '';   // строка get-запроса на дерево параметров

    public getArraySelectedPrms: any[] = [];    // массив выбранных параметров

    public getSelectedParameters: any = [];     // список id выделенных check-ов для возврата их выделения при возврате с другого раздела

    public getTreeParameters: any[] = [];

    public markRemoveDateTime: number = 0;        // отметка изменения даты и времени (включая сенсорное масштабирования графика)
    public markRemoveSelectedPrms: number = 0;    // отметка изменения выбора параметров


    toggleMarkRemoveDateTime() {       // переключаем - отметка изменения даты и времени true
        this.markRemoveDateTime = 1;
    }

    unToggleMarkRemoveDateTime() {
        this.markRemoveDateTime = 0;
    }

    toggleMarkRemoveSelectedPrms() {
        this.markRemoveSelectedPrms = 1;
    }

    unToggleMarkRemoveSelectedPrms() {
        this.markRemoveSelectedPrms = 0;
    }


    updateUrlModel() {
        this.getTreeParametersUrl = '';      // обновляем url строку get-запроса в модели
    }

    updateSelectedPrmsModel() {
        this.updateUrlModel();
        this.getArraySelectedPrms = [];      // опустошаем в модели массив выбранных параметров
        this.getSelectedParameters = [];     // опустошаем в модели массив id выделенных check-ов дерева параметров
    }

    public startDateTime: string;     // дата + время начала
    public stopDateTime: string;      // дата + время завершения

    public zoomMemory: any = [];

    public arrayColoring: any = [
        '#ff0000',
        '#008000',
        '#ff6600',
        '#008080',
        '#ffcc00',
        '#0000ff',
        '#800080',
        '#89a02c',
        '#9955ff',
        '#d38d5f',
        '#ff00ff',
        '#00d4aa',
        '#003380',
        '#d42aff',
        '#ff0000',
        '#008000',
        '#ff6600',
        '#008080',
        '#ffcc00',
        '#0000ff',
        '#800080',
        '#89a02c',
        '#9955ff',
        '#d38d5f',
        '#ff00ff',
        '#00d4aa',
        '#003380',
        '#d42aff'
    ];

  // для подвала
  user: any;
  arrAlarms: any[];


    allTreeParams: any[] = [{

        'Chel_Zinc_Plant': [
            {
                "attributes": [
                    {
                        "description": "Загружено шихты общее",
                        "engUnits": "т",
                        "name": "F_sum_obsh",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Загружено шихты за смену",
                        "engUnits": "т",
                        "name": "F_sum_sm",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Разрежение",
                        "engUnits": "Па",
                        "name": "P_kol_g_i",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Chel_Zinc_Plant.Roasting_Shop",
                "parent": "Chel_Zinc_Plant",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Обжиговый цех",
                "tagName": "Roasting_Shop"
            },
            {
                "attributes": [
                    {
                        "description": "Разрежение",
                        "engUnits": "Па",
                        "name": "P_kol_g_i",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Уровень кислоты в баке 7 ",
                        "engUnits": "",
                        "name": "S07L",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Уровень кислоты в баке 8",
                        "engUnits": "",
                        "name": "S08L",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Уровень кислоты в баке 9",
                        "engUnits": "",
                        "name": "S09L",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Уровень кислоты в баке 10",
                        "engUnits": "",
                        "name": "S10L",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура на 2 т.т.",
                        "engUnits": "",
                        "name": "T2_tt",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Свободный объем",
                        "engUnits": "т",
                        "name": "Vmng",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Свободный объем 2 сорт",
                        "engUnits": null,
                        "name": "Vmng2",
                        "type": 5,
                        "value": "Свободный объем 2 сорт",
                        "values": null
                    }
                ],
                "hierarchicalName": "Chel_Zinc_Plant.Sulfuric_Acid_Shop",
                "parent": "Chel_Zinc_Plant",
                "securityGroup": "Default",
                "shortDesc": "Сернокислотный цех",
                "tagName": "Sulfuric_Acid_Shop"
            },
            {
                "attributes": [
                    {
                        "description": "Расход вельцокиси",
                        "engUnits": "м3/ч",
                        "name": "RVO",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Конвеер 26",
                        "engUnits": "м3/ч",
                        "name": "Cr26",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Конвеер 27",
                        "engUnits": "м3/ч",
                        "name": "Kr27",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Конвеер 36",
                        "engUnits": "м3/ч",
                        "name": "Kr36",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Chel_Zinc_Plant.Waelz_Shop",
                "parent": "Chel_Zinc_Plant",
                "securityGroup": "Default",
                "shortDesc": "Вельц цех",
                "tagName": "Waelz_Shop"
            },
            {
                "attributes": [
                    {
                        "description": "На смыв pH",
                        "engUnits": "%",
                        "name": "Ph.washout",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Из обжига pH",
                        "engUnits": "%",
                        "name": "Ph.Roast",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "ВСГ pH",
                        "engUnits": "%",
                        "name": "Ph.VSG",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "нейтр. pH",
                        "engUnits": "%",
                        "name": "Ph.Neutral",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "ВСНС pH",
                        "engUnits": "%",
                        "name": "Ph.VSNS",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "ВВС-6 pH",
                        "engUnits": "%",
                        "name": "Ph.VVS_6",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Chel_Zinc_Plant.Leaching_Shop",
                "parent": "Chel_Zinc_Plant",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Вышелачивательный цех",
                "tagName": "Leaching_Shop"
            },
            {
                "attributes": [
                    {
                        "description": "Ph Поступающий ц-вый э-лит, 1 серия",
                        "engUnits": "",
                        "name": "Ph_In_Zn_Electrolyte_1",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Ph Поступающий ц-вый э-лит, 2 серия",
                        "engUnits": "",
                        "name": "Ph_In_Zn_Electrolyte_2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Ph Отработанный ц-вый э-лит, ряд A",
                        "engUnits": "",
                        "name": "Ph_Waste_Zn_Electrolyte_A",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Ph Отработанный ц-вый э-лит, ряд B",
                        "engUnits": "",
                        "name": "Ph_Waste_Zn_Electrolyte_B",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Ph Отработанный ц-вый э-лит, ряд C",
                        "engUnits": "",
                        "name": "Ph_Waste_Zn_Electrolyte_C",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Ph Отработанный ц-вый э-лит, ряд D",
                        "engUnits": "",
                        "name": "Ph_Waste_Zn_Electrolyte_D",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Zn Поступающий ц-вый э-лит, 1 серия",
                        "engUnits": "",
                        "name": "Zn_In_Zn_Electrolyte_1",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Zn Поступающий ц-вый э-лит, 2 серия",
                        "engUnits": "",
                        "name": "Zn_In_Zn_Electrolyte_2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Zn Отработанный ц-вый э-лит, ряд A",
                        "engUnits": "",
                        "name": "Zn_Waste_Zn_Electrolyte_A",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Zn Отработанный ц-вый э-лит, ряд B",
                        "engUnits": "",
                        "name": "Zn_Waste_Zn_Electrolyte_B",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Zn Отработанный ц-вый э-лит, ряд C",
                        "engUnits": "",
                        "name": "Zn_Waste_Zn_Electrolyte_C",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Zn Отработанный ц-вый э-лит, ряд D",
                        "engUnits": "",
                        "name": "Zn_Waste_Zn_Electrolyte_D",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Chel_Zinc_Plant.Zinc_Electrolysis_Shop",
                "parent": "Chel_Zinc_Plant",
                "securityGroup": "Default",
                "shortDesc": "Комплекс Электролиза Цинка",
                "tagName": "Zinc_Electrolysis_Shop"
            },
            {
                "attributes": [
                    {
                        "description": "Вцс - восток - уровень ",
                        "engUnits": "",
                        "name": "Level_vcs_east",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Вцс - запад - уровень ",
                        "engUnits": "",
                        "name": "Level_vcs_west",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Всс1 - север - расход ",
                        "engUnits": "м3/ч",
                        "name": "Q_vss1_north",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Всс3 - север - расход ",
                        "engUnits": "м3/ч",
                        "name": "Q_vss3_north",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Всс3 - юг - расход ",
                        "engUnits": "м3/ч",
                        "name": "Q_vss3_south",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "ПСО север",
                        "engUnits": "см",
                        "name": "PSO_north.Level",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "ПСО юг",
                        "engUnits": "см",
                        "name": "PSO_south.Level",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Chel_Zinc_Plant.Hydro_Metallurgical_Shop",
                "parent": "Chel_Zinc_Plant",
                "securityGroup": "Default",
                "shortDesc": "Гидрометаллургический цех",
                "tagName": "Hydro_Metallurgical_Shop"
            },
            {
                "attributes": [
                    {
                        "description": "Расход кислорода",
                        "engUnits": "м3/ч",
                        "name": "Q_O2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход воды",
                        "engUnits": "т/ч",
                        "name": "Q_water",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход газа",
                        "engUnits": "м3/ч",
                        "name": "Q_gas",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Chel_Zinc_Plant.Energy_Shop",
                "parent": "Chel_Zinc_Plant",
                "securityGroup": "Default",
                "shortDesc": "Энергоцех",
                "tagName": "Energy_Shop"
            },
            {
                "attributes": [
                ],
                "hierarchicalName": "Chel_Zinc_Plant.Electrical_Shop",
                "parent": "Chel_Zinc_Plant",
                "securityGroup": "Default",
                "shortDesc": "Электротехнический цех",
                "tagName": "Electrical_Shop"
            }
        ],

        'Roasting_Shop': [
            {
                "attributes": [
                    {
                        "description": "Давление под подиной 1",
                        "engUnits": "кПа",
                        "name": "P1",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Давление под подиной 2",
                        "engUnits": "кПа",
                        "name": "P2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход воздуха",
                        "engUnits": "м3/час",
                        "name": "Q_air",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход O2",
                        "engUnits": "м3/час",
                        "name": "Q_O2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура перед экструдером",
                        "engUnits": "°C",
                        "name": "T_exg",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кипящего слоя",
                        "engUnits": "°C",
                        "name": "T_ks",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура северный циклон",
                        "engUnits": "°C",
                        "name": "T_n",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кипящего слоя",
                        "engUnits": "°C",
                        "name": "T_podsv",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура южный циклон",
                        "engUnits": "°C",
                        "name": "T_s",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура в стояке",
                        "engUnits": "°C",
                        "name": "T_st",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Oven1",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Обжиговая печь №1",
                "tagName": "Roast_Oven1"
            },
            {
                "attributes": [
                    {
                        "description": "Давление под подиной 1",
                        "engUnits": "кПа",
                        "name": "P1",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Давление под подиной 2",
                        "engUnits": "кПа",
                        "name": "P2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход воздуха",
                        "engUnits": "м3/час",
                        "name": "Q_air",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход O2",
                        "engUnits": "м3/час",
                        "name": "Q_O2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура перед экструдером",
                        "engUnits": "°C",
                        "name": "T_exg",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кипящего слоя",
                        "engUnits": "°C",
                        "name": "T_ks",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура северный циклон",
                        "engUnits": "°C",
                        "name": "T_n",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кипящего слоя",
                        "engUnits": "°C",
                        "name": "T_podsv",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура южный циклон",
                        "engUnits": "°C",
                        "name": "T_s",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура в стояке",
                        "engUnits": "°C",
                        "name": "T_st",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Oven2",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Обжиговая печь №2",
                "tagName": "Roast_Oven2"
            },
            {
                "attributes": [
                    {
                        "description": "Давление под подиной 1",
                        "engUnits": "кПа",
                        "name": "P1",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Давление под подиной 2",
                        "engUnits": "кПа",
                        "name": "P2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход воздуха",
                        "engUnits": "м3/час",
                        "name": "Q_air",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход O2",
                        "engUnits": "м3/час",
                        "name": "Q_O2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура перед экструдером",
                        "engUnits": "°C",
                        "name": "T_exg",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кипящего слоя",
                        "engUnits": "°C",
                        "name": "T_ks",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура северный циклон",
                        "engUnits": "°C",
                        "name": "T_n",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кипящего слоя",
                        "engUnits": "°C",
                        "name": "T_podsv",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура южный циклон",
                        "engUnits": "°C",
                        "name": "T_s",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура в стояке",
                        "engUnits": "°C",
                        "name": "T_st",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Oven3",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Обжиговая печь №3",
                "tagName": "Roast_Oven3"
            },
            {
                "attributes": [
                    {
                        "description": "Давление под подиной 1",
                        "engUnits": "кПа",
                        "name": "P1",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Давление под подиной 2",
                        "engUnits": "кПа",
                        "name": "P2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход воздуха",
                        "engUnits": "м3/час",
                        "name": "Q_air",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход O2",
                        "engUnits": "м3/час",
                        "name": "Q_O2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура перед экструдером",
                        "engUnits": "°C",
                        "name": "T_exg",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кипящего слоя",
                        "engUnits": "°C",
                        "name": "T_ks",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура северный циклон",
                        "engUnits": "°C",
                        "name": "T_n",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кипящего слоя",
                        "engUnits": "°C",
                        "name": "T_podsv",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура южный циклон",
                        "engUnits": "°C",
                        "name": "T_s",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура в стояке",
                        "engUnits": "°C",
                        "name": "T_st",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Oven4",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Обжиговая печь №4",
                "tagName": "Roast_Oven4"
            },
            {
                "attributes": [
                    {
                        "description": "Давление под подиной 1",
                        "engUnits": "кПа",
                        "name": "P1",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Давление под подиной 2",
                        "engUnits": "кПа",
                        "name": "P2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход воздуха",
                        "engUnits": "м3/час",
                        "name": "Q_air",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход O2",
                        "engUnits": "м3/час",
                        "name": "Q_O2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура перед экструдером",
                        "engUnits": "°C",
                        "name": "T_exg",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кипящего слоя",
                        "engUnits": "°C",
                        "name": "T_ks",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура северный циклон",
                        "engUnits": "°C",
                        "name": "T_n",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кипящего слоя",
                        "engUnits": "°C",
                        "name": "T_podsv",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура южный циклон",
                        "engUnits": "°C",
                        "name": "T_s",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура в стояке",
                        "engUnits": "°C",
                        "name": "T_st",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Oven5",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Обжиговая печь №5",
                "tagName": "Roast_Oven5"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Pump1",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Насос циркуляции обжига 1",
                "tagName": "Roast_Pump1"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Pump2",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Насос циркуляции обжига 2",
                "tagName": "Roast_Pump2"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Pump3",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Насос циркуляции обжига 3",
                "tagName": "Roast_Pump3"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Pump4",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Насос циркуляции обжига 4",
                "tagName": "Roast_Pump4"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Pump5",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Насос циркуляции обжига 5",
                "tagName": "Roast_Pump5"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Pump6",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Насос циркуляции обжига 6",
                "tagName": "Roast_Pump6"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Pump7",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Насос циркуляции обжига 7",
                "tagName": "Roast_Pump7"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Pump8",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Насос циркуляции обжига 8",
                "tagName": "Roast_Pump8"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_Pump12",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Насос циркуляции обжига 12",
                "tagName": "Roast_Pump12"
            },
            {
                "attributes": [
                    {
                        "description": "Входная температура",
                        "engUnits": "°C",
                        "name": "T_in",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Выходная температура",
                        "engUnits": "°C",
                        "name": "T_out",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_GasCleaning1",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Газоочистка 1",
                "tagName": "Roast_GasCleaning1"
            },
            {
                "attributes": [
                    {
                        "description": "Входная температура",
                        "engUnits": "°C",
                        "name": "T_in",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Выходная температура",
                        "engUnits": "°C",
                        "name": "T_out",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_GasCleaning2",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Газоочистка 2",
                "tagName": "Roast_GasCleaning2"
            },
            {
                "attributes": [
                    {
                        "description": "Входная температура",
                        "engUnits": "°C",
                        "name": "T_in",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Выходная температура",
                        "engUnits": "°C",
                        "name": "T_out",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_GasCleaning3",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Газоочистка 3",
                "tagName": "Roast_GasCleaning3"
            },
            {
                "attributes": [
                    {
                        "description": "Входная температура",
                        "engUnits": "°C",
                        "name": "T_in",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Выходная температура",
                        "engUnits": "°C",
                        "name": "T_out",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_GasCleaning4",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Газоочистка 4",
                "tagName": "Roast_GasCleaning4"
            },
            {
                "attributes": [
                    {
                        "description": "Входная температура",
                        "engUnits": "°C",
                        "name": "T_in",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Выходная температура",
                        "engUnits": "°C",
                        "name": "T_out",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Roast_GasCleaning7",
                "parent": "Roasting_Shop",
                "securityGroup": "Roasting_Group",
                "shortDesc": "Газоочистка 7",
                "tagName": "Roast_GasCleaning7"
            }
        ],

        'Sulfuric_Acid_Shop': [
            {
                "attributes": [
                    {
                        "description": "Концентрация газа",
                        "engUnits": "%",
                        "name": "GasConcentration",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Газовая нагрузка",
                        "engUnits": "м3/час",
                        "name": "GasLoad",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Концентрация SO2 в атмосферу",
                        "engUnits": "%",
                        "name": "SO2Concentration",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура газа перед сушкой",
                        "engUnits": "°С",
                        "name": "Tg_sb",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура сборника мга1 ",
                        "engUnits": "°С",
                        "name": "Tk_mga1",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кислоты сушильной",
                        "engUnits": "°С",
                        "name": "Tk_sb",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "SAS_System3",
                "parent": "Sulfuric_Acid_Shop",
                "securityGroup": "Default",
                "shortDesc": "СКЦ Система 3",
                "tagName": "SAS_System3"
            },
            {
                "attributes": [
                    {
                        "description": "Концентрация газа",
                        "engUnits": "%",
                        "name": "GasConcentration",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Газовая нагрузка",
                        "engUnits": "м3/час",
                        "name": "GasLoad",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Концентрация SO2 в атмосферу",
                        "engUnits": "%",
                        "name": "SO2Concentration",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура газа перед сушкой",
                        "engUnits": "°С",
                        "name": "Tg_sb",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура сборника мга1 ",
                        "engUnits": "°С",
                        "name": "Tk_mga1",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кислоты сушильной",
                        "engUnits": "°С",
                        "name": "Tk_sb",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "SAS_System4",
                "parent": "Sulfuric_Acid_Shop",
                "securityGroup": "Default",
                "shortDesc": "СКЦ Система 4",
                "tagName": "SAS_System4"
            },
            {
                "attributes": [
                    {
                        "description": "Концентрация газа",
                        "engUnits": "%",
                        "name": "GasConcentration",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Газовая нагрузка",
                        "engUnits": "м3/час",
                        "name": "GasLoad",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Концентрация SO2 в атмосферу",
                        "engUnits": "%",
                        "name": "SO2Concentration",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура газа перед сушкой",
                        "engUnits": "°С",
                        "name": "Tg_sb",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура сборника мга1 ",
                        "engUnits": "°С",
                        "name": "Tk_mga1",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура кислоты сушильной",
                        "engUnits": "°С",
                        "name": "Tk_sb",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "SAS_System5",
                "parent": "Sulfuric_Acid_Shop",
                "securityGroup": "Default",
                "shortDesc": "СКЦ Система 5",
                "tagName": "SAS_System5"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "SAS_Pump1",
                "parent": "Sulfuric_Acid_Shop",
                "securityGroup": "Default",
                "shortDesc": "Нагнетатель ККК-5",
                "tagName": "SAS_Pump1"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "SAS_Pump2",
                "parent": "Sulfuric_Acid_Shop",
                "securityGroup": "Default",
                "shortDesc": "Нагнетатель ККК-4",
                "tagName": "SAS_Pump2"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "SAS_Pump3",
                "parent": "Sulfuric_Acid_Shop",
                "securityGroup": "Default",
                "shortDesc": "Нагнетатель ТК-3",
                "tagName": "SAS_Pump3"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "SAS_Pump4",
                "parent": "Sulfuric_Acid_Shop",
                "securityGroup": "Default",
                "shortDesc": "Нагнетатель ККК-2",
                "tagName": "SAS_Pump4"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "SAS_Pump5",
                "parent": "Sulfuric_Acid_Shop",
                "securityGroup": "Default",
                "shortDesc": "Нагнетатель ТК-2а",
                "tagName": "SAS_Pump5"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "SAS_Pump6",
                "parent": "Sulfuric_Acid_Shop",
                "securityGroup": "Default",
                "shortDesc": "Нагнетатель ККК-1",
                "tagName": "SAS_Pump6"
            }
        ],

        'Waelz_Shop': [
            {
                "attributes": [
                    {
                        "description": "Горелка",
                        "engUnits": "",
                        "name": "Burner",
                        "type": 1,
                        "value": null,
                        "values": null
                    },
                    {
                        "description": "Выработка пара",
                        "engUnits": "т/ч",
                        "name": "Q_steam",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура верхней головки",
                        "engUnits": "°C",
                        "name": "T_vg",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Загрузка шихты",
                        "engUnits": "т/ч",
                        "name": "Load",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "KVP_1",
                "parent": "Old_Waelz_Shop",
                "securityGroup": "Default",
                "shortDesc": "Печь КВП1",
                "tagName": "KVP_1"
            },
            {
                "attributes": [
                    {
                        "description": "Горелка",
                        "engUnits": "",
                        "name": "Burner",
                        "type": 1,
                        "value": null,
                        "values": null
                    },
                    {
                        "description": "Выработка пара",
                        "engUnits": "т/ч",
                        "name": "Q_steam",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура верхней головки",
                        "engUnits": "°C",
                        "name": "T_vg",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Загрузка шихты",
                        "engUnits": "т/ч",
                        "name": "Load",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "KVP_2",
                "parent": "Old_Waelz_Shop",
                "securityGroup": "Default",
                "shortDesc": "Печь КВП2",
                "tagName": "KVP_2"
            },
            {
                "attributes": [
                    {
                        "description": "Горелка",
                        "engUnits": "",
                        "name": "Burner",
                        "type": 1,
                        "value": null,
                        "values": null
                    },
                    {
                        "description": "Выработка пара",
                        "engUnits": "т/ч",
                        "name": "Q_steam",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура верхней головки",
                        "engUnits": "°C",
                        "name": "T_vg",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Загрузка шихты",
                        "engUnits": "т/ч",
                        "name": "Load",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "KVP_3",
                "parent": "Old_Waelz_Shop",
                "securityGroup": "Default",
                "shortDesc": "Печь КВП3",
                "tagName": "KVP_3"
            },
            {
                "attributes": [
                    {
                        "description": "Горелка",
                        "engUnits": "",
                        "name": "Burner",
                        "type": 1,
                        "value": null,
                        "values": null
                    },
                    {
                        "description": "Выработка пара",
                        "engUnits": "т/ч",
                        "name": "Q_steam",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура верхней головки",
                        "engUnits": "°C",
                        "name": "T_vg",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Загрузка шихты",
                        "engUnits": "т/ч",
                        "name": "Load",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "KVP_4",
                "parent": "Old_Waelz_Shop",
                "securityGroup": "Default",
                "shortDesc": "Печь КВП4",
                "tagName": "KVP_4"
            },
            {
                "attributes": [
                    {
                        "description": "Горелка",
                        "engUnits": "",
                        "name": "Burner",
                        "type": 1,
                        "value": null,
                        "values": null
                    },
                    {
                        "description": "Выработка пара",
                        "engUnits": "т/ч",
                        "name": "Q_steam",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура верхней головки",
                        "engUnits": "°C",
                        "name": "T_vg",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Загрузка шихты",
                        "engUnits": "т/ч",
                        "name": "Load",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "KVP_5",
                "parent": "New_Waelz_Shop",
                "securityGroup": "Default",
                "shortDesc": "Печь КВП5",
                "tagName": "KVP_5"
            },
            {
                "attributes": [
                    {
                        "description": "Горелка",
                        "engUnits": "",
                        "name": "Burner",
                        "type": 1,
                        "value": null,
                        "values": null
                    },
                    {
                        "description": "Выработка пара",
                        "engUnits": "т/ч",
                        "name": "Q_steam",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Температура верхней головки",
                        "engUnits": "°C",
                        "name": "T_vg",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Загрузка шихты",
                        "engUnits": "т/ч",
                        "name": "Load",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "KVP_6",
                "parent": "New_Waelz_Shop",
                "securityGroup": "Default",
                "shortDesc": "Печь КВП6",
                "tagName": "KVP_6"
            },
            {
                "attributes": [
                    {
                        "description": "Ф/П в работе",
                        "engUnits": "",
                        "name": "Work",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Waelz_Larox1",
                "parent": "New_Waelz_Shop",
                "securityGroup": "Default",
                "shortDesc": "Прессфилтр Larox №1",
                "tagName": "Waelz_Larox1"
            },
            {
                "attributes": [
                    {
                        "description": "Ф/П в работе",
                        "engUnits": "",
                        "name": "Work",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Waelz_Larox2",
                "parent": "New_Waelz_Shop",
                "securityGroup": "Default",
                "shortDesc": "Прессфилтр Larox №2",
                "tagName": "Waelz_Larox2"
            }
        ],

        'Leaching_Shop': [
            {
                "attributes": [
                {
                  "description": "ОЭ по цеху",
                  "engUnits": "%",
                  "name": "Value",
                  "type": 3,
                  "value": "0",
                  "values": null
                }
                ],
                "hierarchicalName": "Vysh_OE",
                "parent": "Sulfuric_Acid_Shop",
                "securityGroup": "Default",
                "shortDesc": "ОЭ по цеху",
                "tagName": "Vysh_OE"
            },
            {
                "attributes": [
                    {
                        "description": "Проток",
                        "engUnits": "",
                        "name": "Duct",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Leach_Thickener1",
                "parent": "Leaching_Shop",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Сгуститель 1",
                "tagName": "Leach_Thickener1"
            },
            {
                "attributes": [
                    {
                        "description": "Проток",
                        "engUnits": "",
                        "name": "Duct",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Leach_Thickener2",
                "parent": "Leaching_Shop",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Сгуститель 2",
                "tagName": "Leach_Thickener2"
            },
            {
                "attributes": [
                    {
                        "description": "Проток",
                        "engUnits": "",
                        "name": "Duct",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Leach_Thickener3",
                "parent": "Leaching_Shop",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Сгуститель 3",
                "tagName": "Leach_Thickener3"
            },
            {
                "attributes": [
                    {
                        "description": "Проток",
                        "engUnits": "",
                        "name": "Duct",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Leach_Thickener4",
                "parent": "Leaching_Shop",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Сгуститель 4",
                "tagName": "Leach_Thickener4"
            },
            {
                "attributes": [
                    {
                        "description": "Проток",
                        "engUnits": "",
                        "name": "Duct",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Leach_Thickener5",
                "parent": "Leaching_Shop",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Сгуститель 5",
                "tagName": "Leach_Thickener5"
            },
            {
                "attributes": [
                    {
                        "description": "Проток",
                        "engUnits": "",
                        "name": "Duct",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Leach_Thickener6",
                "parent": "Leaching_Shop",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Сгуститель 6",
                "tagName": "Leach_Thickener6"
            },
            {
                "attributes": [
                    {
                        "description": "Проток",
                        "engUnits": "",
                        "name": "Duct",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Leach_Thickener7",
                "parent": "Leaching_Shop",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Сгуститель 7",
                "tagName": "Leach_Thickener7"
            },
            {
                "attributes": [
                    {
                        "description": "Проток",
                        "engUnits": "",
                        "name": "Duct",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Leach_Thickener8",
                "parent": "Leaching_Shop",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Сгуститель 8",
                "tagName": "Leach_Thickener8"
            },
            {
                "attributes": [
                    {
                        "description": "Проток",
                        "engUnits": "",
                        "name": "Duct",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Leach_Thickener9",
                "parent": "Leaching_Shop",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Сгуститель 9",
                "tagName": "Leach_Thickener9"
            },
            {
                "attributes": [
                    {
                        "description": "Проток",
                        "engUnits": "",
                        "name": "Duct",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Leach_Thickener10",
                "parent": "Leaching_Shop",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Сгуститель 10",
                "tagName": "Leach_Thickener10"
            },
            {
                "attributes": [
                    {
                        "description": "Проток",
                        "engUnits": "",
                        "name": "Duct",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Leach_Thickener11",
                "parent": "Leaching_Shop",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Сгуститель 11",
                "tagName": "Leach_Thickener11"
            },
            {
                "attributes": [
                    {
                        "description": "Проток",
                        "engUnits": "",
                        "name": "Duct",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "Leach_Thickener12",
                "parent": "Leaching_Shop",
                "securityGroup": "Leaching_Group",
                "shortDesc": "Сгуститель 12",
                "tagName": "Leach_Thickener12"
            }
        ],

        'Zinc_Electrolysis_Shop': [
            {
                "attributes": [
                    {
                        "description": "Уровень",
                        "engUnits": "м",
                        "name": "Level",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Объем ",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "HMS_neutral",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "ГМЦ - чан нейтрального раствора",
                "tagName": "HMS_neutral"
            },
            {
                "attributes": [
                    {
                        "description": "Уровень",
                        "engUnits": "м",
                        "name": "Level",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Объем ",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "HMS_OE",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "ГМЦ - чан отработанного электролита",
                "tagName": "HMS_OE"
            },
            {
                "attributes": [
                    {
                        "description": "Объем ",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Total_HMS",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "ГМЦ всего по цеху",
                "tagName": "Total_HMS"
            },
            {
                "attributes": [
                    {
                        "description": "Уровень",
                        "engUnits": "м",
                        "name": "level",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Объем ",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "VIU_neutral",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "ВИУ - чан нейтрального раствора",
                "tagName": "VIU_neutral"
            },
            {
                "attributes": [
                    {
                        "description": "Уровень",
                        "engUnits": "м",
                        "name": "level",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Объем ",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "VIU_mixed",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "ВИУ - чан смешанного раствора",
                "tagName": "VIU_mixed"
            },
            {
                "attributes": [
                    {
                        "description": "Уровень",
                        "engUnits": "м",
                        "name": "level",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Объем ",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "VIU_OE",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "ВИУ - чан отработанного электролита",
                "tagName": "VIU_OE"
            },
            {
                "attributes": [
                    {
                        "description": "Объем ",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Total_VIU",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "ВИУ - всего",
                "tagName": "Total_VIU"
            },
            {
                "attributes": [
                    {
                        "description": "Объем ",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Total_LS",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "Выщел. всего по цеху",
                "tagName": "Total_LS"
            },
            {
                "attributes": [
                    {
                        "description": "Объем ",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Total_GMO",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "КЭЦ-ГМО всего по цеху",
                "tagName": "Total_GMO"
            },
            {
                "attributes": [
                    {
                        "description": "Объем ",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Total_OE_and_KO",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "КЭЦ - ОЭ и КО всего по цеху",
                "tagName": "Total_OE_and_KO"
            },
            {
                "attributes": [
                    {
                        "description": "Объем ",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Total_CZP",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "Всего по заводу",
                "tagName": "Total_CZP"
            },
            {
                "attributes": [
                    {
                        "description": "Температура",
                        "engUnits": "м3",
                        "name": "Temperarure",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK704A",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "TK704A",
                "tagName": "Tank_TK704A"
            },
            {
                "attributes": [
                    {
                        "description": "Температура",
                        "engUnits": "м3",
                        "name": "Temperarure",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK704B",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "TK704B",
                "tagName": "Tank_TK704B"
            },
            {
                "attributes": [
                    {
                        "description": "Температура",
                        "engUnits": "м3",
                        "name": "Temperarure",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK704C",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "TK704C",
                "tagName": "Tank_TK704C"
            },
            {
                "attributes": [
                    {
                        "description": "Температура",
                        "engUnits": "м3",
                        "name": "Temperarure",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK704D",
                "parent": "Zinc_Electrolysis_Shop",
                "securityGroup": "Default",
                "shortDesc": "TK704D",
                "tagName": "Tank_TK704D"
            }
        ],

        'Solution_Cleaning_Area': [
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK702A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK702A",
                "tagName": "Tank_TK702A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK702B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK702B",
                "tagName": "Tank_TK702B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK702C",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK702C",
                "tagName": "Tank_TK702C"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK702D",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK702D",
                "tagName": "Tank_TK702D"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK510A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK510A",
                "tagName": "Tank_TK510A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK510B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK510B",
                "tagName": "Tank_TK510B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK501A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK501A",
                "tagName": "Tank_TK501A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK501B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK501B",
                "tagName": "Tank_TK501B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK501C",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK501C",
                "tagName": "Tank_TK501C"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_S501A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар S501A",
                "tagName": "Tank_S501A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_S501B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар S501B",
                "tagName": "Tank_S501B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK502A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK502A",
                "tagName": "Tank_TK502A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK502B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK502B",
                "tagName": "Tank_TK502B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK502C",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK502C",
                "tagName": "Tank_TK502C"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK502D",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK502D",
                "tagName": "Tank_TK502D"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_S502B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар S502B",
                "tagName": "Tank_S502B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_S502C",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар S502C",
                "tagName": "Tank_S502C"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK701A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK701A",
                "tagName": "Tank_TK701A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK701B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK701B",
                "tagName": "Tank_TK701B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_S1100",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар S1100",
                "tagName": "Tank_S1100"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK511A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK511A",
                "tagName": "Tank_TK511A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK511B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK511B",
                "tagName": "Tank_TK511B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK513",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK513",
                "tagName": "Tank_TK513"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK1100A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK1100A",
                "tagName": "Tank_TK1100A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK1100B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK1100B",
                "tagName": "Tank_TK1100B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK601",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK601",
                "tagName": "Tank_TK601"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK603A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK603A",
                "tagName": "Tank_TK603A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK603B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK603B",
                "tagName": "Tank_TK603B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK604A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK604A",
                "tagName": "Tank_TK604A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK604B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK604B",
                "tagName": "Tank_TK604B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK605A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK605A",
                "tagName": "Tank_TK605A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK605B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK605B",
                "tagName": "Tank_TK605B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK606A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK606A",
                "tagName": "Tank_TK606A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK606B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK606B",
                "tagName": "Tank_TK606B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK610A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK610A",
                "tagName": "Tank_TK610A"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_TK610B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар TK610B",
                "tagName": "Tank_TK610B"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_S601",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар S601",
                "tagName": "Tank_S601"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_S604",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар S604",
                "tagName": "Tank_S604"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_S605",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар S605",
                "tagName": "Tank_S605"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_S606",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар S606",
                "tagName": "Tank_S606"
            },
            {
                "attributes": [
                    {
                        "description": "Текущий объем резервуара",
                        "engUnits": "м3",
                        "name": "Volume",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Tank_S610",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Резервуар S610",
                "tagName": "Tank_S610"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "ZES_Pump_P510A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Насос P510A",
                "tagName": "ZES_Pump_P510A"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "ZES_Pump_P510B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Насос P510B",
                "tagName": "ZES_Pump_P510B"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "ZES_Pump_P704A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Насос P704A",
                "tagName": "ZES_Pump_P704A"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "ZES_Pump_P704B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Насос P704B",
                "tagName": "ZES_Pump_P704B"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "ZES_Pump_P701A",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Насос P701A",
                "tagName": "ZES_Pump_P701A"
            },
            {
                "attributes": [
                    {
                        "description": "Насос в работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": null,
                        "values": null
                    }
                ],
                "hierarchicalName": "ZES_Pump_P701B",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Насос P701B",
                "tagName": "ZES_Pump_P701B"
            }
        ],

        'Smelting_Area': [
            {
                "attributes": [
                    {
                        "description": "Температура",
                        "engUnits": "°C",
                        "name": "T_ib",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Действительная мощность",
                        "engUnits": "МВт*ч",
                        "name": "MW_ib",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "IB_F801",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Индукционная печь F-801",
                "tagName": "IB_F801"
            },
            {
                "attributes": [
                    {
                        "description": "Температура",
                        "engUnits": "°C",
                        "name": "T_ib",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Действительная мощность",
                        "engUnits": "МВт*ч",
                        "name": "MW_ib",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "IB_F802",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Индукционная печь F-802",
                "tagName": "IB_F802"
            },
            {
                "attributes": [
                    {
                        "description": "Температура",
                        "engUnits": "°C",
                        "name": "T_ib",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Действительная мощность",
                        "engUnits": "МВт*ч",
                        "name": "MW_ib",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "IB_F803",
                "parent": "Solution_Cleaning_Area",
                "securityGroup": "Default",
                "shortDesc": "Индукционная печь F-803",
                "tagName": "IB_F803"
            }
        ],

        'Electrical_Shop': [
            {
                "attributes": [
                    {
                        "description": "Cчетчик кв ампер-часов",
                        "engUnits": "КА*ч",
                        "name": "Counter",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Величина постоянного тока",
                        "engUnits": "КА",
                        "name": "DC",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Энергия переменного тока",
                        "engUnits": "МВт*ч",
                        "name": "P_AC",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Энергия постоянного тока",
                        "engUnits": "МВт*ч",
                        "name": "P_DC",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Напряжение постоянного тока",
                        "engUnits": "В",
                        "name": "V_DC",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "ZES_CV_701C",
                "parent": "Electrical_Shop",
                "securityGroup": "Default",
                "shortDesc": "CV 701C",
                "tagName": "ZES_CV_701C"
            },
            {
                "attributes": [
                    {
                        "description": "Cчетчик кв ампер-часов",
                        "engUnits": "КА*ч",
                        "name": "Counter",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Величина постоянного тока",
                        "engUnits": "КА",
                        "name": "DC",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Энергия переменного тока",
                        "engUnits": "МВт*ч",
                        "name": "P_AC",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Энергия постоянного тока",
                        "engUnits": "МВт*ч",
                        "name": "P_DC",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Напряжение постоянного тока",
                        "engUnits": "В",
                        "name": "V_DC",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "ZES_CV_701B",
                "parent": "Electrical_Shop",
                "securityGroup": "Default",
                "shortDesc": "CV 701B",
                "tagName": "ZES_CV_701B"
            },
            {
                "attributes": [
                    {
                        "description": "Cчетчик кв ампер-часов",
                        "engUnits": "КА*ч",
                        "name": "Counter",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Величина постоянного тока",
                        "engUnits": "КА",
                        "name": "DC",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Энергия переменного тока",
                        "engUnits": "МВт*ч",
                        "name": "P_AC",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Энергия постоянного тока",
                        "engUnits": "МВт*ч",
                        "name": "P_DC",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Напряжение постоянного тока",
                        "engUnits": "В",
                        "name": "V_DC",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "ZES_CV_701A",
                "parent": "Electrical_Shop",
                "securityGroup": "Default",
                "shortDesc": "CV 701A",
                "tagName": "ZES_CV_701A"
            }
        ],

        'Hydro_Metallurgical_Shop': [
            {
                "attributes": [
                    {
                        "description": "Поток",
                        "engUnits": "м3/ч",
                        "name": "Flow",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "HMS_Pump_neutral_solution",
                "parent": "Hydro_Metallurgical_Shop",
                "securityGroup": "Default",
                "shortDesc": "Откачка нейтрального раствора",
                "tagName": "HMS_Pump_neutral_solution"
            },
            {
                "attributes": [
                    {
                        "description": "Фильтр Larox",
                        "engUnits": "",
                        "name": "Work",
                        "type": 1,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Larox",
                "parent": "Hydro_Metallurgical_Shop",
                "securityGroup": "Default",
                "shortDesc": "Фильтр Larox",
                "tagName": "Larox"
            },
            {
                "attributes": [
                    {
                        "description": "Фильтр FB1200",
                        "engUnits": "",
                        "name": "Work",
                        "type": 1,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "FB1200",
                "parent": "Hydro_Metallurgical_Shop",
                "securityGroup": "Default",
                "shortDesc": "Фильтр FB1200",
                "tagName": "FB1200"
            },
            {
                "attributes": [
                    {
                        "description": "Уд. вес",
                        "engUnits": "т/м3",
                        "name": "Value",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "FB1200",
                "parent": "Hydro_Metallurgical_Shop",
                "securityGroup": "Default",
                "shortDesc": "Склад пр-й окиси. Уд. вес",
                "tagName": "HMS_specific_weight"
            },
            {
                "attributes": [
                    {
                        "description": "Общий вес",
                        "engUnits": "т/м3",
                        "name": "Value",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "FB1200",
                "parent": "Hydro_Metallurgical_Shop",
                "securityGroup": "Default",
                "shortDesc": "Склад пр-й окиси. Общий вес",
                "tagName": "HMS_total_weight"
            },
            {
                "attributes": [
                    {
                        "description": "В работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "HMS_Pump7_1",
                "parent": "Hydro_Metallurgical_Shop",
                "securityGroup": "Default",
                "shortDesc": "Насос 7.1",
                "tagName": "HMS_Pump7_1"
            },
            {
                "attributes": [
                    {
                        "description": "В работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "HMS_Pump7_2",
                "parent": "Hydro_Metallurgical_Shop",
                "securityGroup": "Default",
                "shortDesc": "Насос 7.2",
                "tagName": "HMS_Pump7_2"
            },
            {
                "attributes": [
                    {
                        "description": "В работе",
                        "engUnits": "",
                        "name": "Run",
                        "type": 1,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "HMS_Pump7_3",
                "parent": "Hydro_Metallurgical_Shop",
                "securityGroup": "Default",
                "shortDesc": "Насос 7.3",
                "tagName": "HMS_Pump7_3"
            }
        ],

        'Energy_Shop': [
            {
                "attributes": [
                    {
                        "description": "Расход воды промышленной 1",
                        "engUnits": "м3/ч",
                        "name": "Q_water1",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход воды промышленной 2",
                        "engUnits": "м3/ч",
                        "name": "Q_water2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход кислорода Мечел",
                        "engUnits": "м3/ч",
                        "name": "Q_O2_Mechel",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход кислорода ЧЦЗ",
                        "engUnits": "м3/ч",
                        "name": "Q_O2",
                        "type": 3,
                        "value": "0",
                        "values": null
                    },
                    {
                        "description": "Расход газа",
                        "engUnits": "м3/ч",
                        "name": "Q_gas",
                        "type": 3,
                        "value": "0",
                        "values": null
                    }
                ],
                "hierarchicalName": "Energy_Shop",
                "parent": "Chel_Zinc_Plant",
                "securityGroup": "Default",
                "shortDesc": "Расход ресурсов",
                "tagName": "Energy_Shop"
            }
        ]

    }];


    constructor() { }

}
