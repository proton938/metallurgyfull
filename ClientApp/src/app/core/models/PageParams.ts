import { Injectable } from '@angular/core';

@Injectable()

export class PageParams {
  //private params: any = [];
  public page: string;
  public department: string;
  public getHistoryValues: any = '';

  public currentSection: string = '';
  public currentShop: string = '';

  public currentTagName: string = 'Chel_Zinc_Plant';

  constructor() { }

  public setPage(param) {
    this.page = param;
  }

  public getPage() {
    return this.page;
  }

  public setDepartment(param) {
    this.department = param;
  }

  public getDepartment() {
    return this.department;
  }
}
