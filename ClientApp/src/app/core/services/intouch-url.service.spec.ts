import { TestBed } from '@angular/core/testing';

import { IntouchUrlService } from './intouch-url.service';

describe('IntouchUrlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IntouchUrlService = TestBed.get(IntouchUrlService);
    expect(service).toBeTruthy();
  });
});
