import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ChartToolbarService {

  public x: any = 'Да сдравствует X !!!';

  public values: Value[];

  private width: number;
  private height: number;
  private margin = { top: 20, right: 0, bottom: 40, left: 80 };

  public startDateTime: string;
  public stopDateTime: string;
  public currentDateTime: Date;    // текущее время

  public start: number;
  public stop: number;
  public yLimit: number = 1300;
  public dataLength: number;     // ширина диапазона

  public pathUrl: string;

  data1: any[] = [];     // массивы для сбора данных 
  data2: any[] = [];
  data3: any[] = [];
  data4: any[] = [];

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    /*
    this.currentDateTime = new Date();
    this.startDateTime = this.currentDateTime.getFullYear() + '-' + Number(this.currentDateTime.getMonth() + 1) + '-' + this.currentDateTime.getDate() + ' ' + Number(this.currentDateTime.getHours() - 1) + ':' + this.currentDateTime.getMinutes() + ':' + this.currentDateTime.getSeconds();
    this.stopDateTime = this.currentDateTime.getFullYear() + '-' + Number(this.currentDateTime.getMonth() + 1) + '-' + this.currentDateTime.getDate() + ' ' + this.currentDateTime.getHours() + ':' + this.currentDateTime.getMinutes() + ':' + this.currentDateTime.getSeconds();
    */

    this.pathUrl = baseUrl + 'api/PrmValues/';

  }


  dataСollection() {
    this.http.get<Value[]>(this.pathUrl + this.startDateTime + '/' + this.stopDateTime).subscribe(result => {
      this.values = result;

      this.data1 = [];
      this.data2 = [];
      this.data3 = [];
      this.data4 = [];

      this.start = 0;
      this.stop = this.values.length - 1;

      this.getDateArray();

    }, error => console.error(error)); 
  }


  getValues() {
    return this.values;
  }

  getDateArray() {            // Вывод массива данных

    this.data1.push({ dt: new Date(this.values[this.start].dt), value: 0 });
    this.data2.push({ dt: new Date(this.values[this.start].dt), value: 0 });
    this.data3.push({ dt: new Date(this.values[this.start].dt), value: 0 });
    this.data4.push({ dt: new Date(this.values[this.start].dt), value: 0 });
    for (let i = this.start; i <= this.stop; i++) {
      this.data1.push({ dt: new Date(this.values[i].dt), value: this.values[i].value1 });
      this.data2.push({ dt: new Date(this.values[i].dt), value: this.values[i].value2 });
      this.data3.push({ dt: new Date(this.values[i].dt), value: this.values[i].value3 });
      this.data4.push({ dt: new Date(this.values[i].dt), value: this.values[i].value4 });
    }
    this.data1.push({ dt: new Date(this.values[this.stop].dt), value: 0 });
    this.data2.push({ dt: new Date(this.values[this.stop].dt), value: 0 });
    this.data3.push({ dt: new Date(this.values[this.stop].dt), value: 0 });
    this.data4.push({ dt: new Date(this.values[this.stop].dt), value: 0 });

    this.data1.push({ dt: new Date(this.values[this.stop].dt), value: this.yLimit });
    this.data2.push({ dt: new Date(this.values[this.stop].dt), value: this.yLimit });
    this.data3.push({ dt: new Date(this.values[this.stop].dt), value: this.yLimit });
    this.data4.push({ dt: new Date(this.values[this.stop].dt), value: this.yLimit });
    this.dataLength = this.data1.length;
  }
}

interface Value {
  id: number;
  dt: string;
  value1: number;
  value2: number;
  value3: number;
  value4: number;
}



