import { TestBed } from '@angular/core/testing';

import { ChartToolbarService } from './chart-toolbar.service';

describe('ChartToolbarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChartToolbarService = TestBed.get(ChartToolbarService);
    expect(service).toBeTruthy();
  });
});
