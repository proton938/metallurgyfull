import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-chart-toolbar',
  templateUrl: '../../../../shared/layout/chart-toolbar/chart-toolbar.component.html',
  styleUrls: ['../../../../shared/layout/chart-toolbar/chart-toolbar.component.css']
})
export class ChartToolbarComponent implements OnInit {


  title = 'Архив';


  public pathUrl: string;
  public treeParametersUrl: string;
  public getHistoryValuesUrl: string;

  public arrayParameters: ArrayParameters[];
  public getHistoryValues: ArrayParameters[];

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.treeParametersUrl = baseUrl + 'api/parameters/getParametersByArea';
  }

  ngOnInit() {
    // при загрузке страницы выводим последний час

    var currentDateTime: any = new Date();        // Выводим текущую дату

    (<HTMLInputElement>document.getElementById("dateStop")).value = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + currentDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + currentDateTime.getMinutes()).slice(-2);

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var lastDateTime: any = new Date(currentDateTime - eraUnix - 3600000);

    (<HTMLInputElement>document.getElementById("dateStart")).value = lastDateTime.getFullYear() + '-' + ('0' + (lastDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + lastDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + lastDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + lastDateTime.getMinutes()).slice(-2);
  }

  unHideToolbarMenu() {       // скрытие меню инструментов
    var hiddenToolBar = (<HTMLInputElement>document.getElementById("unHiddenToolbar")).className;
    if (hiddenToolBar == 'unhidden_toolbar') {
      (<HTMLInputElement>document.getElementById("unHiddenToolbar")).className = 'hidden_toolbar';
      (<HTMLInputElement>document.getElementById('toolbarTitle')).style.display = 'none';
    } else {
      (<HTMLInputElement>document.getElementById("unHiddenToolbar")).className = 'unhidden_toolbar';
      (<HTMLInputElement>document.getElementById('toolbarTitle')).style.display = 'inline-block';
    }
  }





  public identif: string = '';
  public styleDisplay: any;
  private permission: number = 0;

  treeParameters() {
    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');
    if (currentUrl.length < 5) {
      currentUrl = 'Main';
    } else {
      currentUrl = currentUrl[4];
    }

    var testLoadMenu = (<HTMLInputElement>document.getElementById(currentUrl));

    if (testLoadMenu) {
      currentUrl = (<HTMLInputElement>document.getElementById(currentUrl)).value;
      this.treeParametersUrl = this.treeParametersUrl + '/' + currentUrl;

      this.styleDisplay = (<HTMLInputElement>document.getElementById('one_1')).style.display;
      if (this.styleDisplay == 'none') {
        (<HTMLInputElement>document.getElementById('one_1')).style.display = 'table';
        (<HTMLInputElement>document.getElementById('rolloverDown')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('rolloverUp')).style.display = 'block';
      } else {
        (<HTMLInputElement>document.getElementById('one_1')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('rolloverDown')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('rolloverUp')).style.display = 'none';
      }

      if (this.permission == 0) {
        this.http.get<ArrayParameters[]>(this.treeParametersUrl).subscribe(result => {
          this.arrayParameters = result;
          for (let i = 0; i < this.arrayParameters.length; i++) {      // если имя параметра пустое - заменяем его на системный тег 
            if (this.arrayParameters[i].shortDesc == '') {
              this.arrayParameters[i].shortDesc = this.arrayParameters[i].tagName;
            }
          }
        }, error => console.error(error));
        this.permission = 1;                   // разрешение на повторную выгрузку массива закрываем
      }
    } else {
      alert('Меню навигации еще не догрузилось!');
    }
  }

  unHideTree() {
    this.styleDisplay = (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display;
    if (this.styleDisplay == 'none') {
      (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display = 'table';
      (<HTMLInputElement>document.getElementById('arrowOne_' + this.identif)).innerHTML = 'v';
    } else {
      (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display = 'none';
      (<HTMLInputElement>document.getElementById('arrowOne_' + this.identif)).innerHTML = '>';
    }
  }



}



interface ArrayParameters {
  shortDesc: string;
  attributes: any;
  tagName: string;
  description: string;
  length: number;
}
