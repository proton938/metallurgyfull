import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatTreeModule, MatIconModule, MatButtonModule, MatButton } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ArchiveRoutingModule } from './archive-routing.module';
import { HistorianComponent } from './pages/historian/historian.component';
import { ParametersComponent } from './pages/parameters/parameters.component';
import { ArchiveComponent } from './pages/archive/archive.component';
import { ChartToolbarComponent } from './pages/archive/chart-toolbar/chart-toolbar.component';

@NgModule({
  imports: [
    CommonModule,
    ArchiveRoutingModule,
    MatTreeModule, BrowserAnimationsModule, MatIconModule, MatButtonModule
  ],
  declarations: [HistorianComponent, ParametersComponent, ArchiveComponent, ChartToolbarComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ArchiveModule { }
