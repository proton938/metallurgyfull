import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SharedModule } from './shared/shared.module';
import { HomeModule } from './home/home.module';
import { ReportsModule } from './reports/reports.module';
import { AnalysisModule } from './analysis/analysis.module';
import { ArchiveModule } from './archive/archive.module';
import { ProcessSchemeModule } from './process-scheme/process-scheme.module';
import { SpecificationsModule } from './specifications/specifications.module';
import { WorkOrdersModule } from './work-orders/work-orders.module';


import { AppComponent } from './app.component';
import { NavMenuComponent } from './shared/layout/nav-menu/nav-menu.component';
//import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
//import { AboutUsComponent } from './home/pages/about-us/about-us.component';
//import { ContactUsComponent } from './home/pages/contact-us/contact-us.component';
//import { HeaderComponent } from './shared/layout/header/header.component';
//import { FooterComponent } from './shared/layout/footer/footer.component';

//import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
//import { SafePipe } from './core/pipes/safe';
import { IntouchUrlService } from './core/services/intouch-url.service';
import { PageParams } from "../app/core/models/PageParams";
import { TreeParams } from "../app/core/models/TreeParams";
import { AnimateParams } from "../app/core/models/AnimateParams";
import { TestComponent } from './test/test.component';
import { KnowledgeComponent } from './knowledge/knowledge.component'

import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //req.headers.append('Authorization', 'Windows')
    //const clonedRequest = req.clone({ headers: req.headers.set('Authorization', 'Windows') });
    req = req.clone({
      withCredentials: true
    });
    return next.handle(req);
  }
}

import { HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    //HomeComponent,
    CounterComponent,
    FetchDataComponent,
    TestComponent,
    KnowledgeComponent,
    //AboutUsComponent,
    //ContactUsComponent,
    //HeaderComponent,
    //FooterComponent,
    //SafePipe
  ],
  imports: [
    BrowserModule,//.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      //{ path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'test', component: TestComponent },
      { path: 'knowledge', component: KnowledgeComponent },
      { path: 'knowledge/:id', component: KnowledgeComponent }
    ]),
    SharedModule,
    HomeModule,
    ReportsModule,
    AnalysisModule,
    ArchiveModule,
    ProcessSchemeModule,
    SpecificationsModule,
    WorkOrdersModule
  ],
  providers: [IntouchUrlService, PageParams, TreeParams, AnimateParams, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthenticationInterceptor,
    multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

//platformBrowserDynamic().bootstrapModule(ProcessSchemeModule);
