"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./about-us/about-us.component"));
__export(require("./contact-us/contact-us.component"));
__export(require("./home/home.component"));
__export(require("./home-main/home-main.component"));
__export(require("./home/obj/ks1/ks1.component"));
__export(require("./home/obj/ks2/ks2.component"));
__export(require("./home/services/services.component"));
__export(require("./tools/tools.component"));
//# sourceMappingURL=index.js.map