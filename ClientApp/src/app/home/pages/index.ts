export * from './about-us/about-us.component';
export * from './contact-us/contact-us.component';
export * from './home/home.component';
export * from './home-main/home-main.component';
export * from './home/obj/ks1/ks1.component';
export * from './home/obj/ks2/ks2.component';
export * from './home/services/services.component';
export * from './tools/tools.component';
