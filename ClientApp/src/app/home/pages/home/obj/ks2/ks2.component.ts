import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-ks2',
  templateUrl: './ks2.component.html',
  styleUrls: ['./ks2.component.css']
})
export class Ks2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }


  public run: boolean = false;
  public boolean: boolean = false;

  public onShow: boolean = false;   // открыто ли окно подраздела 


  startAnimation() {
    if (this.boolean == true) {
      if (this.run == false) {
        var allChildrens: any = (<HTMLInputElement>document.getElementById("windowKs2"));
        allChildrens.children[0].style.display = 'block';  // раскрываем стартовый кадр
        allChildrens.children[allChildrens.children.length - 1].style.display = 'none';  // скрываем стоп-кадр

        // запускаем раскадровку
        this.storyboard("windowKs2", 0, 5, 300);
        this.run = true; // отмечаем, что анимация запущена
      }
    } else {
      this.run = false; // отмечаем, что анимация осталась не запущена
    }
  }


  stopAnimation() {
    this.boolean = false;
    this.run = false;

    var allChildrens: any = (<HTMLInputElement>document.getElementById("windowKs2"));
    for (let j = 0; j < allChildrens.children.length; j++) {  // скрываем все действующие кадры
      allChildrens.children[j].style.display = 'none';
    }
    allChildrens.children[allChildrens.children.length - 1].style.display = 'block';  // раскрываем стоп-кадр
  }



  storyboard(id, counter, max, interval) {  // метод раскадровки
    var that = this;

    if (this.boolean == true) {
      var allChildrens: any = (<HTMLInputElement>document.getElementById(id));
      if (counter < max) {
        allChildrens.children[counter].style.display = 'none';
        allChildrens.children[counter + 1].style.display = 'block';
        counter++;
      } else {
        allChildrens.children[counter].style.display = 'none';
        allChildrens.children[0].style.display = 'block';
        counter = 0;
      }

      setTimeout(function () {
        that.storyboard(id, counter, max, interval);
      }, interval);
    }
  }


  @Output() switchBlockAnimation = new EventEmitter<boolean>();

  close(x: any) {
    // передаем значение false для переменной blockAnimation в родительском компоненте для разблокировки
    this.switchBlockAnimation.emit(x);

    (<HTMLInputElement>document.getElementById("InTouchWeb")).style.display = 'block';
    (<HTMLInputElement>document.getElementById("showKs2")).style.display = 'none';

    this.stopAnimation();

    this.onShow = false; // отмечаем, что окно закрыто
  }


}
