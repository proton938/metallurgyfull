import { Component, HostListener, OnInit, Inject, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TreeParams } from "../../../core/models/TreeParams";
import { Ks1Component } from './obj/ks1/ks1.component';
import { Ks2Component } from './obj/ks2/ks2.component';
import { ServicesComponent } from './services/services.component';

import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3Path from 'd3-path';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],

})


export class HomeComponent implements OnInit {


  @ViewChild(Ks1Component, { static: true })
  public ks1: Ks1Component;

  @ViewChild(Ks2Component, { static: true })
  public ks2: Ks2Component;


  public blockAnimation: boolean = false;  // блокировка анимации

  switchBlockAnimation(x: any) {  // метод вызываемый из дочернего компонента close() - запуск анимации в родительском
    this.blockAnimation = x;
    this.startStopAnimation();
  }


  // метод определения уровня резервуаров
  levelTank(maxLevel: number, currentLevel: number, maxHeight: number, maxTop: number, idLevel: string, idBottom: string, idTop: string, idWater: string, idStreak: string, idTrend: string) {

    (<HTMLInputElement>document.getElementById(idLevel)).style.height = currentLevel / maxLevel * maxHeight + '%';
    (<HTMLInputElement>document.getElementById(idLevel)).style.top = maxTop + maxHeight - (currentLevel / maxLevel * maxHeight) + '%';

    (<HTMLInputElement>document.getElementById(idTop)).style.top = maxTop + maxHeight - (currentLevel / maxLevel * maxHeight) - maxHeight / 8.5 + '%';
    (<HTMLInputElement>document.getElementById(idWater)).style.top = maxTop + maxHeight - (currentLevel / maxLevel * maxHeight) - maxHeight / 8 + '%';

    if (idTrend != '') {
      (<HTMLInputElement>document.getElementById(idTrend)).style.top = maxTop + maxHeight - (currentLevel / maxLevel * maxHeight) - maxHeight / 2.7 + '%';
    }

    if (idStreak != '') {
      (<HTMLInputElement>document.getElementById(idStreak)).style.height = currentLevel / maxLevel * maxHeight + 3.1 + '%';
      (<HTMLInputElement>document.getElementById(idStreak)).style.top = maxTop + maxHeight - (currentLevel / maxLevel * maxHeight) - maxHeight / 8 + 1.0 + '%';
    }

    if (currentLevel == 0) {
      (<HTMLInputElement>document.getElementById(idBottom)).style.display = 'none';
      (<HTMLInputElement>document.getElementById(idTop)).style.display = 'none';
      (<HTMLInputElement>document.getElementById(idWater)).style.display = 'none';
      (<HTMLInputElement>document.getElementById(idLevel)).style.display = 'none';
    } else {
      (<HTMLInputElement>document.getElementById(idBottom)).style.display = 'block';
      (<HTMLInputElement>document.getElementById(idTop)).style.display = 'block';
      (<HTMLInputElement>document.getElementById(idWater)).style.display = 'block';
      (<HTMLInputElement>document.getElementById(idLevel)).style.display = 'block';
    }
  }



  public currentDateTime: any;   // текущее время
  public lastDateTime: any;      // час назад

  public data: any[] = [];

  public getHistoryValuesUrl: string;
  public getHistoryValues: ArrayParameters[];

  public getHistoryValuesUrl2: string;
  public getHistoryValues2: ArrayParameters[];

  public historyValues: any[] = [];
  public arrayForChart: any[] = [
  ];       // выводим массив значений времени для вертикальной шкалы

  public arrayForVysh: any[] = [];

  public arrayParameters: any[] = [
    'Roasting_Shop.F_sum_sm'
  ];



  // переменные текстовых svg
  public RS_R_collector: string = '##.##';
  public RS_O2_total: string = '00.00';
  public ks1_Q_air: string = '##.##';
  public ks1_Q_O2: string = '##.##';
  public ks1_T_ks: string = '##.##';
  public ks2_Q_air: string = '##.##';
  public ks2_Q_O2: string = '##.##';
  public ks2_T_ks: string = '##.##';
  public ks3_Q_air: string = '##.##';
  public ks3_Q_O2: string = '##.##';
  public ks3_T_ks: string = '##.##';
  public ks4_Q_air: string = '##.##';
  public ks4_Q_O2: string = '##.##';
  public ks4_T_ks: string = '##.##';
  public ks5_Q_air: string = '##.##';
  public ks5_Q_O2: string = '##.##';
  public ks5_T_ks: string = '##.##';

  public System3_SO2Concentration: string = '##.##';
  public System4_SO2Concentration: string = '##.##';
  public System5_SO2Concentration: string = '##.##';
  public System3_GasLoad: string = '##.##';
  public System4_GasLoad: string = '##.##';
  public System5_GasLoad: string = '##.##';
  public System3_GasConcentration: string = '##.##';
  public System4_GasConcentration: string = '##.##';
  public System5_GasConcentration: string = '##.##';
  public SAS_Vmng: string = '##.##';
  public SAS_Vmng2: string = '##.##';

  public vp1_Load: string = '##.##';
  public vp1_T_vg: string = '##.##';
  public vp2_Load: string = '##.##';
  public vp2_T_vg: string = '##.##';
  public vp3_Load: string = '##.##';
  public vp3_T_vg: string = '##.##';
  public vp4_Load: string = '##.##';
  public vp4_T_vg: string = '##.##';
  public vp5_Load: string = '##.##';
  public vp5_T_vg: string = '##.##';
  public vp6_Load: string = '##.##';
  public vp6_T_vg: string = '##.##';
  public Waelz_Larox1: string = '##.##';
  public Waelz_Larox2: string = '##.##';

  public KEC_Total_VIU: string = '##.##';
  public KEC_Total_HMS: string = '##.##';
  public KEC_Total_LS: string = '##.##';
  public KEC_Total_GMO: string = '##.##';
  public KEC_Total_OE_and_KO: string = '##.##';
  public KEC_Total_All: string = '##.##';
  public KEC_Tank_VIU_mixed: string = '##.##';
  public KEC_Tank_VIU_mixed_level: string = '00.00';
  public KEC_Tank_VIU_mixed_trend: string = '0';
  public KEC_Tank_VIU_neutral: string = '##.##';
  public KEC_Tank_VIU_neutral_level: string = '00.00';
  public KEC_Tank_VIU_neutral_trend: string = '0';
  public KEC_Tank_VIU_OE: string = '##.##';
  public KEC_Tank_VIU_OE_level: string = '00.00';
  public KEC_Tank_VIU_OE_trend: string = '0';
  public KEC_Tank_TK510A: string = '##.##';
  public KEC_Tank_TK510B: string = '##.##';
  public KEC_Tank_TK702A: string = '##.##';
  public KEC_Tank_TK702B: string = '##.##';
  public KEC_Tank_TK702C: string = '##.##';
  public KEC_Tank_TK702D: string = '##.##';
  public KEC_Tank_TK705A: string = '##.##';
  public KEC_Tank_TK705B: string = '##.##';
  public KEC_Tank_TK705C: string = '##.##';
  public KEC_Tank_TK705D: string = '##.##';
  public KEC_Tank_TK705E: string = '##.##';
  public KEC_Tank_TK705F: string = '##.##';
  public KEC_Tank_Damper: string = '##.##';
  public KEC_Pump_P510A: string = '##.##';
  public KEC_Pump_P510A_Flow: string = '##.##';
  public KEC_Pump_P510B: string = '##.##';
  public KEC_Pump_P510B_Flow: string = '##.##';
  public KEC_Pump_P703A: string = '##.##';
  public KEC_Pump_P703A_Flow: string = '##.##';
  public KEC_Pump_P703B: string = '##.##';
  public KEC_Pump_P703B_Flow: string = '##.##';
  public ZES_Pump_P701A: string = '##.##';
  public ZES_Pump_P701A_Flow: string = '##.##';
  public ZES_Pump_P701B: string = '##.##';
  public ZES_Pump_P701B_Flow: string = '##.##';

  public LS_pH_washout: string = '##.##';
  public LS_pH_roasting: string = '##.##';
  public LS_pH_VSG: string = '##.##';
  public LS_pH_neutral: string = '##.##';
  public LS_pH_VSNS: string = '##.##';
  public LS_pH_VSKS: string = '##.##';
  public LS_pH_VVS_6: string = '##.##';

  public HMS_Tank_HMS_neutral: string = '##.##';
  public HMS_neutral_Level: string = '##.##';
  public HMS_Tank_HMS_OE: string = '##.##';
  public HMS_OE_Level: string = '##.##';
  public HMS_specific_weight: string = '##.##';   // удельный вес
  public HMS_total_weight: string = '##.##';   // общийвес
  public HMS_Thickener1: string = '##.##';
  public HMS_Thickener2: string = '##.##';
  public HMS_Thickener3: string = '##.##';
  public HMS_Thickener4: string = '##.##';
  public HMS_Thickener5: string = '##.##';
  public HMS_Thickener5_1: string = '##.##';
  public HMS_Thickener5_2: string = '##.##';
  public HMS_Larox: string = '##.##';
  public HMS_Pump7_1: string = '##.##';
  public HMS_Pump7_2: string = '##.##';
  public HMS_Pump7_3: string = '##.##';
  public HMS_Pump_neutral_solution_Flow: string = '##.##';    // откачка нейтрального раствора
  public HMS_Diefenbach14_1: string = '0';
  public HMS_Diefenbach14_2: string = '0';
  public FB1200: string;

  public ZES_CV_701A_P_AC: string = '##.##';
  public ZES_CV_701A_DC: string = '##.##';
  public ZES_CV_701A_V_DC: string = '##.##';
  public ZES_CV_701A_P_DC: string = '##.##';
  public ZES_CV_701A_Counter: string = '##.##';

  public ZES_CV_701B_P_AC: string = '##.##';
  public ZES_CV_701B_DC: string = '##.##';
  public ZES_CV_701B_V_DC: string = '##.##';
  public ZES_CV_701B_P_DC: string = '##.##';
  public ZES_CV_701B_Counter: string = '##.##';

  public ZES_CV_701C_P_AC: string = '##.##';
  public ZES_CV_701C_DC: string = '##.##';
  public ZES_CV_701C_V_DC: string = '##.##';
  public ZES_CV_701C_P_DC: string = '##.##';
  public ZES_CV_701C_Counter: string = '##.##';

  public ZES_CV_701C: string = '##.##';

  public IB_F801_T_ib: string = '##.##';
  public IB_F802_T_ib: string = '##.##';
  public IB_F803_T_ib: string = '##.##';

  public Energy_Shop_Q_water1: string = '##.##';
  public Energy_Shop_Q_water2: string = '##.##';
  public Energy_Shop_Q_O2_Mechel: string = '##.##';
  public Energy_Shop_Q_O2: string = '##.##';
  public Energy_Shop_Q_gas: string = '##.##';


  /*

  @HostListener('mousewheel', ['$event'])  // зуммирование экрана колесиком при полноэкранном режиме
  onMouseWheel(e) {
    if ((<HTMLInputElement>document.getElementById("deployButton")).className == 'unDeployButton') {
      var delta = e.deltaY || e.detail || e.wheelDelta;
      var iFrameWidth: any = (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width;
      iFrameWidth = Number(iFrameWidth.slice(0, iFrameWidth.length - 1))
      if (delta < 0) {
        if (iFrameWidth < 300) {
          iFrameWidth = iFrameWidth + 10;
          (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = iFrameWidth + '%';
        }
      } else {
        if (iFrameWidth > 100) {
          iFrameWidth = iFrameWidth - 10;
          (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = iFrameWidth + '%';
        }
      }
    }

    this.chartScale();
  }

*/

  @HostListener('window:resize', ['$event'])   // при изменении размера окна браузера - масштабируем  диаграммы
  onResize() {
    this.chartScale();
  }



  public InTouchWebWidth: any = 0;    // зуммирование экрана регулятором при полноэкранном режиме
  getRegulatorWidth() {
    if ((<HTMLInputElement>document.getElementById("deployButton")).className == "unDeployButton") {
      this.InTouchWebWidth = (<HTMLInputElement>document.getElementById("regulatorWidth")).value;
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = this.InTouchWebWidth + '%';

      if (this.InTouchWebWidth > 110) {
        (<HTMLInputElement>document.getElementById("leftScroll")).style.display = 'block';
        (<HTMLInputElement>document.getElementById("rightScroll")).style.display = 'block';
      } else {
        (<HTMLInputElement>document.getElementById("leftScroll")).style.display = 'none';
        (<HTMLInputElement>document.getElementById("rightScroll")).style.display = 'none';
      }
    }

    this.chartScale();
  }


  public schemeWindowLeftOffset: any = 0;  // скроллинг экрана при полноэкранном режиме
  public schemeWindowTopOffset: any = 0;  
  getWindowOffset() {
    this.schemeWindowTopOffset = (<HTMLInputElement>document.getElementById("schemeWindow")).scrollTop;
    this.schemeWindowLeftOffset = (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft;
  }




  private baseUrl: string;
  private serv: any;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, public tree: TreeParams, public Serv: ServicesComponent) {
    this.baseUrl = baseUrl;
    this.serv = Serv;
    this.getHistoryValuesUrl = this.baseUrl + 'api/PrmValues/GetHistoryValues';

  }



  public equipmentElements: Animate[] = [

    { id: "ks1", counter: 0, max: 5, interval: 300, boolean: false, run: false },
    { id: "ks2", counter: 4, max: 5, interval: 250, boolean: false, run: false },
    { id: "ks3", counter: 3, max: 5, interval: 230, boolean: false, run: false },
    { id: "ks4", counter: 1, max: 5, interval: 220, boolean: false, run: false },
    { id: "ks5", counter: 2, max: 5, interval: 200, boolean: false, run: false },

    { id: "vp1", counter: 0, max: 5, interval: 350, boolean: false, run: false },
    { id: "vp2", counter: 1, max: 5, interval: 320, boolean: false, run: false },
    { id: "vp3", counter: 2, max: 5, interval: 340, boolean: false, run: false },
    { id: "vp4", counter: 3, max: 5, interval: 330, boolean: false, run: false },
    { id: "vp5", counter: 4, max: 5, interval: 300, boolean: false, run: false },
    { id: "vp6", counter: 4, max: 5, interval: 310, boolean: false, run: false },

    { id: "larox1", counter: 0, max: 1, interval: 1000, boolean: false, run: false },
    { id: "larox2", counter: 0, max: 1, interval: 1100, boolean: false, run: false },
    { id: "larox3", counter: 0, max: 1, interval: 1050, boolean: false, run: false },
    { id: "larox4", counter: 0, max: 1, interval: 1030, boolean: false, run: false },
    { id: "larox5", counter: 0, max: 1, interval: 1090, boolean: false, run: false },
    { id: "larox6", counter: 0, max: 1, interval: 1010, boolean: false, run: false },
    { id: "larox7", counter: 0, max: 1, interval: 1030, boolean: false, run: false },
    { id: "larox8", counter: 0, max: 1, interval: 1080, boolean: false, run: false },

    { id: "pump1", counter: 0, max: 2, interval: 100, boolean: false, run: false },
    { id: "pump2", counter: 0, max: 2, interval: 108, boolean: false, run: false },
    { id: "pump3", counter: 0, max: 2, interval: 112, boolean: false, run: false },
    { id: "pump4", counter: 0, max: 2, interval: 120, boolean: false, run: false },
    { id: "pump5", counter: 0, max: 2, interval: 90, boolean: false, run: false },
    { id: "pump6", counter: 0, max: 2, interval: 90, boolean: false, run: false },
    { id: "pump7", counter: 0, max: 2, interval: 90, boolean: false, run: false },
    { id: "pump8", counter: 0, max: 2, interval: 90, boolean: false, run: false },
    { id: "pump9", counter: 0, max: 2, interval: 90, boolean: false, run: false },
    { id: "pump10", counter: 0, max: 2, interval: 90, boolean: false, run: false },
    { id: "pump11", counter: 0, max: 2, interval: 105, boolean: false, run: false },
    { id: "pump12", counter: 0, max: 2, interval: 115, boolean: false, run: false },

    { id: "pump90_1", counter: 0, max: 2, interval: 125, boolean: false, run: false },
    { id: "pump90_2", counter: 0, max: 2, interval: 120, boolean: false, run: false },
    { id: "pump90_3", counter: 0, max: 2, interval: 120, boolean: false, run: false },
    { id: "pump90_4", counter: 0, max: 2, interval: 120, boolean: false, run: false },
    { id: "pump90_5", counter: 0, max: 2, interval: 120, boolean: false, run: false },
    { id: "pump90_6", counter: 0, max: 2, interval: 120, boolean: false, run: false },
    { id: "pump90_7", counter: 0, max: 2, interval: 120, boolean: false, run: false },
    { id: "pump90_8", counter: 0, max: 2, interval: 120, boolean: false, run: false },
    { id: "pump90_9", counter: 0, max: 2, interval: 120, boolean: false, run: false },
    { id: "pump90_10", counter: 0, max: 2, interval: 120, boolean: false, run: false },
    { id: "pump90_11", counter: 0, max: 2, interval: 120, boolean: false, run: false },
    { id: "pump90_12", counter: 0, max: 2, interval: 120, boolean: false, run: false },

    { id: "thickener1", counter: 0, max: 8, interval: 800, boolean: false, run: false },
    { id: "thickener2", counter: 1, max: 8, interval: 810, boolean: false, run: false },
    { id: "thickener3", counter: 2, max: 8, interval: 780, boolean: false, run: false },
    { id: "thickener4", counter: 3, max: 8, interval: 815, boolean: false, run: false },
    { id: "thickener5", counter: 4, max: 8, interval: 790, boolean: false, run: false },
    { id: "thickener5_1", counter: 5, max: 8, interval: 805, boolean: false, run: false },
    { id: "thickener5_2", counter: 7, max: 8, interval: 795, boolean: false, run: false },

    { id: "water1", counter: 0, max: 3, interval: 290, boolean: true, run: false },
    { id: "water2", counter: 0, max: 3, interval: 350, boolean: true, run: false },
    { id: "water3", counter: 0, max: 3, interval: 320, boolean: true, run: false },
    { id: "water4", counter: 0, max: 3, interval: 310, boolean: true, run: false },
    { id: "water5", counter: 0, max: 3, interval: 360, boolean: true, run: false },
    { id: "water6", counter: 0, max: 3, interval: 330, boolean: true, run: false },
    { id: "water7", counter: 0, max: 3, interval: 326, boolean: true, run: false },
    { id: "water8", counter: 0, max: 3, interval: 315, boolean: true, run: false },

    { id: "induction_bake1", counter: 0, max: 11, interval: 580, boolean: false, run: false },
    { id: "induction_bake2", counter: 0, max: 13, interval: 600, boolean: false, run: false },
    { id: "induction_bake3", counter: 0, max: 11, interval: 500, boolean: false, run: false },

    { id: "water9", counter: 0, max: 3, interval: 325, boolean: true, run: false },
    { id: "water10", counter: 0, max: 3, interval: 333, boolean: true, run: false },
    { id: "water11", counter: 0, max: 3, interval: 342, boolean: true, run: false },
    { id: "water12", counter: 0, max: 3, interval: 342, boolean: true, run: false },

    { id: "pump90_13", counter: 0, max: 2, interval: 125, boolean: false, run: false },
    { id: "pump90_14", counter: 0, max: 2, interval: 117, boolean: false, run: false },
  ];




  setUp() {
    this.equipmentElements[0].boolean = true;
    this.equipmentElements[1].boolean = true;
    this.equipmentElements[7].boolean = true;
    this.startStopAnimation();

    // если это значение true - стартует анимация в дочернем компоненте
    this.ks1.boolean = this.equipmentElements[0].boolean;
    if (this.ks1.onShow == true) {
      this.ks1.startAnimation();
    }

    // если это значение true - стартует анимация в дочернем компоненте
    this.ks2.boolean = this.equipmentElements[0].boolean;
    if (this.ks2.onShow == true) {
      this.ks2.startAnimation();
    }
  }

  setDown() {
    this.equipmentElements[0].boolean = false;
    this.equipmentElements[1].boolean = false;
    this.equipmentElements[7].boolean = false;
    this.startStopAnimation();

    if (this.equipmentElements[0].boolean == false) {
      this.ks1.stopAnimation();
    }

    if (this.equipmentElements[0].boolean == false) {
      this.ks2.stopAnimation();
    }
  }





  private chartLeft: number;
  private chartTop: number;



  private arrayForSvg: arrayForSvg[] = [   // массив параметров для вывода графиков
    {
      svgClass: '.Obj',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'RS_R_collector.Value', stroke: '#feb41b', fill: 'rgba(255, 150, 0, 0.1)', arrayForChart: [] }
      ]
    },
    {
      svgClass: '.Vysh',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'Vysh_OE.Value', stroke: '#feb41b', fill: 'rgba(255, 150, 0, 0.1)', arrayForChart: [] }
      ]
    },
    {
      svgClass: '.Velc_Old',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'KVP_1.Load', stroke: 'rgb(255, 0, 255)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'KVP_2.Load', stroke: 'rgb(255, 0, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Waelz_WO1.Value', stroke: 'rgb(0, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Waelz_WO2.Value', stroke: 'rgb(0, 255, 255)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
      ]
    },
    {
      svgClass: '.Velc',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'KVP_5.Load', stroke: 'rgb(255, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'KVP_6.Load', stroke: 'rgb(150, 150, 255)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        // { request: 'KVP_2.T_vg', stroke: 'rgb(0, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        // { request: 'Waelz_Cr27.Value', stroke: 'rgb(0, 255, 255)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        // { request: 'KVP_3.T_vg', stroke: 'rgb(255, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
      ]
    },
    {
      svgClass: '.Gidromet',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'HMS_Pump_neutral_solution.Flow', stroke: '#feb41b', fill: 'rgba(255, 150, 0, 0.1)', arrayForChart: [] },
        // { request: 'HMS_VSS2.Value', stroke: 'rgb(255, 0, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        // { request: 'HMS_intake_initial_solution.Value', stroke: 'rgb(0, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
      ]
    },
    {
      svgClass: '.KEC',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'ZES_Pump_P510A.Flow', stroke: 'rgb(255, 0, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'ZES_Pump_P701B.Flow', stroke: 'rgb(0, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        // { request: 'Tank_Damper.Volume', stroke: 'rgb(0, 255, 255)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] }
      ]
    },
    /*
    {
      svgClass: '.Velc',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'Waelz_Shop.RVO', stroke: 'rgb(255, 0, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Waelz_Shop.Kr26', stroke: 'rgb(0, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Waelz_Shop.Kr27', stroke: 'rgb(0, 255, 255)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Waelz_Shop.Kr36', stroke: 'rgb(255, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] }
      ]
    },
    {
      svgClass: '.Gidromet',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'Hydro_Metallurgical_Shop.Q_vss2', stroke: 'rgb(255, 0, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Hydro_Metallurgical_Shop.RIR', stroke: 'rgb(0, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] }
      ]
    },
    {
      svgClass: '.KEC',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'Zinc_Electrolysis_Shop.R_after-510A', stroke: 'rgb(255, 0, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Zinc_Electrolysis_Shop.R_after-510B', stroke: 'rgb(0, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Zinc_Electrolysis_Shop.R_after-703А-B', stroke: 'rgb(0, 255, 255)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] }
      ]
    },
    */
  ];



  private buildSvg(shopNumber) {

    this.arrayForSvg[shopNumber].sidesVar = d3.select(this.arrayForSvg[shopNumber].svgClass)
      .append('g')
      .attr('transform', 'translate(' + 0 + ',' + 0 + ')');

    this.arrayForSvg[shopNumber].sidesVar.append("line")              // линия top
      .attr('stroke', 'rgb(230, 230, 230)')
      .attr('stroke-width', this.chartStrokeWidth * 1)
      .attr("x1", 0)
      .attr("y1", 0)
      .attr("x2", 100 + '%')
      .attr("y2", 0);

    this.arrayForSvg[shopNumber].sidesVar.append("line")              // линия bottom
      .attr('stroke', 'rgb(230, 230, 230)')
      .attr('stroke-width', this.chartStrokeWidth * 1)
      .attr("x1", 0)
      .attr("y1", 100 + '%')
      .attr("x2", 100 + '%')
      .attr("y2", 100 + '%');

    this.arrayForSvg[shopNumber].sidesVar.append("line")              // линия left
      .attr('stroke', 'rgb(230,230, 230)')
      .attr('stroke-width', this.chartStrokeWidth * 1)
      .attr("x1", 0)
      .attr("y1", 0)
      .attr("x2", 0)
      .attr("y2", 100 + '%');

    this.arrayForSvg[shopNumber].sidesVar.append("line")              // линия right
      .attr('stroke', 'rgb(230, 230, 230)')
      .attr('stroke-width', this.chartStrokeWidth * 1)
      .attr("x1", 100 + '%')
      .attr("y1", 0)
      .attr("x2", 100 + '%')
      .attr("y2", 100 + '%');

    this.arrayForSvg[shopNumber].svgVar = d3.select(this.arrayForSvg[shopNumber].svgClass)
      .append('g')
      .attr('transform', 'translate(' + this.chartLeft + ',' + this.chartTop + ')');
  }




  private x: any;
  private y: any;

  private width: number;
  private height: number;
  private fontSize: number;
  private chartStrokeWidth: number;
  private tickStrokeWidth: number;



  private activeAddXandYAxis(shopNumber) {
    // range of data configuring
    this.x = d3Scale.scaleTime().range([0, this.width]);
    this.y = d3Scale.scaleLinear().range([this.height, 0]);
    this.x.domain(d3Array.extent(this.arrayForSvg[shopNumber].arrayForAxis, (d) => d.dt));
    this.y.domain(d3Array.extent(this.arrayForSvg[shopNumber].arrayForAxis, (d) => d.val));

    // Configure the X Axis
    this.arrayForSvg[shopNumber].svgVar.append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .attr('stroke-width', this.tickStrokeWidth)
      .attr('style', 'stroke-opacity: 1')
      .attr('style', 'font-size: ' + this.fontSize + 'px')
      .call(d3Axis.axisBottom(this.x)
        .tickSize(-this.height)
        .ticks(5));

    // Configure the Y Axis
    this.arrayForSvg[shopNumber].svgVar.append('g')
      .attr('style', 'stroke-opacity: 1')
      .attr('style', 'font-size: ' + this.fontSize + 'px')
      .attr('stroke-width', this.tickStrokeWidth)
      .call(d3Axis.axisLeft(this.y)
        .tickSize(-this.width)
        .ticks(4))
  }


  private line: d3Shape.Line<[number, number]>; // this is line defination

  activeLineAndPathObj() {        // вывод графиков

    for (let i = 0; i < this.arrayForSvg.length; i++) {

      this.activeAddXandYAxis(i);

      for (let chartCount = 0; chartCount < this.arrayForSvg[i].charts.length; chartCount++) {

        var multiplier: number;
        if (this.arrayForSvg[i].charts[chartCount].fill != 'rgba(0, 0, 0, 0)') {
          multiplier = 1;
        } else {
          multiplier = 2;   // если график имеет прозрачную заливку увеличиваем толщину контура
        }

        this.line = d3Shape.line()
          .x((d: any) => this.x(d.dt))
          .y((d: any) => this.y(d.val));
        // Configuring line path
        this.arrayForSvg[i].svgVar.append('path')
          .datum(this.arrayForSvg[i].charts[chartCount].arrayForChart)
          .attr('stroke', this.arrayForSvg[i].charts[chartCount].stroke)
          .attr('fill', this.arrayForSvg[i].charts[chartCount].fill)
          .attr('stroke-width', this.chartStrokeWidth * multiplier)
          .attr('d', this.line)
          .attr('id', this.arrayForSvg[i].charts[chartCount].request);
      }
    }

  }



  ngOnInit() {

    this.startStopAnimation();
    for (let i = 0; i < this.arrayForSvg.length; i++) {
      this.buildSvg(i);
    }
    this.chartScale();
    this.chartLoad();

    this.serv.runChartLoad++;

  }



  chartScale() {                                                                    // метод масштабирования графиков
    var svgTemplate = (<HTMLInputElement>document.getElementById('obj'));
    this.width = svgTemplate.clientWidth - (svgTemplate.clientWidth * 0.175);
    this.height = svgTemplate.clientWidth * 0.35;
    this.chartLeft = svgTemplate.clientWidth * 0.116;
    this.chartTop = svgTemplate.clientWidth * 0.058;
    this.fontSize = svgTemplate.clientWidth * 0.032;
    this.chartStrokeWidth = svgTemplate.clientWidth / 340;
    this.tickStrokeWidth = svgTemplate.clientWidth / 800;

    if (this.arrayForSvg[0].charts[0].arrayForChart.length > 2) {

      for (let i = 0; i < this.arrayForSvg.length; i++) {
        this.arrayForSvg[i].svgVar.selectAll("*").remove();
        this.arrayForSvg[i].sidesVar.selectAll("*").remove();

        this.buildSvg(i);
      }

      this.activeLineAndPathObj();

      this.coloringAxis();
    }

    var Q_airScale = document.getElementsByClassName('Q_air');
    for (let i = 0; i < Q_airScale.length; i++) {
      const text = Q_airScale[i];
      if (text instanceof HTMLElement) {
        text.style.fontSize = svgTemplate.clientWidth * 0.06 + 'px';
      }
    }

    var Q_O2Scale = document.getElementsByClassName('Q_O2');
    for (let i = 0; i < Q_O2Scale.length; i++) {
      const text = Q_O2Scale[i];
      if (text instanceof HTMLElement) {
        text.style.fontSize = svgTemplate.clientWidth * 0.06 + 'px';
      }
    }

    var T_vgScale = document.getElementsByClassName('T_vg');
    for (let i = 0; i < T_vgScale.length; i++) {
      const text = T_vgScale[i];
      if (text instanceof HTMLElement) {
        text.style.fontSize = svgTemplate.clientWidth * 0.06 + 'px';
      }
    }

    var LoadScale = document.getElementsByClassName('Load');
    for (let i = 0; i < LoadScale.length; i++) {
      const text = LoadScale[i];
      if (text instanceof HTMLElement) {
        text.style.fontSize = svgTemplate.clientWidth * 0.06 + 'px';
      }
    }

    var GreenColorScale = document.getElementsByClassName('GreenColor');
    for (let i = 0; i < GreenColorScale.length; i++) {
      const text = GreenColorScale[i];
      if (text instanceof HTMLElement) {
        text.style.fontSize = svgTemplate.clientWidth * 0.06 + 'px';
      }
    }

    var GreenColor2Scale = document.getElementsByClassName('GreenColor2');
    for (let i = 0; i < GreenColor2Scale.length; i++) {
      const text = GreenColor2Scale[i];
      if (text instanceof HTMLElement) {
        text.style.fontSize = svgTemplate.clientWidth * 0.06 + 'px';
      }
    }

    var fontVIU = document.getElementsByClassName('fontVIU');
    for (let i = 0; i < fontVIU.length; i++) {
      const text = fontVIU[i];
      if (text instanceof HTMLElement) {
        text.style.fontSize = svgTemplate.clientWidth * 0.06 + 'px';
      }
    }

    var streaks = document.getElementsByClassName('streak');
    for (let i = 0; i < streaks.length; i++) {
      const streak = streaks[i];
      if (streak instanceof HTMLElement) {
        streak.style.borderRight = 'solid ' + svgTemplate.clientWidth * 0.003 + '1px #fff';
        streak.style.borderTop = 'solid ' + svgTemplate.clientWidth * 0.003 + '1px #fff';
        streak.style.borderBottom = 'solid ' + svgTemplate.clientWidth * 0.003 + '1px #fff';
      }
    }

    var LS_thickener = document.getElementsByClassName('LS_thickener');
    for (let i = 0; i < LS_thickener.length; i++) {
      const thickener = LS_thickener[i];
      if (thickener instanceof HTMLElement) {
        thickener.style.border = 'solid ' + svgTemplate.clientWidth * 0.006 + '1px rgb(100,100,100)';
      }
    }

  }


  public turnRequest: number = 0;  // чередователь запросов
  public turnTimeout: number = 10000;  // чередователь интервалов между запросами

  chartLoad() {       // загрузка данных для графиков
    var that = this;

    // this.serv.runChartLoad++;

    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');

    if (this.serv.runChartLoad > 1) {
      this.serv.runChartLoad--;
    } else {
      if (currentUrl.length < 5) {

        if (this.turnRequest == 0) {
          this.firstRequest();
          this.turnTimeout = 1000;
        }
        if (this.turnRequest == 1) {
          this.secondRequest();
          this.turnRequest = -1;
          this.turnTimeout = 10000;
        }
        this.turnRequest++;

        setTimeout(function () {
          that.chartLoad();
        }, this.turnTimeout);
      }
    }
  }

  firstRequest() {
    this.getHistoryValuesUrl = this.baseUrl + 'api/PrmValues/GetHistoryValues';

    this.currentDateTime = new Date();        // Выводим текущую дату
    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));
    this.lastDateTime = new Date(this.currentDateTime - eraUnix - 4*3600000);

    this.lastDateTime = this.lastDateTime.getFullYear() + '-' + ('0' + (this.lastDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + this.lastDateTime.getDate()).slice(-2) + ' ' + ('0' + this.lastDateTime.getHours()).slice(-2) + ':' + ('0' + this.lastDateTime.getMinutes()).slice(-2);
    this.currentDateTime = this.currentDateTime.getFullYear() + '-' + ('0' + (this.currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + this.currentDateTime.getDate()).slice(-2) + ' ' + ('0' + this.currentDateTime.getHours()).slice(-2) + ':' + ('0' + this.currentDateTime.getMinutes()).slice(-2);


    this.getHistoryValuesUrl = this.getHistoryValuesUrl + '/'
      + 'RS_R_collector.Value, Vysh_OE.Value, Waelz_WO1.Value, Waelz_WO2.Value, Waelz_Cr27.Value,'
      + 'HMS_Pump7_1.Run, HMS_Pump7_2.Run, HMS_Pump7_3.Run, HMS_Pump_neutral_solution.Flow,'
      
      + 'SAS_System3.SO2Concentration, SAS_System3.GasLoad, SAS_System3.GasConcentration,'
      + 'SAS_System4.SO2Concentration, SAS_System4.GasLoad, SAS_System4.GasConcentration,'
      + 'SAS_System5.SO2Concentration, SAS_System5.GasLoad, SAS_System5.GasConcentration,'
      + 'KVP_1.T_vg, KVP_2.T_vg, KVP_3.T_vg, KVP_4.T_vg, KVP_5.T_vg, KVP_6.T_vg,'
      + 'KVP_1.Load, KVP_2.Load, KVP_5.Load, KVP_6.Load,'
      + 'VIU_mixed.Volume, VIU_neutral.Volume, VIU_OE.Volume,'
      + 'ZES_Pump_P510A.Run, ZES_Pump_P510B.Run, ZES_Pump_P703A.Run, ZES_Pump_P703B.Run, ZES_Pump_P701A.Run, ZES_Pump_P701A.Flow, ZES_Pump_P701B.Run, ZES_Pump_P701B.Flow,'
      + 'ZES_Pump_P510A.Flow, ZES_Pump_P510B.Flow, ZES_Pump_P703B.Flow, ZES_Pump_P703A.Flow, Tank_TK510A.Volume, Tank_TK510B.Volume,'
      + 'Tank_TK702A.Volume, Tank_TK702B.Volume, Tank_TK702C.Volume, Tank_TK702D.Volume,'
      + 'Tank_TK705A.Volume, Tank_TK705B.Volume, Tank_TK705C.Volume, Tank_TK705D.Volume, Tank_TK705E.Volume, Tank_TK705F.Volume,'
      + 'Total_VIU.Volume, Total_HMS.Volume, Total_LS.Volume, Total_GMO.Volume,'
      + 'ZES_CV_701A.P_AC, ZES_CV_701A.DC, ZES_CV_701A.V_DC, ZES_CV_701A.P_DC, ZES_CV_701A.Counter,'
      + 'ZES_CV_701B.P_AC, ZES_CV_701B.DC, ZES_CV_701B.V_DC, ZES_CV_701B.P_DC, ZES_CV_701B.Counter,'
      + 'ZES_CV_701C.P_AC, ZES_CV_701C.DC, ZES_CV_701C.V_DC, ZES_CV_701C.P_DC, ZES_CV_701C.Counter,'
      + 'Tank_Damper.Volume,'
      + 'HMS_OE.Volume, HMS_neutral.Volume, HMS_specific_weight.Value, HMS_total_weight.Value, HMS_VSS2.Value, HMS_intake_initial_solution.Value,'
      + 'IB_F801.T_ib, IB_F802.T_ib, IB_F803.T_ib,'
      + ' / ' + this.lastDateTime + '/' + this.currentDateTime;

    this.http.get<ArrayParameters[]>(this.getHistoryValuesUrl).subscribe(result => {

      this.getHistoryValues = result;

      // выводим значения в текстовые поля и переключаем изображения (работает/остановлено)

      // обжиговый цех

      this.RS_R_collector = '' + (this.getHistoryValues['RS_R_collector.Value'][this.getHistoryValues['RS_R_collector.Value'].length - 1].val / 10).toFixed(1);

      
      // сернокислотный цех

      this.System3_SO2Concentration = this.getHistoryValues['SAS_System3.SO2Concentration'][this.getHistoryValues['SAS_System3.SO2Concentration'].length - 1].val.toFixed(4);
      this.System3_GasLoad = this.getHistoryValues['SAS_System3.GasLoad'][this.getHistoryValues['SAS_System3.GasLoad'].length - 1].val.toFixed();
      this.System3_GasConcentration = this.getHistoryValues['SAS_System3.GasConcentration'][this.getHistoryValues['SAS_System3.GasConcentration'].length - 1].val.toFixed(1);
      this.System4_SO2Concentration = this.getHistoryValues['SAS_System4.SO2Concentration'][this.getHistoryValues['SAS_System4.SO2Concentration'].length - 1].val.toFixed(4);
      this.System4_GasLoad = this.getHistoryValues['SAS_System4.GasLoad'][this.getHistoryValues['SAS_System4.GasLoad'].length - 1].val.toFixed();
      this.System4_GasConcentration = this.getHistoryValues['SAS_System4.GasConcentration'][this.getHistoryValues['SAS_System4.GasConcentration'].length - 1].val.toFixed(1);
      this.System5_SO2Concentration = this.getHistoryValues['SAS_System5.SO2Concentration'][this.getHistoryValues['SAS_System5.SO2Concentration'].length - 1].val.toFixed(4);
      this.System5_GasLoad = this.getHistoryValues['SAS_System5.GasLoad'][this.getHistoryValues['SAS_System5.GasLoad'].length - 1].val.toFixed();
      this.System5_GasConcentration = this.getHistoryValues['SAS_System5.GasConcentration'][this.getHistoryValues['SAS_System5.GasConcentration'].length - 1].val.toFixed(1);


      // вельц цех

      this.vp1_Load = this.getHistoryValues['KVP_1.Load'][this.getHistoryValues['KVP_1.Load'].length - 1].val.toFixed(1);
      this.vp1_T_vg = this.getHistoryValues['KVP_1.T_vg'][this.getHistoryValues['KVP_1.T_vg'].length - 1].val.toFixed();
      if (Number(this.vp1_T_vg) < 200) {
        this.equipmentElements[5].boolean = false;
      } else {
        this.equipmentElements[5].boolean = true;
      }
      this.vp2_Load = this.getHistoryValues['KVP_2.Load'][this.getHistoryValues['KVP_2.Load'].length - 1].val.toFixed(1);
      this.vp2_T_vg = this.getHistoryValues['KVP_2.T_vg'][this.getHistoryValues['KVP_2.T_vg'].length - 1].val.toFixed();
      if (Number(this.vp2_T_vg) < 200) {
        this.equipmentElements[6].boolean = false;
      } else {
        this.equipmentElements[6].boolean = true;
      }
      this.vp3_Load = this.getHistoryValues['Waelz_WO1.Value'][this.getHistoryValues['Waelz_WO1.Value'].length - 1].val.toFixed(1);
      this.vp3_T_vg = this.getHistoryValues['KVP_3.T_vg'][this.getHistoryValues['KVP_3.T_vg'].length - 1].val.toFixed();
      if (Number(this.vp3_T_vg) < 400) {
        this.equipmentElements[7].boolean = false;
      } else {
        this.equipmentElements[7].boolean = true;
      }
      this.vp4_Load = this.getHistoryValues['Waelz_WO2.Value'][this.getHistoryValues['Waelz_WO2.Value'].length - 1].val.toFixed(1);
      this.vp4_T_vg = this.getHistoryValues['KVP_4.T_vg'][this.getHistoryValues['KVP_4.T_vg'].length - 1].val.toFixed();
      if (Number(this.vp4_T_vg) < 400) {
        this.equipmentElements[8].boolean = false;
      } else {
        this.equipmentElements[8].boolean = true;
      }
      this.vp5_Load = this.getHistoryValues['KVP_5.Load'][this.getHistoryValues['KVP_5.Load'].length - 1].val.toFixed(1);
      this.vp5_T_vg = this.getHistoryValues['KVP_5.T_vg'][this.getHistoryValues['KVP_5.T_vg'].length - 1].val.toFixed();
      if (Number(this.vp5_T_vg) < 400) {
        this.equipmentElements[9].boolean = false;
      } else {
        this.equipmentElements[9].boolean = true;
      }
      this.vp6_Load = this.getHistoryValues['KVP_6.Load'][this.getHistoryValues['KVP_6.Load'].length - 1].val.toFixed(1);
      this.vp6_T_vg = this.getHistoryValues['KVP_6.T_vg'][this.getHistoryValues['KVP_6.T_vg'].length - 1].val.toFixed();
      if (Number(this.vp6_T_vg) < 400) {
        this.equipmentElements[10].boolean = false;
      } else {
        this.equipmentElements[10].boolean = true;
      }

      // КЭЦ

      this.KEC_Total_VIU = this.getHistoryValues['Total_VIU.Volume'][this.getHistoryValues['Total_VIU.Volume'].length - 1].val.toFixed(1);
      this.KEC_Total_HMS = this.getHistoryValues['Total_HMS.Volume'][this.getHistoryValues['Total_HMS.Volume'].length - 1].val.toFixed(1);
      this.KEC_Total_LS = this.getHistoryValues['Total_LS.Volume'][this.getHistoryValues['Total_LS.Volume'].length - 1].val.toFixed(1);
      this.KEC_Total_GMO = this.getHistoryValues['Total_GMO.Volume'][this.getHistoryValues['Total_GMO.Volume'].length - 1].val.toFixed(1);

      this.KEC_Tank_VIU_mixed = this.getHistoryValues['VIU_mixed.Volume'][this.getHistoryValues['VIU_mixed.Volume'].length - 1].val.toFixed(1);
      this.KEC_Tank_VIU_mixed_trend = ((Number(this.KEC_Tank_VIU_mixed) - Number(this.getHistoryValues['VIU_mixed.Volume'][0].val))/4).toFixed(1);
      this.levelTank(1200, Number(this.KEC_Tank_VIU_mixed), 6.65, 45.5, 'level_VIU_mixed', 'bottom_VIU_mixed', 'top_VIU_mixed', 'water3', 'streak3', 'trend_VIU_mixed');

      this.KEC_Tank_VIU_neutral = this.getHistoryValues['VIU_neutral.Volume'][this.getHistoryValues['VIU_neutral.Volume'].length - 1].val.toFixed(1);
      this.KEC_Tank_VIU_neutral_trend = ((Number(this.KEC_Tank_VIU_neutral) - Number(this.getHistoryValues['VIU_neutral.Volume'][0].val))/4).toFixed(1);
      this.levelTank(1200, Number(this.KEC_Tank_VIU_neutral), 6.65, 45.5, 'level_VIU_neutral', 'bottom_VIU_neutral', 'top_VIU_neutral', 'water1', 'streak1', 'trend_VIU_neutral');

      this.KEC_Tank_VIU_OE = this.getHistoryValues['VIU_OE.Volume'][this.getHistoryValues['VIU_OE.Volume'].length - 1].val.toFixed(1);
      this.KEC_Tank_VIU_OE_trend = ((Number(this.KEC_Tank_VIU_OE) - Number(this.getHistoryValues['VIU_OE.Volume'][0].val))/4).toFixed(1);
      this.levelTank(1200, Number(this.KEC_Tank_VIU_OE), 6.65, 45.5, 'level_VIU_OE', 'bottom_VIU_OE', 'top_VIU_OE', 'water2', 'streak2', 'trend_VIU_OE');

      this.KEC_Pump_P510A = this.getHistoryValues['ZES_Pump_P510A.Run'][this.getHistoryValues['ZES_Pump_P510A.Run'].length - 1].val;
      if (this.KEC_Pump_P510A == '0') {
        this.equipmentElements[32].boolean = false;
      } else {
        this.equipmentElements[32].boolean = true;
      }
      this.KEC_Pump_P510A_Flow = this.getHistoryValues['ZES_Pump_P510A.Flow'][this.getHistoryValues['ZES_Pump_P510A.Flow'].length - 1].val.toFixed(1);
      this.KEC_Pump_P510B = this.getHistoryValues['ZES_Pump_P510B.Run'][this.getHistoryValues['ZES_Pump_P510B.Run'].length - 1].val;
      if (this.KEC_Pump_P510B == '0') {
        this.equipmentElements[31].boolean = false;
      } else {
        this.equipmentElements[31].boolean = true;
      }
      this.KEC_Pump_P510B_Flow = this.getHistoryValues['ZES_Pump_P510B.Flow'][this.getHistoryValues['ZES_Pump_P510B.Flow'].length - 1].val.toFixed(1);
      this.KEC_Pump_P703A = this.getHistoryValues['ZES_Pump_P703A.Run'][this.getHistoryValues['ZES_Pump_P703A.Run'].length - 1].val;
      if (this.KEC_Pump_P703A == '0') {
        this.equipmentElements[33].boolean = false;
      } else {
        this.equipmentElements[33].boolean = true;
      }

      // условие вывода графика для P510A/B - выводится график насоса с большим значением в текущий моммент 
      if (this.KEC_Pump_P510A_Flow > this.KEC_Pump_P510B_Flow) {        
        this.arrayForSvg[5].charts[0].request = 'ZES_Pump_P510A.Flow';
      } else {
        this.arrayForSvg[5].charts[0].request = 'ZES_Pump_P510B.Flow';
      }

      this.ZES_Pump_P701A = this.getHistoryValues['ZES_Pump_P701A.Run'][this.getHistoryValues['ZES_Pump_P701A.Run'].length - 1].val;
      if (this.ZES_Pump_P701A == '0') {
        this.equipmentElements[65].boolean = false;
      } else {
        this.equipmentElements[65].boolean = true;
      }
      this.ZES_Pump_P701A_Flow = this.getHistoryValues['ZES_Pump_P701A.Flow'][this.getHistoryValues['ZES_Pump_P701A.Flow'].length - 1].val.toFixed(1);

      this.ZES_Pump_P701B = this.getHistoryValues['ZES_Pump_P701B.Run'][this.getHistoryValues['ZES_Pump_P701B.Run'].length - 1].val;
      if (this.ZES_Pump_P701B == '0') {
        this.equipmentElements[66].boolean = false;
      } else {
        this.equipmentElements[66].boolean = true;
      }
      this.ZES_Pump_P701B_Flow = this.getHistoryValues['ZES_Pump_P701B.Flow'][this.getHistoryValues['ZES_Pump_P701B.Flow'].length - 1].val.toFixed(1);

      // условие вывода графика для P701A/B - выводится график насоса с большим значением в текущий моммент 
      if (this.ZES_Pump_P701A_Flow > this.ZES_Pump_P701B_Flow) {
        this.arrayForSvg[5].charts[1].request = 'ZES_Pump_P701A.Flow';
      } else {
        this.arrayForSvg[5].charts[1].request = 'ZES_Pump_P701B.Flow';
      }

      this.KEC_Pump_P703A_Flow = this.getHistoryValues['ZES_Pump_P703A.Flow'][this.getHistoryValues['ZES_Pump_P703A.Flow'].length - 1].val.toFixed(1);
      this.KEC_Pump_P703B = this.getHistoryValues['ZES_Pump_P703B.Run'][this.getHistoryValues['ZES_Pump_P703B.Run'].length - 1].val;
      if (this.KEC_Pump_P703B == '0') {
        this.equipmentElements[34].boolean = false;
      } else {
        this.equipmentElements[34].boolean = true;
      }
      this.KEC_Pump_P703B_Flow = this.getHistoryValues['ZES_Pump_P703B.Flow'][this.getHistoryValues['ZES_Pump_P703B.Flow'].length - 1].val.toFixed(1);


      this.KEC_Tank_TK510A = this.getHistoryValues['Tank_TK510A.Volume'][this.getHistoryValues['Tank_TK510A.Volume'].length - 1].val.toFixed(1);
      this.levelTank(600, Number(this.KEC_Tank_TK510A), 6.0, 52.3, 'level_KEC_TK510A', 'bottom_KEC_TK510A', 'top_KEC_TK510A', 'water8', '', '');

      this.KEC_Tank_TK510B = this.getHistoryValues['Tank_TK510B.Volume'][this.getHistoryValues['Tank_TK510B.Volume'].length - 1].val.toFixed(1);
      this.levelTank(600, Number(this.KEC_Tank_TK510B), 6.0, 52.3, 'level_KEC_TK510B', 'bottom_KEC_TK510B', 'top_KEC_TK510B', 'water7', '', '');

      this.KEC_Tank_Damper = this.getHistoryValues['Tank_Damper.Volume'][this.getHistoryValues['Tank_Damper.Volume'].length - 1].val.toFixed(1);

      this.KEC_Tank_TK702A = this.getHistoryValues['Tank_TK702A.Volume'][this.getHistoryValues['Tank_TK702A.Volume'].length - 1].val.toFixed(1);
      this.levelTank(1000, Number(this.KEC_Tank_TK702A), 6.0, 52.3, 'level_KEC_TK702A', 'bottom_KEC_TK702A', 'top_KEC_TK702A', 'water9', '', '');

      this.KEC_Tank_TK702B = this.getHistoryValues['Tank_TK702B.Volume'][this.getHistoryValues['Tank_TK702B.Volume'].length - 1].val.toFixed(1);
      this.levelTank(1000, Number(this.KEC_Tank_TK702B), 6.0, 52.3, 'level_KEC_TK702B', 'bottom_KEC_TK702B', 'top_KEC_TK702B', 'water10', '', '');

      this.KEC_Tank_TK702C = this.getHistoryValues['Tank_TK702C.Volume'][this.getHistoryValues['Tank_TK702C.Volume'].length - 1].val.toFixed(1);
      this.levelTank(500, Number(this.KEC_Tank_TK702C), 6.0, 52.3, 'level_KEC_TK702C', 'bottom_KEC_TK702C', 'top_KEC_TK702C', 'water11', '', '');

      this.KEC_Tank_TK702D = this.getHistoryValues['Tank_TK702D.Volume'][this.getHistoryValues['Tank_TK702D.Volume'].length - 1].val.toFixed(1);
      this.levelTank(500, Number(this.KEC_Tank_TK702D), 6.0, 52.3, 'level_KEC_TK702D', 'bottom_KEC_TK702D', 'top_KEC_TK702D', 'water12', '', '');

      this.KEC_Tank_TK705A = this.getHistoryValues['Tank_TK705A.Volume'][this.getHistoryValues['Tank_TK705A.Volume'].length - 1].val.toFixed(1);
      this.KEC_Tank_TK705B = this.getHistoryValues['Tank_TK705B.Volume'][this.getHistoryValues['Tank_TK705B.Volume'].length - 1].val.toFixed(1);
      this.KEC_Tank_TK705C = this.getHistoryValues['Tank_TK705C.Volume'][this.getHistoryValues['Tank_TK705C.Volume'].length - 1].val.toFixed(1);
      this.KEC_Tank_TK705D = this.getHistoryValues['Tank_TK705D.Volume'][this.getHistoryValues['Tank_TK705D.Volume'].length - 1].val.toFixed(1);
      this.KEC_Tank_TK705E = this.getHistoryValues['Tank_TK705E.Volume'][this.getHistoryValues['Tank_TK705E.Volume'].length - 1].val.toFixed(1);
      this.KEC_Tank_TK705F = this.getHistoryValues['Tank_TK705F.Volume'][this.getHistoryValues['Tank_TK705F.Volume'].length - 1].val.toFixed(1);

      this.ZES_CV_701A_P_AC = this.getHistoryValues['ZES_CV_701A.P_AC'][this.getHistoryValues['ZES_CV_701A.P_AC'].length - 1].val.toFixed(1);
      this.ZES_CV_701A_DC = this.getHistoryValues['ZES_CV_701A.DC'][this.getHistoryValues['ZES_CV_701A.DC'].length - 1].val.toFixed(1);
      this.ZES_CV_701A_V_DC = this.getHistoryValues['ZES_CV_701A.V_DC'][this.getHistoryValues['ZES_CV_701A.V_DC'].length - 1].val.toFixed(1);
      this.ZES_CV_701A_P_DC = this.getHistoryValues['ZES_CV_701A.P_DC'][this.getHistoryValues['ZES_CV_701A.P_DC'].length - 1].val.toFixed(1);
      this.ZES_CV_701A_Counter = this.getHistoryValues['ZES_CV_701A.Counter'][this.getHistoryValues['ZES_CV_701A.Counter'].length - 1].val.toFixed(1);

      this.ZES_CV_701B_P_AC = this.getHistoryValues['ZES_CV_701B.P_AC'][this.getHistoryValues['ZES_CV_701B.P_AC'].length - 1].val.toFixed(1);
      this.ZES_CV_701B_DC = this.getHistoryValues['ZES_CV_701B.DC'][this.getHistoryValues['ZES_CV_701B.DC'].length - 1].val.toFixed(1);
      this.ZES_CV_701B_V_DC = this.getHistoryValues['ZES_CV_701B.V_DC'][this.getHistoryValues['ZES_CV_701B.V_DC'].length - 1].val.toFixed(1);
      this.ZES_CV_701B_P_DC = this.getHistoryValues['ZES_CV_701B.P_DC'][this.getHistoryValues['ZES_CV_701B.P_DC'].length - 1].val.toFixed(1);
      this.ZES_CV_701B_Counter = this.getHistoryValues['ZES_CV_701B.Counter'][this.getHistoryValues['ZES_CV_701B.Counter'].length - 1].val.toFixed(1);

      this.ZES_CV_701C_P_AC = this.getHistoryValues['ZES_CV_701C.P_AC'][this.getHistoryValues['ZES_CV_701C.P_AC'].length - 1].val.toFixed(1);
      this.ZES_CV_701C_DC = this.getHistoryValues['ZES_CV_701C.DC'][this.getHistoryValues['ZES_CV_701C.DC'].length - 1].val.toFixed(1);
      this.ZES_CV_701C_V_DC = this.getHistoryValues['ZES_CV_701C.V_DC'][this.getHistoryValues['ZES_CV_701C.V_DC'].length - 1].val.toFixed(1);
      this.ZES_CV_701C_P_DC = this.getHistoryValues['ZES_CV_701C.P_DC'][this.getHistoryValues['ZES_CV_701C.P_DC'].length - 1].val.toFixed(1);
      this.ZES_CV_701C_Counter = this.getHistoryValues['ZES_CV_701C.Counter'][this.getHistoryValues['ZES_CV_701C.Counter'].length - 1].val.toFixed(1);


      this.IB_F801_T_ib = this.getHistoryValues['IB_F801.T_ib'][this.getHistoryValues['IB_F801.T_ib'].length - 1].val;
      if (Number(this.IB_F801_T_ib) < 300) {
        this.equipmentElements[58].boolean = false;
      } else {
        this.equipmentElements[58].boolean = true;
      }
      this.IB_F802_T_ib = this.getHistoryValues['IB_F802.T_ib'][this.getHistoryValues['IB_F802.T_ib'].length - 1].val;
      if (Number(this.IB_F802_T_ib) < 300) {
        this.equipmentElements[59].boolean = false;
      } else {
        this.equipmentElements[59].boolean = true;
      }
      this.IB_F803_T_ib = this.getHistoryValues['IB_F803.T_ib'][this.getHistoryValues['IB_F803.T_ib'].length - 1].val;
      if (Number(this.IB_F803_T_ib) < 300) {
        this.equipmentElements[60].boolean = false;
      } else {
        this.equipmentElements[60].boolean = true;
      }


      // Гидрометаллургический цех

      this.HMS_Tank_HMS_OE = this.getHistoryValues['HMS_OE.Volume'][this.getHistoryValues['HMS_OE.Volume'].length - 1].val.toFixed(1);
      this.levelTank(600, Number(this.HMS_Tank_HMS_OE), 6.65, 67.85, 'level_HMS_OE', 'bottom_HMS_OE', 'top_HMS_OE', 'water5', 'streak5', '');

      this.HMS_Tank_HMS_neutral = this.getHistoryValues['HMS_neutral.Volume'][this.getHistoryValues['HMS_neutral.Volume'].length - 1].val.toFixed(1);
      this.levelTank(600, Number(this.HMS_Tank_HMS_neutral), 6.65, 67.85, 'level_HMS_neutral', 'bottom_HMS_neutral', 'top_HMS_neutral', 'water4', 'streak4', '');


      

      this.HMS_Pump_neutral_solution_Flow = this.getHistoryValues['HMS_Pump_neutral_solution.Flow'][this.getHistoryValues['HMS_Pump_neutral_solution.Flow'].length - 1].val.toFixed(1);
      this.HMS_specific_weight = this.getHistoryValues['HMS_specific_weight.Value'][this.getHistoryValues['HMS_specific_weight.Value'].length - 1].val.toFixed(1);
      this.HMS_total_weight = this.getHistoryValues['HMS_total_weight.Value'][this.getHistoryValues['HMS_total_weight.Value'].length - 1].val.toFixed(1);

      this.HMS_Pump7_1 = this.getHistoryValues['HMS_Pump7_1.Run'][this.getHistoryValues['HMS_Pump7_1.Run'].length - 1].val;
      if (this.HMS_Pump7_1 == '0') {
        this.equipmentElements[35].boolean = false;
      } else {
        this.equipmentElements[35].boolean = true;
      }
      this.HMS_Pump7_2 = this.getHistoryValues['HMS_Pump7_2.Run'][this.getHistoryValues['HMS_Pump7_2.Run'].length - 1].val;
      if (this.HMS_Pump7_2 == '0') {
        this.equipmentElements[36].boolean = false;
      } else {
        this.equipmentElements[36].boolean = true;
      }
      this.HMS_Pump7_3 = this.getHistoryValues['HMS_Pump7_3.Run'][this.getHistoryValues['HMS_Pump7_3.Run'].length - 1].val;
      if (this.HMS_Pump7_3 == '0') {
        this.equipmentElements[37].boolean = false;
      } else {
        this.equipmentElements[37].boolean = true;
      }

      this.startStopAnimation();
      this.bodyHttp();


    }, error => {

      console.error(error);

      if (this.arrayForSvg[0].charts[0].arrayForChart.length == 0) {
        this.http.get<ArrayParameters[]>('assets/replase_server/historyValues/Main.json').subscribe((result) => {

          this.getHistoryValues = result;

          this.bodyHttp();

        });
      } else {

        // процедура циклической прокрутки графиков

        for (let j = 0; j < this.arrayForSvg.length; j++) {

          this.arrayForSvg[j].svgVar.selectAll("*").remove();

          for (let chartCount = 0; chartCount < this.arrayForSvg[j].charts.length; chartCount++) {

            if (this.arrayForSvg[j].charts[chartCount].fill != 'rgba(0, 0, 0, 0)') { // если график имеет непрозрачную заливку двигаем цикл со второй по предпоследнюю точку

              var bufer = this.arrayForSvg[j].charts[chartCount].arrayForChart[1].val;
              for (let i = 0; i < this.arrayForSvg[j].charts[chartCount].arrayForChart.length; i++) {
                if (i > 0 && i < this.arrayForSvg[j].charts[chartCount].arrayForChart.length - 2) {
                  this.arrayForSvg[j].charts[chartCount].arrayForChart[i].val = this.arrayForSvg[j].charts[0].arrayForChart[i + 1].val;
                }
              }
              this.arrayForSvg[j].charts[chartCount].arrayForChart[this.arrayForSvg[j].charts[chartCount].arrayForChart.length - 2].val = bufer;

            } else {

              var bufer = this.arrayForSvg[j].charts[chartCount].arrayForChart[0].val;
              for (let i = 0; i < this.arrayForSvg[j].charts[chartCount].arrayForChart.length; i++) {
                if (i < this.arrayForSvg[j].charts[chartCount].arrayForChart.length - 1) {
                  this.arrayForSvg[j].charts[chartCount].arrayForChart[i].val = this.arrayForSvg[j].charts[chartCount].arrayForChart[i + 1].val;
                }
              }
              this.arrayForSvg[j].charts[chartCount].arrayForChart[this.arrayForSvg[j].charts[chartCount].arrayForChart.length - 1].val = bufer;

            }

          }


        }

        this.activeLineAndPathObj();
        this.coloringAxis();

      }
    });
  }

  secondRequest() {
    // второй запрос (т. к. в первом получилось чрезмерное колличество параметров)

    console.log('Старт2');

    this.getHistoryValuesUrl2 = this.baseUrl + 'api/PrmValues/GetHistoryValues';

    this.currentDateTime = new Date();        // Выводим текущую дату
    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));
    this.lastDateTime = new Date(this.currentDateTime - eraUnix - 3600000);

    this.lastDateTime = this.lastDateTime.getFullYear() + '-' + ('0' + (this.lastDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + this.lastDateTime.getDate()).slice(-2) + ' ' + ('0' + this.lastDateTime.getHours()).slice(-2) + ':' + ('0' + this.lastDateTime.getMinutes()).slice(-2);
    this.currentDateTime = this.currentDateTime.getFullYear() + '-' + ('0' + (this.currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + this.currentDateTime.getDate()).slice(-2) + ' ' + ('0' + this.currentDateTime.getHours()).slice(-2) + ':' + ('0' + this.currentDateTime.getMinutes()).slice(-2);

    this.getHistoryValuesUrl2 = this.getHistoryValuesUrl2 + '/'
      + 'Roast_Oven1.Q_air, Roast_Oven1.Q_air1, Roast_Oven1.Q_air2, Roast_Oven1.Q_O2, Roast_Oven1.T_ks,'
      + 'Roast_Oven2.Q_air, Roast_Oven2.Q_air1, Roast_Oven2.Q_air2, Roast_Oven2.Q_O2, Roast_Oven2.T_ks,'
      + 'Roast_Oven3.Q_air, Roast_Oven3.Q_air1, Roast_Oven3.Q_air2, Roast_Oven3.Q_O2, Roast_Oven3.T_ks,'
      + 'Roast_Oven4.Q_air, Roast_Oven4.Q_air1, Roast_Oven4.Q_air2, Roast_Oven4.Q_O2, Roast_Oven4.T_ks,'
      + 'Roast_Oven5.Q_air, Roast_Oven5.Q_air1, Roast_Oven5.Q_air2, Roast_Oven5.Q_O2, Roast_Oven5.T_ks,'

      + 'Total_OE_and_KO.Volume, Total_all.Volume,'
      + 'VIU_OE.level, VIU_neutral.level, VIU_mixed.level, '
      + 'Sulfuric_Acid_Shop.Vmng, Sulfuric_Acid_Shop.Vmng2, '
      + 'Leach_Thickener1.Work, Leach_Thickener2.Work, Leach_Thickener3.Work, Leach_Thickener4.Work, Leach_Thickener5.Work, Leach_Thickener6.Work, Leach_Thickener7.Work,'
      + 'Leach_Thickener8.Work, Leach_Thickener9.Work, Leach_Thickener10.Work, Leach_Thickener11.Work, Leach_Thickener12.Work,'
      + 'Leaching_Shop.Ph.Neutral, Leaching_Shop.Ph.Roast, Leaching_Shop.Ph.VSG, Leaching_Shop.Ph.VSNS, Leaching_Shop.Ph.VSKS, Leaching_Shop.Ph.VVS_6, Leaching_Shop.Ph.washout,'
      + 'Energy_Shop.Q_water1, Energy_Shop.Q_water2, Energy_Shop.Q_O2_Mechel, Energy_Shop.Q_O2, Energy_Shop.Q_gas,'
      + 'Waelz_Larox1.Work, Waelz_Larox2.Work, Diefenbach14_1.Status, Diefenbach14_2.Status,'
      + 'Larox.Work, FB1200.Work,'
      + 'HMS_neutral.Level, HMS_OE.Level, '
      + 'HMS_Thickener1.Pump.Run, HMS_Thickener2.Pump.Run, HMS_Thickener3.Pump.Run, HMS_Thickener4.Pump.Run, HMS_Thickener5.Pump.Run,'
      + 'HMS_Thickener5_1.Duct, HMS_Thickener5_2.Duct,'
      + 'RS_O2_total.Value'
      + ' / ' + this.lastDateTime + '/' + this.currentDateTime;
    this.http.get<ArrayParameters[]>(this.getHistoryValuesUrl2).subscribe(data => {
      this.getHistoryValues2 = data;

      // обжиговый цех

      this.ks1_Q_air = (this.getHistoryValues2['Roast_Oven1.Q_air'][this.getHistoryValues2['Roast_Oven1.Q_air'].length - 1].val + this.getHistoryValues2['Roast_Oven1.Q_air1'][this.getHistoryValues2['Roast_Oven1.Q_air1'].length - 1].val + this.getHistoryValues2['Roast_Oven1.Q_air2'][this.getHistoryValues2['Roast_Oven1.Q_air2'].length - 1].val).toFixed(0);
      this.ks1_Q_O2 = this.getHistoryValues2['Roast_Oven1.Q_O2'][this.getHistoryValues2['Roast_Oven1.Q_O2'].length - 1].val.toFixed(1);
      this.ks1_T_ks = this.getHistoryValues2['Roast_Oven1.T_ks'][this.getHistoryValues2['Roast_Oven1.T_ks'].length - 1].val.toFixed(1);
      if (Number(this.ks1_T_ks) < 850) {
        this.equipmentElements[0].boolean = false;
        this.equipmentElements[38].boolean = false;
      } else {
        this.equipmentElements[0].boolean = true;
        this.equipmentElements[38].boolean = true;
      }
      this.ks2_Q_air = (this.getHistoryValues2['Roast_Oven2.Q_air'][this.getHistoryValues2['Roast_Oven2.Q_air'].length - 1].val + this.getHistoryValues2['Roast_Oven2.Q_air1'][this.getHistoryValues2['Roast_Oven2.Q_air1'].length - 1].val + this.getHistoryValues2['Roast_Oven2.Q_air2'][this.getHistoryValues2['Roast_Oven2.Q_air2'].length - 1].val).toFixed(0);
      this.ks2_Q_O2 = this.getHistoryValues2['Roast_Oven2.Q_O2'][this.getHistoryValues2['Roast_Oven2.Q_O2'].length - 1].val.toFixed(1);
      this.ks2_T_ks = this.getHistoryValues2['Roast_Oven2.T_ks'][this.getHistoryValues2['Roast_Oven2.T_ks'].length - 1].val.toFixed(1);
      if (Number(this.ks2_T_ks) < 850) {
        this.equipmentElements[1].boolean = false;
        this.equipmentElements[40].boolean = false;
      } else {
        this.equipmentElements[1].boolean = true;
        this.equipmentElements[40].boolean = true;
      }

      this.ks3_Q_air = (this.getHistoryValues2['Roast_Oven3.Q_air'][this.getHistoryValues2['Roast_Oven3.Q_air'].length - 1].val + this.getHistoryValues2['Roast_Oven3.Q_air1'][this.getHistoryValues2['Roast_Oven3.Q_air1'].length - 1].val + this.getHistoryValues2['Roast_Oven3.Q_air2'][this.getHistoryValues2['Roast_Oven3.Q_air2'].length - 1].val).toFixed(0);
      this.ks3_Q_O2 = this.getHistoryValues2['Roast_Oven3.Q_O2'][this.getHistoryValues2['Roast_Oven3.Q_O2'].length - 1].val.toFixed(1);
      this.ks3_T_ks = this.getHistoryValues2['Roast_Oven3.T_ks'][this.getHistoryValues2['Roast_Oven3.T_ks'].length - 1].val.toFixed(1);
      if (Number(this.ks3_T_ks) < 850) {
        this.equipmentElements[2].boolean = false;
        this.equipmentElements[41].boolean = false;
      } else {
        this.equipmentElements[2].boolean = true;
        this.equipmentElements[41].boolean = true;
      }

      this.ks4_Q_air = (this.getHistoryValues2['Roast_Oven4.Q_air'][this.getHistoryValues2['Roast_Oven4.Q_air'].length - 1].val + this.getHistoryValues2['Roast_Oven4.Q_air1'][this.getHistoryValues2['Roast_Oven4.Q_air1'].length - 1].val + this.getHistoryValues2['Roast_Oven4.Q_air2'][this.getHistoryValues2['Roast_Oven4.Q_air2'].length - 1].val).toFixed(0);
      this.ks4_Q_O2 = this.getHistoryValues2['Roast_Oven4.Q_O2'][this.getHistoryValues2['Roast_Oven4.Q_O2'].length - 1].val.toFixed(1);
      this.ks4_T_ks = this.getHistoryValues2['Roast_Oven4.T_ks'][this.getHistoryValues2['Roast_Oven4.T_ks'].length - 1].val.toFixed(1);
      if (Number(this.ks4_T_ks) < 850) {
        this.equipmentElements[3].boolean = false;
        this.equipmentElements[39].boolean = false;
      } else {
        this.equipmentElements[3].boolean = true;
        this.equipmentElements[39].boolean = true;
      }

      this.ks5_Q_air = (this.getHistoryValues2['Roast_Oven5.Q_air'][this.getHistoryValues2['Roast_Oven5.Q_air'].length - 1].val + this.getHistoryValues2['Roast_Oven5.Q_air1'][this.getHistoryValues2['Roast_Oven5.Q_air1'].length - 1].val + this.getHistoryValues2['Roast_Oven5.Q_air2'][this.getHistoryValues2['Roast_Oven5.Q_air2'].length - 1].val).toFixed(1);
      this.ks5_Q_O2 = this.getHistoryValues2['Roast_Oven5.Q_O2'][this.getHistoryValues2['Roast_Oven5.Q_O2'].length - 1].val.toFixed(1);
      this.ks5_T_ks = this.getHistoryValues2['Roast_Oven5.T_ks'][this.getHistoryValues2['Roast_Oven5.T_ks'].length - 1].val.toFixed(1);
      if (Number(this.ks5_T_ks) < 850) {
        this.equipmentElements[4].boolean = false;
        this.equipmentElements[42].boolean = false;
      } else {
        this.equipmentElements[4].boolean = true;
        this.equipmentElements[42].boolean = true;
      }

      // this.RS_O2_total = this.getHistoryValues2['RS_O2_total.Value'][this.getHistoryValues2['RS_O2_total.Value'].length - 1].val.toFixed(1);

      this.RS_O2_total = (Number(this.ks1_Q_O2) + Number(this.ks2_Q_O2) + Number(this.ks3_Q_O2) + Number(this.ks4_Q_O2) + Number(this.ks5_Q_O2)).toFixed(0);



      this.KEC_Total_OE_and_KO = this.getHistoryValues2['Total_OE_and_KO.Volume'][this.getHistoryValues2['Total_OE_and_KO.Volume'].length - 1].val.toFixed(1);
      this.KEC_Total_All = this.getHistoryValues2['Total_all.Volume'][this.getHistoryValues2['Total_all.Volume'].length - 1].val.toFixed(1);

      this.KEC_Tank_VIU_OE_level = this.getHistoryValues2['VIU_OE.level'][this.getHistoryValues2['VIU_OE.level'].length - 1].val.toFixed(1);
      this.KEC_Tank_VIU_neutral_level = this.getHistoryValues2['VIU_neutral.level'][this.getHistoryValues2['VIU_neutral.level'].length - 1].val.toFixed(1);
      this.KEC_Tank_VIU_mixed_level = this.getHistoryValues2['VIU_mixed.level'][this.getHistoryValues2['VIU_mixed.level'].length - 1].val.toFixed(1);
      

      this.SAS_Vmng = this.getHistoryValues2['Sulfuric_Acid_Shop.Vmng'][this.getHistoryValues2['Sulfuric_Acid_Shop.Vmng'].length - 1].val.toFixed(1);
      this.SAS_Vmng2 = this.getHistoryValues2['Sulfuric_Acid_Shop.Vmng2'][this.getHistoryValues2['Sulfuric_Acid_Shop.Vmng2'].length - 1].val.toFixed(1);


      for (let i = 1; i <= 12; i++) {  // зеленые точки сгустителей в выщелачевательном цеху
        if (this.getHistoryValues2['Leach_Thickener' + i + '.Work'][this.getHistoryValues2['Leach_Thickener' + i + '.Work'].length - 1].val == '0') {
          (<HTMLInputElement>document.getElementById('LS_thickener' + i)).style.background = 'none';
        } else {
          if (i == 4 || i == 7 || i == 8 || i == 9 || i == 10 || i == 11) {
            (<HTMLInputElement>document.getElementById('LS_thickener' + i)).style.background = 'rgb(0, 255, 0)';
          }
          if (i == 6) {
            (<HTMLInputElement>document.getElementById('LS_thickener' + i)).style.background = 'rgb(0, 255, 255)';
          }
          if (i == 1 || i == 3 || i == 5) {
            (<HTMLInputElement>document.getElementById('LS_thickener' + i)).style.background = 'rgb(255, 255, 0)';
          }
          if (i == 2) {
            (<HTMLInputElement>document.getElementById('LS_thickener' + i)).style.background = 'rgb(170, 136, 0)';
          }
          if (i == 12) {
            (<HTMLInputElement>document.getElementById('LS_thickener' + i)).style.background = 'rgb(212, 170, 0)';
          }
        }
      }


      this.LS_pH_neutral = this.getHistoryValues2['Leaching_Shop.Ph.Neutral'][this.getHistoryValues2['Leaching_Shop.Ph.Neutral'].length - 1].val.toFixed(1);
      this.LS_pH_roasting = this.getHistoryValues2['Leaching_Shop.Ph.Roast'][this.getHistoryValues2['Leaching_Shop.Ph.Roast'].length - 1].val.toFixed(1);
      this.LS_pH_VSG = this.getHistoryValues2['Leaching_Shop.Ph.VSG'][this.getHistoryValues2['Leaching_Shop.Ph.VSG'].length - 1].val.toFixed(1);
      this.LS_pH_VSNS = this.getHistoryValues2['Leaching_Shop.Ph.VSNS'][this.getHistoryValues2['Leaching_Shop.Ph.VSNS'].length - 1].val.toFixed(1);
      if (Number(this.LS_pH_VSNS) < 5) {
        if (this.startLS_pH_VSNS == false) {
          this.alarmLS_pH_VSNS();
        }
      }
      this.LS_pH_VSKS = this.getHistoryValues2['Leaching_Shop.Ph.VSKS'][this.getHistoryValues2['Leaching_Shop.Ph.VSKS'].length - 1].val.toFixed(1);
      if (Number(this.LS_pH_VSKS) < 4) {
        if (this.startLS_pH_VSKS == false) {
          this.alarmLS_pH_VSKS();
        }
      }
      this.LS_pH_VVS_6 = this.getHistoryValues2['Leaching_Shop.Ph.VVS_6'][this.getHistoryValues2['Leaching_Shop.Ph.VVS_6'].length - 1].val.toFixed(1);
      this.LS_pH_washout = this.getHistoryValues2['Leaching_Shop.Ph.washout'][this.getHistoryValues2['Leaching_Shop.Ph.washout'].length - 1].val.toFixed(1);

      this.Waelz_Larox1 = this.getHistoryValues2['Waelz_Larox1.Work'][this.getHistoryValues2['Waelz_Larox1.Work'].length - 1].val;
      if (this.Waelz_Larox1 == '0') {
      this.equipmentElements[11].boolean = false;
      } else {
        this.equipmentElements[11].boolean = true;
      }
      this.Waelz_Larox2 = this.getHistoryValues2['Waelz_Larox2.Work'][this.getHistoryValues2['Waelz_Larox2.Work'].length - 1].val;
      if (this.Waelz_Larox2 == '0') {
        this.equipmentElements[12].boolean = false;
      } else {
        this.equipmentElements[12].boolean = true;
      }


      this.HMS_Thickener1 = this.getHistoryValues2['HMS_Thickener1.Pump.Run'][this.getHistoryValues2['HMS_Thickener1.Pump.Run'].length - 1].val;
      if (this.HMS_Thickener1 == '0') {
        this.equipmentElements[43].boolean = false;
      } else {
        this.equipmentElements[43].boolean = true;
      }
      this.HMS_Thickener2 = this.getHistoryValues2['HMS_Thickener2.Pump.Run'][this.getHistoryValues2['HMS_Thickener2.Pump.Run'].length - 1].val;
      if (this.HMS_Thickener2 == '0') {
        this.equipmentElements[44].boolean = false;
      } else {
        this.equipmentElements[44].boolean = true;
      }
      this.HMS_Thickener3 = this.getHistoryValues2['HMS_Thickener3.Pump.Run'][this.getHistoryValues2['HMS_Thickener3.Pump.Run'].length - 1].val;
      if (this.HMS_Thickener3 == '0') {
        this.equipmentElements[45].boolean = false;
      } else {
        this.equipmentElements[45].boolean = true;
      }
      this.HMS_Thickener4 = this.getHistoryValues2['HMS_Thickener4.Pump.Run'][this.getHistoryValues2['HMS_Thickener4.Pump.Run'].length - 1].val;
      if (this.HMS_Thickener4 == '0') {
        this.equipmentElements[46].boolean = false;
      } else {
        this.equipmentElements[46].boolean = true;
      }
      this.HMS_Thickener5 = this.getHistoryValues2['HMS_Thickener5.Pump.Run'][this.getHistoryValues2['HMS_Thickener5.Pump.Run'].length - 1].val;
      if (this.HMS_Thickener5 == '0') {
        this.equipmentElements[47].boolean = false;
      } else {
        this.equipmentElements[47].boolean = true;
      }

      this.HMS_Thickener5_1 = this.getHistoryValues2['HMS_Thickener5_1.Duct'][this.getHistoryValues2['HMS_Thickener5_1.Duct'].length - 1].val;
      if (this.HMS_Thickener5_1 == '0') {
        this.equipmentElements[48].boolean = false;
      } else {
        this.equipmentElements[48].boolean = true;
      }
      this.HMS_Thickener5_2 = this.getHistoryValues2['HMS_Thickener5_2.Duct'][this.getHistoryValues2['HMS_Thickener5_2.Duct'].length - 1].val;
      if (this.HMS_Thickener5_2 == '0') {
        this.equipmentElements[49].boolean = false;
      } else {
        this.equipmentElements[49].boolean = true;
      }

      this.HMS_Larox = this.getHistoryValues2['Larox.Work'][this.getHistoryValues2['Larox.Work'].length - 1].val;
      if (this.HMS_Larox == '0') {
        this.equipmentElements[13].boolean = false;
      } else {
        this.equipmentElements[13].boolean = true;
      }

      this.FB1200 = this.getHistoryValues2['FB1200.Work'][this.getHistoryValues2['FB1200.Work'].length - 1].val;
      if (this.FB1200 == '0') {
        this.equipmentElements[14].boolean = false;
      } else {
        this.equipmentElements[14].boolean = true;
      }


      this.HMS_Diefenbach14_1 = this.getHistoryValues2['Diefenbach14_1.Status'][this.getHistoryValues2['Diefenbach14_1.Status'].length - 1].val;
      if (this.HMS_Diefenbach14_1 == '0' || this.HMS_Diefenbach14_1 == '8') {
        this.equipmentElements[17].boolean = false;
      } else {
        this.equipmentElements[17].boolean = true;
      }
      this.HMS_Diefenbach14_2 = this.getHistoryValues2['Diefenbach14_2.Status'][this.getHistoryValues2['Diefenbach14_2.Status'].length - 1].val;
      if (this.HMS_Diefenbach14_2 == '0' || this.HMS_Diefenbach14_2 == '8') {
        this.equipmentElements[18].boolean = false;
      } else {
        this.equipmentElements[18].boolean = true;
      }

      this.HMS_neutral_Level = this.getHistoryValues2['HMS_neutral.Level'][this.getHistoryValues2['HMS_neutral.Level'].length - 1].val.toFixed(1);
      this.HMS_OE_Level = this.getHistoryValues2['HMS_OE.Level'][this.getHistoryValues2['HMS_OE.Level'].length - 1].val.toFixed(1);

      this.Energy_Shop_Q_water1 = this.getHistoryValues2['Energy_Shop.Q_water1'][this.getHistoryValues2['Energy_Shop.Q_water1'].length - 1].val.toFixed(1);
      this.Energy_Shop_Q_water2 = this.getHistoryValues2['Energy_Shop.Q_water2'][this.getHistoryValues2['Energy_Shop.Q_water2'].length - 1].val.toFixed(1);
      this.Energy_Shop_Q_O2_Mechel = this.getHistoryValues2['Energy_Shop.Q_O2_Mechel'][this.getHistoryValues2['Energy_Shop.Q_O2_Mechel'].length - 1].val.toFixed(1);
      this.Energy_Shop_Q_O2 = (this.getHistoryValues2['Energy_Shop.Q_O2'][this.getHistoryValues2['Energy_Shop.Q_O2'].length - 1].val * 100).toFixed(1);
      this.Energy_Shop_Q_gas = (this.getHistoryValues2['Energy_Shop.Q_gas'][this.getHistoryValues2['Energy_Shop.Q_gas'].length - 1].val*1000).toFixed(1);

      this.startStopAnimation();

    }, error => {
      console.error(error);
    });
  }vs



  bodyHttp() {  // метод формирования массива данных для граффического представления

    for (let j = 0; j < this.arrayForSvg.length; j++) {

      this.arrayForSvg[j].svgVar.selectAll("*").remove();
      this.arrayForSvg[j].sidesVar.selectAll("*").remove();

      var min: number = 1000000;
      var max: number = 0;

      for (let chartCount = 0; chartCount < this.arrayForSvg[j].charts.length; chartCount++) {  // цикл вывода массивов для графиков (их может оказаться несколько в одной системе координат)

        if (this.arrayForSvg[j].svgClass == '.Obj' || this.arrayForSvg[j].svgClass == '.Velc' || this.arrayForSvg[j].svgClass == '.Velc_Old' || this.arrayForSvg[j].svgClass == '.KEC' || this.arrayForSvg[j].svgClass == '.Gidromet') {
          this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request] = this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request].slice(100);
        }

        this.arrayForSvg[j].charts[chartCount].arrayForChart = [];

        for (let i = 0; i < this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request].length; i++) {
          if (min > this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request][i].val) {
            min = this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request][i].val;
          }
          if (max < this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request][i].val) {
            max = this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request][i].val;
          }

          this.arrayForSvg[j].charts[chartCount].arrayForChart.push({ dt: new Date(this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request][i].dt), val: this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request][i].val });
        }

        if (this.arrayForSvg[j].charts[chartCount].fill != 'rgba(0, 0, 0, 0)') {  // если график имеет непрозрачную заливку добавляем нулевые точки в начале и в конце
          this.arrayForSvg[j].charts[chartCount].arrayForChart.unshift({ dt: this.arrayForSvg[j].charts[chartCount].arrayForChart[0].dt, val: min });
          this.arrayForSvg[j].charts[chartCount].arrayForChart.push({ dt: this.arrayForSvg[j].charts[chartCount].arrayForChart[this.arrayForSvg[j].charts[chartCount].arrayForChart.length - 1].dt, val: min });
        }

      }


      this.arrayForSvg[j].arrayForAxis = [];
      // задаем массив для координатных осей с начальными и конечными значениями
      this.arrayForSvg[j].arrayForAxis.push({ dt: this.arrayForSvg[j].charts[0].arrayForChart[0].dt, val: min }, { dt: this.arrayForSvg[j].charts[0].arrayForChart[this.arrayForSvg[j].charts[0].arrayForChart.length - 1].dt, val: max });

      this.buildSvg(j);
    }

    this.activeLineAndPathObj();

    this.coloringAxis();

    if (this.startAlarmsRsCollector == false) {
      this.alarmRsCollector();
    }

  }



  coloringAxis() {
    // процедура перекрашивания координатных осей
    var ticks = document.getElementsByClassName('tick');

    for (let i = 0; i < ticks.length; i++) {
      ticks[i].children[0].attributes['stroke'].value = '#ff7f2aff';    // окрашивание сетки
      ticks[i].children[1].attributes['fill'].value = '#fff';           // окрашивание текстов
    }

    var domain = document.getElementsByClassName('domain');             // окрашивание краев
    for (let i = 0; i < domain.length; i++) {
      domain[i].attributes['stroke'].value = '#ff7f2aff';
    }
  }


  min: number = 0;
  startAlarmsRsCollector: boolean = false;  // запущен ли метод анимации мерцания графика "Разряжение в коллекторе"
  alarmRsCollector() {                      // метод анимации мерцания графика "Разряжение в коллекторе"
    var that = this;

    this.min = this.arrayForSvg[0].charts[0].arrayForChart[this.arrayForSvg[0].charts[0].arrayForChart.length - 2].val;
    
    if (this.min < 250) {
      var stroke = (<HTMLInputElement>document.getElementById('RS_R_collector.Value')).style.stroke;
      if (stroke != 'rgb(255, 50, 50)') {
        (<HTMLInputElement>document.getElementById('RS_R_collector.Value')).style.stroke = 'rgb(255, 50, 50)';
        (<HTMLInputElement>document.getElementById('RS_R_collector.Value')).style.fill = 'rgba(255, 50, 20, 0.5)';
      } else {
        (<HTMLInputElement>document.getElementById('RS_R_collector.Value')).style.stroke = 'rgb(255, 0, 0)';
        (<HTMLInputElement>document.getElementById('RS_R_collector.Value')).style.fill = 'rgba(255, 0, 0, 0.1)';
      }

      setTimeout(function () {
        that.alarmRsCollector();
      }, 1000);
      this.startAlarmsRsCollector = true;

    } else {
      (<HTMLInputElement>document.getElementById('RS_R_collector.Value')).style.stroke = this.arrayForSvg[0].charts[0].stroke;
      (<HTMLInputElement>document.getElementById('RS_R_collector.Value')).style.fill = this.arrayForSvg[0].charts[0].fill;
      this.startAlarmsRsCollector = false;
    }
  }



  startLS_pH_VSNS: boolean = false;  // запущен ли метод мерцания цифры ВСНС
  alarmLS_pH_VSNS() {
    var that = this;
    if (Number(this.LS_pH_VSNS) < 5) {
      var color = (<HTMLInputElement>document.getElementById('LS_pH_VSNS')).style.color;
      if (color != 'rgb(255, 0, 0)') {
        (<HTMLInputElement>document.getElementById('LS_pH_VSNS')).style.color = 'rgb(255, 0, 0)';
      } else {
        (<HTMLInputElement>document.getElementById('LS_pH_VSNS')).style.color = 'rgb(100, 100, 100)';
      }
      setTimeout(function () {
        that.alarmLS_pH_VSNS();
      }, 200);
      this.startLS_pH_VSNS = true;
    } else {
      (<HTMLInputElement>document.getElementById('LS_pH_VSNS')).style.color = 'rgb(0,255,0)';
      this.startLS_pH_VSNS = false;
    }
  }

  startLS_pH_VSKS: boolean = false;  // запущен ли метод мерцания цифры ВСКС
  alarmLS_pH_VSKS() {
    var that = this;
    if (Number(this.LS_pH_VSKS) < 4) {
      var color = (<HTMLInputElement>document.getElementById('LS_pH_VSKS')).style.color;
      if (color != 'rgb(255, 0, 0)') {
        (<HTMLInputElement>document.getElementById('LS_pH_VSKS')).style.color = 'rgb(255, 0, 0)';
      } else {
        (<HTMLInputElement>document.getElementById('LS_pH_VSKS')).style.color = 'rgb(100, 100, 100)';
      }
      setTimeout(function () {
        that.alarmLS_pH_VSKS();
      }, 190);
      this.startLS_pH_VSKS = true;
    } else {
      (<HTMLInputElement>document.getElementById('LS_pH_VSKS')).style.color = 'rgb(0,255,0)';
      this.startLS_pH_VSKS = false;
    }
  }





  startStopAnimation() {
    if (this.blockAnimation == false)   // если нет блокировки анимации
    {

      for (let i = 0; i < this.equipmentElements.length; i++) {
        var allChildrens: any = (<HTMLInputElement>document.getElementById(this.equipmentElements[i].id));  // выбираем элемент для анимации

        if (this.equipmentElements[i].boolean == true) {   // если объект оборудования включен...
          if (this.equipmentElements[i].run != true) {  // ...но еще не запущена раскадровка

            allChildrens.children[this.equipmentElements[i].counter].style.display = 'block';  // раскрываем стартовый кадр
            allChildrens.children[allChildrens.children.length - 1].style.display = 'none';  // скрываем стоп-кадр

            // запускаем раскадровку
            this.storyboard(this.equipmentElements[i].id, this.equipmentElements[i].counter, this.equipmentElements[i].max, this.equipmentElements[i].interval, i);
            this.equipmentElements[i].run = true;  // отмечаем, что раскадровка уже запущена
          }

        } else {     // если объект оборудования выключен

          for (let j = 0; j < allChildrens.children.length; j++) {  // скрываем все действующие кадры
            allChildrens.children[j].style.display = 'none';
          }
          allChildrens.children[allChildrens.children.length - 1].style.display = 'block';  // раскрываем стоп-кадр

          this.equipmentElements[i].run = false;  // отмечаем, что раскадровка остановлена
        }
      }

    } else {  // если блокировка анимации установлена

      for (let i = 0; i < this.equipmentElements.length; i++) {
        var allChildrens: any = (<HTMLInputElement>document.getElementById(this.equipmentElements[i].id));  // выбираем элемент для анимации

        for (let j = 0; j < allChildrens.children.length; j++) {  // скрываем все действующие кадры
          allChildrens.children[j].style.display = 'none';
        }
        allChildrens.children[allChildrens.children.length - 1].style.display = 'block';  // раскрываем стоп-кадр

        this.equipmentElements[i].run = false;  // отмечаем, что раскадровка остановлена
      }

    }
  }


  storyboard(id, counter, max, interval, index) {  // метод раскадровки
    var that = this;

    if (this.blockAnimation == false) {
      if (this.equipmentElements[index].boolean == true) {

        var allChildrens: any = (<HTMLInputElement>document.getElementById(id));
        if (counter < max) {
          allChildrens.children[counter].style.display = 'none';
          allChildrens.children[counter + 1].style.display = 'block';
          counter++;
        } else {
          allChildrens.children[counter].style.display = 'none';
          allChildrens.children[0].style.display = 'block';
          counter = 0;
        }

        setTimeout(function () {
          that.storyboard(id, counter, max, interval, index);
        }, interval);
      }
    }

  }




  showKs1() {
    (<HTMLInputElement>document.getElementById("InTouchWeb")).style.display = 'none';
    (<HTMLInputElement>document.getElementById("showKs1")).style.display = 'block';

    this.ks1.onShow = true; // отмечаем, что окно подраздела открыто для контроля анимации в нем

    // если это значение true - стартует анимация в дочернем компоненте
    this.ks1.boolean = this.equipmentElements[0].boolean;
    this.ks1.startAnimation();

    this.blockAnimation = true;
    this.startStopAnimation();
  }

  showKs2() {
    (<HTMLInputElement>document.getElementById("InTouchWeb")).style.display = 'none';
    (<HTMLInputElement>document.getElementById("showKs2")).style.display = 'block';

    this.ks2.onShow = true; // отмечаем, что окно подраздела открыто для контроля анимации в нем

    // если это значение true - стартует анимация в дочернем компоненте
    this.ks2.boolean = this.equipmentElements[1].boolean;
    this.ks2.startAnimation();

    this.blockAnimation = true;
    this.startStopAnimation();
  }




  public tPermission: number = 0;  // переключатель движения прокрутки

  rightScroll() {
    if (this.tPermission == 0) {
      var that = this;
      setTimeout(function () {
        var left: number = (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft;
        (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft = left - 5;
        that.rightScroll();
      }, 1);
    }
  }


  leftScroll() {
    if (this.tPermission == 0) {
      var that = this;
      setTimeout(function () {
        var left: number = (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft;
        (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft = left + 5;
        that.leftScroll();
      }, 1);
    }
  }




  zoomingRS() {
    var deployButtonStatus = (<HTMLInputElement>document.getElementById("deployButton")).className;
    if (deployButtonStatus == 'unDeployButton') {
      this.InTouchWebWidth = '225';
      (<HTMLInputElement>document.getElementById("regulatorWidth")).value = this.InTouchWebWidth;
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = this.InTouchWebWidth + '%';
      var InTouchWebClientWidth = (<HTMLInputElement>document.getElementById("InTouchWeb")).clientWidth;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollTop = Number(InTouchWebClientWidth) / 30;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft = Number(InTouchWebClientWidth) / 60;
      this.chartScale();
      (<HTMLInputElement>document.getElementById('deployButton')).style.display = 'none';
      (<HTMLInputElement>document.getElementById('schemeRollUp')).style.display = 'block';

      (<HTMLInputElement>document.getElementById("leftScroll")).style.display = 'block';
      (<HTMLInputElement>document.getElementById("rightScroll")).style.display = 'block';
    }
  }


  zoomingSKC() {
    var deployButtonStatus = (<HTMLInputElement>document.getElementById("deployButton")).className;
    if (deployButtonStatus == 'unDeployButton') {
      this.InTouchWebWidth = '225';
      (<HTMLInputElement>document.getElementById("regulatorWidth")).value = this.InTouchWebWidth;
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = this.InTouchWebWidth + '%';
      var InTouchWebClientWidth = (<HTMLInputElement>document.getElementById("InTouchWeb")).clientWidth;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollTop = Number(InTouchWebClientWidth) / 30;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft = Number(InTouchWebClientWidth) / 5;
      this.chartScale();
      (<HTMLInputElement>document.getElementById('deployButton')).style.display = 'none';
      (<HTMLInputElement>document.getElementById('schemeRollUp')).style.display = 'block';

      (<HTMLInputElement>document.getElementById("leftScroll")).style.display = 'block';
      (<HTMLInputElement>document.getElementById("rightScroll")).style.display = 'block';
    }
  }

  zoomingWaelz() {
    var deployButtonStatus = (<HTMLInputElement>document.getElementById("deployButton")).className;
    if (deployButtonStatus == 'unDeployButton') {
      this.InTouchWebWidth = '200';
      (<HTMLInputElement>document.getElementById("regulatorWidth")).value = this.InTouchWebWidth;
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = this.InTouchWebWidth + '%';
      var InTouchWebClientWidth = (<HTMLInputElement>document.getElementById("InTouchWeb")).clientWidth;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollTop = Number(InTouchWebClientWidth) / 30;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft = Number(InTouchWebClientWidth) / 2.06;
      this.chartScale();
      (<HTMLInputElement>document.getElementById('deployButton')).style.display = 'none';
      (<HTMLInputElement>document.getElementById('schemeRollUp')).style.display = 'block';

      (<HTMLInputElement>document.getElementById("leftScroll")).style.display = 'block';
      (<HTMLInputElement>document.getElementById("rightScroll")).style.display = 'block';
    }
  }

  zoomingLS() {
    var deployButtonStatus = (<HTMLInputElement>document.getElementById("deployButton")).className;
    if (deployButtonStatus == 'unDeployButton') {
      this.InTouchWebWidth = '220';
      (<HTMLInputElement>document.getElementById("regulatorWidth")).value = this.InTouchWebWidth;
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = this.InTouchWebWidth + '%';
      var InTouchWebClientWidth = (<HTMLInputElement>document.getElementById("InTouchWeb")).clientWidth;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollTop = Number(InTouchWebClientWidth) / 4.5;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft = Number(InTouchWebClientWidth) / 60;
      this.chartScale();
      (<HTMLInputElement>document.getElementById('deployButton')).style.display = 'none';
      (<HTMLInputElement>document.getElementById('schemeRollUp')).style.display = 'block';

      (<HTMLInputElement>document.getElementById("leftScroll")).style.display = 'block';
      (<HTMLInputElement>document.getElementById("rightScroll")).style.display = 'block';
    }
  }

  zoomingGMS() {
    var deployButtonStatus = (<HTMLInputElement>document.getElementById("deployButton")).className;
    if (deployButtonStatus == 'unDeployButton') {
      this.InTouchWebWidth = '198';
      (<HTMLInputElement>document.getElementById("regulatorWidth")).value = this.InTouchWebWidth;
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = this.InTouchWebWidth + '%';
      var InTouchWebClientWidth = (<HTMLInputElement>document.getElementById("InTouchWeb")).clientWidth;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollTop = Number(InTouchWebClientWidth) / 2.5;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft = Number(InTouchWebClientWidth) / 60;
      this.chartScale();
      (<HTMLInputElement>document.getElementById('deployButton')).style.display = 'none';
      (<HTMLInputElement>document.getElementById('schemeRollUp')).style.display = 'block';

      (<HTMLInputElement>document.getElementById("leftScroll")).style.display = 'block';
      (<HTMLInputElement>document.getElementById("rightScroll")).style.display = 'block';
    }
  }

  zoomingKEC() {
    var deployButtonStatus = (<HTMLInputElement>document.getElementById("deployButton")).className;
    if (deployButtonStatus == 'unDeployButton') {
      this.InTouchWebWidth = '195';
      (<HTMLInputElement>document.getElementById("regulatorWidth")).value = this.InTouchWebWidth;
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = this.InTouchWebWidth + '%';
      var InTouchWebClientWidth = (<HTMLInputElement>document.getElementById("InTouchWeb")).clientWidth;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollTop = Number(InTouchWebClientWidth) / 3.85;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft = Number(InTouchWebClientWidth) / 2;
      this.chartScale();
      (<HTMLInputElement>document.getElementById('deployButton')).style.display = 'none';
      (<HTMLInputElement>document.getElementById('schemeRollUp')).style.display = 'block';

      (<HTMLInputElement>document.getElementById("leftScroll")).style.display = 'block';
      (<HTMLInputElement>document.getElementById("rightScroll")).style.display = 'block';
    }
  }

  zoomingVIU() {
    var deployButtonStatus = (<HTMLInputElement>document.getElementById("deployButton")).className;
    if (deployButtonStatus == 'unDeployButton') {
      this.InTouchWebWidth = '195';
      (<HTMLInputElement>document.getElementById("regulatorWidth")).value = this.InTouchWebWidth;
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = this.InTouchWebWidth + '%';
      var InTouchWebClientWidth = (<HTMLInputElement>document.getElementById("InTouchWeb")).clientWidth;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollTop = Number(InTouchWebClientWidth) / 4.7;
      (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft = Number(InTouchWebClientWidth) / 6;
      this.chartScale();
      (<HTMLInputElement>document.getElementById('deployButton')).style.display = 'none';
      (<HTMLInputElement>document.getElementById('schemeRollUp')).style.display = 'block';

      (<HTMLInputElement>document.getElementById("leftScroll")).style.display = 'block';
      (<HTMLInputElement>document.getElementById("rightScroll")).style.display = 'block';
    }
  }


  schemeRollUp() {
    (<HTMLInputElement>document.getElementById("regulatorWidth")).value = '0';
    (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = '100%';
    (<HTMLInputElement>document.getElementById('deployButton')).style.display = 'block';
    (<HTMLInputElement>document.getElementById('schemeRollUp')).style.display = 'none';
    (<HTMLInputElement>document.getElementById("schemeWindow")).scrollTop = 0;
    (<HTMLInputElement>document.getElementById("schemeWindow")).scrollLeft = 0;

    (<HTMLInputElement>document.getElementById("leftScroll")).style.display = 'none';
    (<HTMLInputElement>document.getElementById("rightScroll")).style.display = 'none';

    this.chartScale();
  }


  deployScheme() {
    if ((<HTMLInputElement>document.getElementById("deployButton")).className == "deployButton") {
      (<HTMLInputElement>document.getElementById("regulatorWidth")).style.display = 'block';
      (<HTMLInputElement>document.getElementById("regulatorWidth")).value = '100';
      (<HTMLInputElement>document.getElementById("schemeWindow")).style.position = "fixed";
      (<HTMLInputElement>document.getElementById("schemeWindow")).style.zIndex = "990";
      (<HTMLInputElement>document.getElementById("deployButton")).className = "unDeployButton";
    } else {
      (<HTMLInputElement>document.getElementById("regulatorWidth")).style.display = 'none';
      (<HTMLInputElement>document.getElementById("schemeWindow")).style.position = "relative";
      (<HTMLInputElement>document.getElementById("schemeWindow")).style.zIndex = "1";
      (<HTMLInputElement>document.getElementById("deployButton")).className = "deployButton";
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = '100%';
      this.InTouchWebWidth = '0';
      (<HTMLInputElement>document.getElementById("regulatorWidth")).value = this.InTouchWebWidth;

      (<HTMLInputElement>document.getElementById("leftScroll")).style.display = 'none';
      (<HTMLInputElement>document.getElementById("rightScroll")).style.display = 'none';
    }

    this.chartScale();
  }

}


interface arrayForSvg {
  svgClass: string;
  sidesVar: any;
  svgVar: any;
  charts: charts[];
  arrayForAxis: any[];
}

interface charts {
  arrayForChart: any[];
  request: string;
  stroke: string;
  fill: string;
}


interface Animate {
  id: string;
  counter: number;
  max: number;
  interval: number;
  boolean: boolean;
  run: boolean;
}


interface ArrayParameters {
  dt: any;
  shortDesc: string;
  attributes: any;
  tagName: string;
  description: string;
  length: number;
}
