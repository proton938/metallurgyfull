import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
//import { NgModule } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';

import { AboutUsComponent, ContactUsComponent, HomeComponent, ToolsComponent } from './pages';

import { SharedModule } from '../shared';

import { MatTreeModule, MatIconModule, MatButtonModule, MatButton } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ks1Component } from './pages/home/obj/ks1/ks1.component';
import { Ks2Component } from './pages/home/obj/ks2/ks2.component';
import { ServicesComponent } from './pages/home/services/services.component';
import { HomeMainComponent } from './pages/home-main/home-main.component';

@NgModule({
  imports: [
    HomeRoutingModule, SharedModule, MatTreeModule, BrowserAnimationsModule, MatIconModule, MatButtonModule
  ],
  providers: [ServicesComponent
  ],
  declarations: [AboutUsComponent, ContactUsComponent, HomeComponent, ToolsComponent, Ks1Component, Ks2Component, HomeMainComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HomeModule {

}
