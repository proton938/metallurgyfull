import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { TreeParams } from "../../../core/models/TreeParams";
import { PageParams } from "../../../core/models/PageParams";

@Component({
  selector: 'app-alarms',
  templateUrl: './alarms.component.html',
  styleUrls: ['./alarms.component.css']
})
export class AlarmsComponent implements OnInit {
  public treeParams: TreeParams;
  public pageParams: PageParams;
  url: string;
  svc: HttpClient;
  arrReports: any[];
  currentDate: any;

  constructor(public tree: TreeParams, public page: PageParams, private route: ActivatedRoute, private router: Router,
    http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.treeParams = tree;
    this.pageParams = page;
    this.url = baseUrl;
    this.svc = http;
  }

  ngOnInit() {
    var currentDateTime: any = new Date();        // Выводим текущую дату
    this.currentDate = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("curDate")).value = this.currentDate;
    this.getAlarms(this.currentDate);
  }

  removeDate() {
    this.currentDate = (<HTMLInputElement>document.getElementById("curDate")).value;
    this.treeParams.arrAlarms = [];
    this.getAlarms(this.currentDate);
  }

  getAlarms(date) {
    this.svc.get<any[]>(this.url + 'api/alarms/GetAlarms2/' + date, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).subscribe(result => {  // конструкция для вывода массива из контроллера  
      this.treeParams.arrAlarms = result;
      console.log(this.treeParams.arrAlarms);
      for (let i = 0; i < this.treeParams.arrAlarms.length; i++) {
        var date: any = new Date(this.treeParams.arrAlarms[i].eventTime);
        var dateString: string = ('0' + date.getDate()).slice(-2) + ' - ' + ('0' + (date.getMonth() + 1)).slice(-2) + ' - ' + date.getFullYear();
        this.treeParams.arrAlarms[i].viewTime = dateString;
      }
    }, error => {
        console.error(error);
        alert('Ошибка запроса');
    });
  }


  setAcknowledgement(message, date) {
    this.svc.get<any[]>(this.url + 'api/alarms/SetAcknowledgement/' + message + '/' + date, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).subscribe(result => {  // конструкция для вывода массива из контроллера
      this.treeParams.arrAlarms = result;
    }, error => {
      console.error(error);
      alert('Ошибка квитирования');
    });
    this.removeDate();
  }


  onButtonClick(ev) {
    var currentTag: any = document.location.href;
    currentTag = currentTag.split('/');
    if (currentTag.length < 5) {
      currentTag = 'Main';
    } else {
      currentTag = currentTag[4];
    }
    currentTag = (<HTMLInputElement>document.getElementById(currentTag)).value;

    this.svc.get<any[]>(this.url + 'api/Reports/ShopReports/Electrical_Shop/2020-06-25/2020-06-26', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).subscribe(result => {  // конструкция для вывода массива из контроллера
      this.arrReports = result;
      alert(this.arrReports);
    }, error => console.error(error));
    /*const body = {
      name : "Electrical_Shop",
      val: "Загружено за сутки: 1213 т. В работе печи: КС-1, 2, 5 (КС-3 через забрасыватель, КС-2, 4 через форкамеру). Ограничение по расходу О2: по заводу не более 9,0 тыс. м3/ч. КС-2 управляется туго по t. На КС-2 контролируют выгрузку и состояние порогов. Режимы без изменений. Сделали данные выгрузки: 3-2-2 (огарок нормальный). Шихта: 40% Учалы, 20% Актюбинск, 10% Александринка, 10% Сибай, 10% Бурибай,10% зачистки + 2 гр./см Гай. На газоочистке в работе секции № 1, 2, 4, 7. Разрежение в чистом коллекторе -28 - -32 мм. Атмосфера удовлетворительная. Насосы исправны, циркуляция в норме. По складу без замечаний (три/два кррановщика). Выгрузка: (см. №3): 6 п/в Сибай, 2 д Александринка, 1 д зачистки: (см. №1): с р/д 1 п/в Гай, 2 д Актюбинск; занимались М. П."
    }
    const headers = { 'content-type': 'application/json' }
    console.log(body)
    this.svc.post<StringTag>('http://localhost:5001/api/parameters/CreateOptionsValue', body, { 'headers': headers }).subscribe(data => {
      alert('Успешно = ' + data);
      console.log(data);
    }, error => {
      if (error.error.text != undefined) {
        alert(body);
        console.dir(error);
      }
    });*/
  }
}

interface Alarm {
  id: string;
  eventTime: Date;
  sourceProcess: string;
  condition: string;
  limit: string;
  value: string;
  type: string;
  comment: string;
  sourceCondition: string;
  messаge: string;
}

interface StringTag {
  name: string;
  dt: Date;
  val: string;
  qual: number;
}
