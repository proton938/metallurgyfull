import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorShopReportComponent } from './editor-shop-report.component';

describe('EditorShopReportComponent', () => {
  let component: EditorShopReportComponent;
  let fixture: ComponentFixture<EditorShopReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorShopReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorShopReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
