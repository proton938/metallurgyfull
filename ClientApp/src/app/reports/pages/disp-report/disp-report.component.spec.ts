import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispReportComponent } from './disp-report.component';

describe('DispReportComponent', () => {
  let component: DispReportComponent;
  let fixture: ComponentFixture<DispReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
