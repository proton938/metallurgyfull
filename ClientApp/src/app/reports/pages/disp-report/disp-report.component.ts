import { Component, OnInit, OnDestroy, Inject, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { takeWhile, catchError, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-disp-report',
  templateUrl: './disp-report.component.html',
  styleUrls: ['./disp-report.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DispReportComponent implements OnInit {
  alive: boolean = true;
  url: string;
  svc: HttpClient;
  report: string;
  myStyle: SafeHtml;

  constructor(private route: ActivatedRoute, private router: Router, private sanitizer: DomSanitizer,
    http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.url = baseUrl;
    this.svc = http;
  }

  ngOnInit() {
    var currentDateTime: any = new Date();        // Выводим текущую дату
    (<HTMLInputElement>document.getElementById("curDate")).value = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);

    this.onShowReport();
  }

  onShowReport() {
    var currentTag: HTMLDivElement;
    currentTag = (<HTMLDivElement>document.getElementById("report"));
    // currentTag.append('<div id="content" style="white-space:nowrap">');

    var dt: Date;
    var shift: number;
    dt = new Date((<HTMLInputElement>document.getElementById('curDate')).value);
    shift = Number((<HTMLSelectElement>document.getElementById('curShift')).value);

    var template: ShopReport;
    this.svc.get<ShopReport>(this.url + 'api/Reports/GetDispReport2/' + dt.toDateString() + '/' + shift, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        tap(data => console.log('server data:', data)),
        catchError(this.handleError('getData')),
        takeWhile(() => this.alive))
      .subscribe(result => {
        template = result;
        //this.report = template.text;
        currentTag.innerHTML = template.text;
      }, error => {
          console.error(error);
          currentTag.innerHTML = '';
      });

  }

  ngOnDestroy() {
    this.alive = false;
  }

  private handleError(operation: String) {
    return (err: any) => {
      let errMsg = 'error in ${operation}() retrieving ${this.url}';
      console.log('${errMsg}:', err)
      if (err instanceof HttpErrorResponse) {
        // you could extract more info about the error if you want, e.g.:
        console.log('status: ${err.status}, ${err.statusText}');
        // errMsg = ...
      }
      return Observable.throw(errMsg);
    }
  }
}
interface ShopReport {
  name: string;
  text: string;
  dt: Date;
}


