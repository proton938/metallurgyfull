import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PageParams } from "../../../core/models/PageParams";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  splitLink = document.location.href.split('/');

  public reports: Report[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, public params: PageParams) {
    http.get<Report[]>(baseUrl + 'api/Reports/Reports2').subscribe(result => {
      this.reports = result;
    }, error => {
        console.error(error);
    });

  }

  ngOnInit() {
  }

  dotoReport(index) {
    this.params.currentShop = '' + this.reports[index].url;
  }


  unhideTree(i) {
    var display = (<HTMLInputElement>document.getElementById('content_reports_' + i)).style.display;
    if (display == 'none') {
      (<HTMLInputElement>document.getElementById('content_reports_' + i)).style.display = 'table';
      (<HTMLInputElement>document.getElementById('arrow_' + (Number(i)+1))).innerHTML = 'v';
    } else {
      (<HTMLInputElement>document.getElementById('content_reports_' + i)).style.display = 'none';
      (<HTMLInputElement>document.getElementById('arrow_' + (Number(i) + 1))).innerHTML = '>';
    }
  }

}

interface Report {
  id: string;
  url: number;
  name: number;
  description: string;
}
