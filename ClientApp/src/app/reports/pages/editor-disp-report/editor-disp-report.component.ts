import { Component, OnInit, OnDestroy, Inject } from '@angular/core';


//import * as DecoupledEditor from '@ckeditor/ckeditor5-build-decoupled-document';
/*
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import SpecialCharacters from '@ckeditor/ckeditor5-special-characters/src/specialcharacters';
import SpecialCharactersEssentials from '@ckeditor/ckeditor5-special-characters/src/specialcharactersessentials';
*/

import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router'; 
import { filter, map, takeWhile, catchError, tap } from 'rxjs/operators';
import { _throw } from 'rxjs/observable/throw';
import { Observable } from 'rxjs/Observable';
import { PageParams } from "../../../core/models/PageParams"
import { RedirectComponent } from '../../../shared/layout/pages/redirect/redirect.component';

import * as Editor from './../../../../assets/ckeditor.js'

@Component({
  selector: 'app-editor-shop-report',
  templateUrl: './editor-disp-report.component.html',
  styleUrls: ['./editor-disp-report.component.css']
})
export class EditorDispReportComponent implements OnInit {

  public Editor = Editor;

  public editorConfig = {
    fontFamily: {
      options: [
        'default',
        'Ubuntu, Arial, sans-serif',
        'Ubuntu Mono, Courier New, Courier, monospace'
      ]
    },
    toolbar: [
      'heading',
      '|',
      'bulletedList',
      'numberedList',
      'alignment',
      '|',
      'FontFamily',
      'FontSize',
      'bold',
      'italic',
      'Underline',
      'Strikethrough',
      '|',
      'link',
      'specialCharacters',
      'superscript',
      'subscript',
      '|',
      'insertTable',
      'tableColumn',
      'tableRow',
      'mergeTableCells',
      '|',
      'undo',
      'redo'
    ]
  };

  public onReady(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
    this.Editor = editor;
  }



  alive: boolean = true;
  url: string;
  svc: HttpClient;
  currentTag: string;
  currentDate: any;
  currentShift: any;

  classButtons: any[] = [
    { onreport: true, value: 'Транспортный цех', urlId: 'Tr_shop', hidden: true },
    { onreport: true, value: 'КЭЦ. Электролиз', urlId: 'KEC', hidden: false },
    { onreport: true, value: 'КЭЦ. ГМО', urlId: 'KEC_EO', hidden: false },
    { onreport: true, value: 'КЭЦ. Плавильное отделение', urlId: 'KEC_PO', hidden: false },
    { onreport: true, value: 'КЭЦ. Кадмиевое отделение', urlId: 'KEC_CdO', hidden: true },
    { onreport: true, value: 'Обжиговый цех', urlId: 'Obj', hidden: false },
    { onreport: true, value: 'Выщелачивательный цех', urlId: 'Vysh', hidden: false },
    { onreport: true, value: 'Вельц-цех', urlId: 'Velc_old', hidden: false },
    { onreport: true, value: 'КВП-5,6', urlId: 'Velc_Kvp56', hidden: false },
    { onreport: true, value: 'ГМЦ', urlId: 'Gidromet', hidden: false },
    { onreport: true, value: 'СКЦ', urlId: 'SKC', hidden: false },
    { onreport: true, value: 'ПО', urlId: 'PO', hidden: false }
  ];

  constructor(private route: ActivatedRoute, private router: Router, public params: PageParams,
    http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.url = baseUrl;
    this.svc = http;
  }

  ngOnInit() {

    var currentDateTime: any = new Date();        // Выводим текущую дату
    this.currentDate = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("curDate")).value = this.currentDate;
    this.currentShift = (<HTMLInputElement>document.getElementById("curShift")).value;
    this.getClassButtons(this.currentDate, this.currentShift);

    var currentTag: any = document.location.href;
    currentTag = currentTag.split('/');
    if (currentTag.length < 5) {
      currentTag = 'Main';
    } else {
      currentTag = currentTag[4];
    }

    for (let i = 0; this.classButtons.length; i++) {
      if (this.classButtons[i].urlId == currentTag) {
        this.onLoad(i + 1);
      }
    }

    // currentTag = (<HTMLInputElement>document.getElementById(currentTag)).value;
  }


  removeDateShift() {
    this.currentDate = (<HTMLInputElement>document.getElementById("curDate")).value;
    this.currentShift = (<HTMLInputElement>document.getElementById("curShift")).value;
    this.getClassButtons(this.currentDate, this.currentShift);
  }


  getClassButtons(date, shift) {
    var template: TemplateReport;
    this.svc.get<TemplateReport>(this.url + 'api/Reports/GetReadyReports/' + date + '/' + shift, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        tap(data => console.log('server data:', data)),
        catchError(this.handleError('getData')),
        takeWhile(() => this.alive))
      .subscribe(result => {
        template = result;

        for (let i = 0; i < this.classButtons.length; i++) {
          this.classButtons[i].onreport = true;
        }

        for (let i = 0; i < this.classButtons.length; i++) {
          for (let j = 0; j < template.length; j++) {
            if (template[j] == i + 1) {
              this.classButtons[i].onreport = false;
            }
          }
        }

        this.Editor.setData(template.text);
      }, error => {
        console.error(error);
        for (let i = 0; i < this.classButtons.length; i++) {
          this.classButtons[i].onreport = true;
        }
      });
  }


  getSavedReports(shop) {
    var template: TemplateReport;
    this.svc.get<TemplateReport>(this.url + 'api/Reports/GetTemplateReport2/' + shop + '/' + this.currentDate + '/' + this.currentShift, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        tap(data => console.log('server data:', data)),
        catchError(this.handleError('getData')),
        takeWhile(() => this.alive))
      .subscribe(result => {
        template = result;
        alert('Данные благополучно сохранены');
        this.Editor.setData(template.text);
      }, error => {
        console.error(error);
        alert(error);
      });
  }


  ngOnDestroy() {
    this.alive = false;
  }


  public onLoad(ev) {
    this.params.department = this.classButtons[ev-1].urlId;
    this.currentTag = ev;
    var template: TemplateReport;
    this.svc.get<TemplateReport>(this.url + 'api/Reports/GetTemplateReport2/' + ev + '/' + this.currentDate + '/' + this.currentShift, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        tap(data => console.log('server data:', data)),
        catchError(this.handleError('getData')),
        takeWhile(() => this.alive))
      .subscribe(result => {
        template = result;
        if (template.text == undefined) {
          template = template.value;
        }
        this.Editor.setData(template.text);
      }, error => {
          console.error(error);
          alert(error);
      });
  }

  public onSave(ev) {
    const data = this.Editor.getData();
    const body = {
      name: this.currentTag,
      text: data,
      dt: (<HTMLInputElement>document.getElementById('curDate')).value,
      shift: (<HTMLSelectElement>document.getElementById('curShift')).value,
    }
    console.log(body.dt);
    console.log(body.shift);

    const headers = { 'content-type': 'application/json' }
    var res: any;
    this.svc.post<TemplateReport>(this.url + 'api/Reports/SetTemplateReport2', body, { 'headers': headers })
      .subscribe(data => {
        console.log(data);
      }, error => {
          alert('Ошибка сохранения');
          console.dir(error);
          if (error.error.text != undefined) {
            console.dir(error);
            alert('Ошибка сохранения');
          }
      });
  }

  private handleError(operation: String) {
    return (err: any) => {
      let errMsg = 'error in ${operation}() retrieving ${this.url}';
      console.log('${errMsg}:', err)
      if (err instanceof HttpErrorResponse) {
        // you could extract more info about the error if you want, e.g.:
        console.log('status: ${err.status}, ${err.statusText}');
        // errMsg = ...
      }
      return Observable.throw(errMsg);
    }
  }

  testMetod() {
    /*
    this.svc.get(this.getHistoryValuesUrl).subscribe(result => {

      this.getHistoryValues = result;

    });
    */
  }
}

interface TemplateReport {
  value: any;
  name: string;
  text: string;
  shop: number;
  dt: Date;
  shift: number;
  length: number;
}
