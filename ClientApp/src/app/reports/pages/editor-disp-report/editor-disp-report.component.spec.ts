import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorDispReportComponent } from './editor-disp-report.component';

describe('EditorDispReportComponent', () => {
  let component: EditorDispReportComponent;
  let fixture: ComponentFixture<EditorDispReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorDispReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorDispReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
