import { Component, OnInit } from '@angular/core';
import { PageParams } from "../../../core/models/PageParams";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})

export class ReportComponent implements OnInit {

  url: string;
  urlSafe: SafeResourceUrl;

  constructor(public params: PageParams, public sanitizer: DomSanitizer) {
    this.url = this.params.currentShop;
  }

  ngOnInit() {
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    alert(this.url);
  }

}
