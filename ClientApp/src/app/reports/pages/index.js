"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./reports/reports.component"));
__export(require("./report/report.component"));
__export(require("./editor-shop-report/editor-shop-report.component"));
__export(require("./editor-disp-report/editor-disp-report.component"));
__export(require("./disp-report/disp-report.component"));
__export(require("./alarms/alarms.component"));
//# sourceMappingURL=index.js.map