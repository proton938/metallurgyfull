import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { ReportsRoutingModule } from './reports-routing.module';

import { ReportsComponent, ReportComponent, EditorShopReportComponent, DispReportComponent, AlarmsComponent, EditorDispReportComponent } from './pages';

@NgModule({
  imports: [
    CKEditorModule,
    CommonModule,
    ReportsRoutingModule
  ],
  declarations: [ReportsComponent, ReportComponent, EditorShopReportComponent, DispReportComponent, AlarmsComponent, EditorDispReportComponent]
})
export class ReportsModule { }
