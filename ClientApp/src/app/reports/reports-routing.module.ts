import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent, ReportComponent, EditorShopReportComponent, DispReportComponent, AlarmsComponent, EditorDispReportComponent } from './pages';

const routes: Routes = [
  { path: 'reports', component: ReportsComponent },
  { path: 'reports/:id', component: ReportsComponent },
  { path: 'report', component: ReportComponent },
  { path: 'report/:id', component: ReportComponent },
  { path: 'editor', component: EditorShopReportComponent },
  { path: 'editor/:id', component: EditorShopReportComponent },
  { path: 'editor-disp', component: EditorDispReportComponent },
  { path: 'editor-disp/:id', component: EditorDispReportComponent },
  { path: 'disp', component: DispReportComponent },
  { path: 'disp/:id', component: DispReportComponent },
  { path: 'alarms', component: AlarmsComponent },
  { path: 'alarms/:id', component: AlarmsComponent }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
