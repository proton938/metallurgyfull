import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
//import { IntouchUrlService } from '../../../core/services/intouch-url.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';
import { PageParams } from "../../../core/models/PageParams";

@Component({
  selector: 'app-mnemoscheme',
  templateUrl: './mnemoscheme.component.html',
  styleUrls: ['./mnemoscheme.component.css']
})

export class MnemoschemeComponent implements OnInit {
  area: string;
  url: String;
  sanitizer: DomSanitizer;

  public baseUrl: string;
  public pageParams: PageParams;

  constructor(@Inject('BASE_URL') baseUrl: string, private params: PageParams, private http: HttpClient, private route: ActivatedRoute, private router: Router, private san: DomSanitizer/*, private urlService: IntouchUrlService*/) {
    this.area = "test";
    this.sanitizer = san;
    this.baseUrl = baseUrl;
    this.pageParams = params;
  }

  ngOnInit() {
    this.area = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        params.get('id'))).toString();

    // this.scaleIFrame();

    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');


    if (currentUrl[4] != 'Main') {
      (<HTMLInputElement>document.getElementById("inputText")).style.display = "block";
    } else {
      (<HTMLInputElement>document.getElementById("inputText")).style.display = "none";
    }
  }


  dataBufer: any;

  addValue() {
    var infoInput: string = (<HTMLInputElement>document.getElementById("infoInput")).value;

    /*
    this.http.get(this.baseUrl + 'api/parameters/CreateOptionsValue/' + this.pageParams.currentTagName + '/' + infoInput).subscribe(result => {
      this.dataBufer = result;
      alert('Данные благополучно отправлены.');
    }, error => {
        console.error(error);
        alert('');
    });
    */

    
     const body = {
      name: this.pageParams.currentTagName,
      val: infoInput
    };

    return this.http.post<StringTag[]>(this.baseUrl + 'api/parameters/CreateOptionsValue', body).subscribe(data => {
      alert('Успешно = ' + data);
      console.log(data);
      alert('Успешно');
    }, error => {
        console.log(error);
        alert('Данные благополучно отправлены');
    });
    
  }


  photoURL() {
    this.area = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        params.get('id'))).toString();
    let id = this.route.snapshot.paramMap.get('id');
    this.url = "http://ZN-WWSP-01.zinc.ru/intouchweb/api/symbol/" + id.toString();
    return this.url;

  }

  // http://zinc-sp2017/intouchweb/api/symbol/

  scaleIFrame() {
    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');

    currentUrl = currentUrl[4];

    if (currentUrl == "Energ") {
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = "500px";
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.height = "400px";
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.marginLeft = "30px";
    }

    if (currentUrl == "Electr") {

      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = "900px";
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.height = "400px";
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.marginLeft = "30px";
    }

  }

    //onRefresh() {
    //  this.url = "test";
    //}
}



interface StringTag {
  name: string;
  dt: any;
  val: string;
  qual: number;
}

