import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkordersComponent, WorkorderComponent } from './pages';

const routes: Routes = [
  { path: 'workorders', component: WorkordersComponent },
  { path: 'workorders/:id', component: WorkordersComponent },
  { path: 'workorder', component: WorkorderComponent },
  { path: 'workorder/:id', component: WorkorderComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkOrdersRoutingModule { }
