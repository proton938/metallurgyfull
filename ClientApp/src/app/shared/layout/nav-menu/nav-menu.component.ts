import { Component, Inject, OnInit, ViewChild, ElementRef, Injectable } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { TreeParams } from "../../../core/models/TreeParams";

import { PageParams } from "../../../core/models/PageParams";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

import { RedirectComponent } from '../pages/redirect/redirect.component';

@Injectable()
export class WithCredentialsInterceptor implements HttpInterceptor {


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({
      withCredentials: true
    });

    return next.handle(request);
  }
}

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  public treeParams: TreeParams;

  private auth: Auth;
  //private options = new RequestOptions({ withCredentials: true });
  public headers = new HttpHeaders({
    'Autorization': 'Windows'
  });

  // public values: Value[];

  public values: any[] = [
    {
      "abbreviation": "Челябинский Цинковый Завод",
      "areas": [
        {
          "abbreviation": "Обжиговый цех",
          "areas": [
          ],
          "attributes": [
          ],
          "basedOn": "$Area",
          "containedName": "Roasting_Shop",
          "container": "Chel_Zinc_Plant",
          "hierarchicalName": "Chel_Zinc_Plant.Roasting_Shop",
          "id": 0,
          "mnemoDesc": "Мнемосхема ОЦ",
          "mnemoRef": "Obj",
          "mnemoType": "Symbol",
          "parent": "Chel_Zinc_Plant",
          "securityGroup": "Roasting_Group",
          "shortDesc": "Обжиговый цех",
          "tagName": "Roasting_Shop"
        },
        {
          "abbreviation": "Сернокислотный цех",
          "areas": [
          ],
          "attributes": [
          ],
          "basedOn": "$Area",
          "containedName": "Sulfuric_Acid_Shop",
          "container": "Chel_Zinc_Plant",
          "hierarchicalName": "Chel_Zinc_Plant.Sulfuric_Acid_Shop",
          "id": 1,
          "mnemoDesc": "Мнемосхема СКЦ",
          "mnemoRef": "SKC",
          "mnemoType": "Symbol",
          "parent": "Chel_Zinc_Plant",
          "securityGroup": "Default",
          "shortDesc": "Сернокислотный цех",
          "tagName": "Sulfuric_Acid_Shop"
        },
        {
          "abbreviation": "Вельц цех",
          "areas": [],
          "attributes": [
          ],
          "basedOn": "$Area",
          "containedName": "Waelz_Shop",
          "container": "Chel_Zinc_Plant",
          "hierarchicalName": "Chel_Zinc_Plant.Waelz_Shop",
          "id": 2,
          "mnemoDesc": "Мнемосхема ВЦ",
          "mnemoRef": "Velc",
          "mnemoType": "Symbol",
          "parent": "Chel_Zinc_Plant",
          "securityGroup": "Default",
          "shortDesc": "Вельц цех",
          "tagName": "Waelz_Shop"
        },
        {
          "abbreviation": "Выщелачивательный цех",
          "areas": [
          ],
          "attributes": [
          ],
          "basedOn": "$Area",
          "containedName": "Leaching_Shop",
          "container": "Chel_Zinc_Plant",
          "hierarchicalName": "Chel_Zinc_Plant.Leaching_Shop",
          "id": 3,
          "mnemoDesc": "Мнемосхема ВыщЦ",
          "mnemoRef": "Vysh",
          "mnemoType": "Symbol",
          "parent": "Chel_Zinc_Plant",
          "securityGroup": "Leaching_Group",
          "shortDesc": "Выщелачивательный цех",
          "tagName": "Leaching_Shop"
        },
        {
          "abbreviation": "Комплекс Электролиза Цинка",
          "areas": [
            {
              "abbreviation": "КЭЦ ООР",
              "areas": [
              ],
              "attributes": [
              ],
              "basedOn": "$Area",
              "containedName": "Solution_Cleaning_Area",
              "container": "Zinc_Electrolysis_Shop",
              "hierarchicalName": "Chel_Zinc_Plant.Zinc_Electrolysis_Shop.Solution_Cleaning_Area",
              "id": 9,
              "mnemoDesc": "Мнемосхема КЭЦ ООР",
              "mnemoRef": "KEC_EO",
              "mnemoType": "Symbol",
              "parent": "Zinc_Electrolysis_Shop",
              "securityGroup": "Default",
              "shortDesc": "Отделение очистки растворов",
              "tagName": "Solution_Cleaning_Area"
            },
            {
              "abbreviation": "КЭЦ ПО",
              "areas": [
              ],
              "attributes": [
              ],
              "basedOn": "$Area",
              "containedName": "Smelting_Area",
              "container": "Zinc_Electrolysis_Shop",
              "hierarchicalName": "Chel_Zinc_Plant.Zinc_Electrolysis_Shop.Smelting_Area",
              "id": 10,
              "mnemoDesc": "Мнемосхема КЭЦ ПО",
              "mnemoRef": "KEC_PO",
              "mnemoType": "Symbol",
              "parent": "Zinc_Electrolysis_Shop",
              "securityGroup": "Default",
              "shortDesc": "Плавильное отделение",
              "tagName": "Smelting_Area"
            },
		        {
              "abbreviation": "Электролиз",
              "areas": [
              ],
              "attributes": [
              ],
              "basedOn": "$Area",
              "containedName": "Electrical_Shop",
              "container": "Chel_Zinc_Plant",
              "hierarchicalName": "Chel_Zinc_Plant.Electrical_Shop",
              "id": 7,
              "mnemoDesc": "Мнемосхема электротехнического цеха",
              "mnemoRef": "Electr",
              "mnemoType": "Symbol",
              "parent": "Chel_Zinc_Plant",
              "securityGroup": "Default",
              "shortDesc": "Электролиз",
              "tagName": "Electrical_Shop"
            }
          ],
          "attributes": [
          ],
          "basedOn": "$Area",
          "containedName": "Zinc_Electrolysis_Shop",
          "container": "Chel_Zinc_Plant",
          "hierarchicalName": "Chel_Zinc_Plant.Zinc_Electrolysis_Shop",
          "id": 4,
          "mnemoDesc": "Мнемосхема КЭЦ",
          "mnemoRef": "KEC",
          "mnemoType": "Symbol",
          "parent": "Chel_Zinc_Plant",
          "securityGroup": "Default",
          "shortDesc": "Комплекс Электролиза Цинка",
          "tagName": "Zinc_Electrolysis_Shop"
        },
        {
          "abbreviation": "ГМЦ",
          "areas": [
          ],
          "attributes": [
          ],
          "basedOn": "$Area",
          "containedName": "Hydro_Metallurgical_Shop",
          "container": "Chel_Zinc_Plant",
          "hierarchicalName": "Chel_Zinc_Plant.Hydro_Metallurgical_Shop",
          "id": 5,
          "mnemoDesc": "Мнемосхема ГМЦ",
          "mnemoRef": "Gidromet",
          "mnemoType": "Symbol",
          "parent": "Chel_Zinc_Plant",
          "securityGroup": "Default",
          "shortDesc": "Гидрометаллургический цех",
          "tagName": "Hydro_Metallurgical_Shop"
        },
        {
          "abbreviation": "Энергоресурсы",
          "areas": [
          ],
          "attributes": [
          ],
          "basedOn": "$Area",
          "containedName": "Energy_Shop",
          "container": "Chel_Zinc_Plant",
          "hierarchicalName": "Chel_Zinc_Plant.Energy_Shop",
          "id": 6,
          "mnemoDesc": "Мнемосхема энергоцеха",
          "mnemoRef": "Energ",
          "mnemoType": "Symbol",
          "parent": "Chel_Zinc_Plant",
          "securityGroup": "Default",
          "shortDesc": "Энергоресурсы",
          "tagName": "Energy_Shop"
        }
      ],
      "attributes": [
      ],
      "basedOn": "$Area",
      "containedName": "",
      "container": null,
      "hierarchicalName": "Chel_Zinc_Plant",
      "id": 0,
      "mnemoDesc": "Мнемосхема завода",
      "mnemoRef": "Main",
      "mnemoType": "Symbol",
      "parent": null,
      "securityGroup": "Default",
      "shortDesc": "Челябинский Цинковый Завод",
      "tagName": "Chel_Zinc_Plant"
    }
  ];

  public currentPage;
  public countArrayURL;
  public sum: string = '';
  public varMenu: any = 0;

  public arrayExistingPage: any = [
    { pageName: 'mnemoscheme' },
    { pageName: 'specifications' },
    { pageName: 'archive' },
    { pageNmae: 'workorders' },
    { pageName: 'analytics' },
    { pageName: 'predictiveanalytics' },
    { pageName: 'reports' },
    { pageName: 'stk' },
    { pageName: 'knowledge' },  
  ];

  isExpanded = false;

  public pageParams: PageParams;


  constructor(private route: ActivatedRoute, private router: Router, private params: PageParams,  // праметры для роутинга
    public tree: TreeParams,   // загрузка модели дерева параметров
    http: HttpClient, @Inject('BASE_URL') baseUrl: string  // параметры для http-запроса
  ) {
    this.pageParams = params;
    this.treeParams = tree;

    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');

    if (currentUrl[3] == '' || currentUrl[3] == 'redirect') {
      this.pageParams.page = 'mnemoscheme';
    } else {
      this.pageParams.page = currentUrl[3];
    }

    if (currentUrl[4] != 0 || currentUrl[4] != 'mnemoscheme' || currentUrl != undefined) {
      this.pageParams.currentShop = currentUrl[4];
    } else {
      this.pageParams.currentShop = 'ЧЦЗ';
      this.pageParams.currentTagName = (<HTMLInputElement>document.getElementById("Main")).value;
    }

    //var username: string = JSON.parse(localStorage.getItem('currentUser'));

    /*
    http.get<Value[]>(baseUrl + 'api/equipments/getareas', {
      headers: new HttpHeaders({
        'Test': 'Test',
        'Content-Type': 'application/json',
      })
    }).subscribe(result => {  // конструкция для вывода массива из контроллера
      this.values = result;
    }, error => {
        console.error(error);
        http.get<Value[]>('assets/replase_server/GetAreas2.json').subscribe((result) => {
          this.values = result;
          alert(JSON.stringify(this.values));
        });
    }
    );
    */

  }
  

  getAlt() {
    let elem = document.getElementById('showShortDesc');
    document.addEventListener('mousemove', function (event) {
     // elem.innerHTML = event.pageX + ' : ' + event.pageY;
      (<HTMLInputElement>document.getElementById("showShortDesc")).style.left = event.pageX + 'px';
      (<HTMLInputElement>document.getElementById("showShortDesc")).style.top = event.pageY - window.pageYOffset + 20 + 'px';
    });
    (<HTMLInputElement>document.getElementById("showShortDesc")).style.display = 'block';
  }

  unGetAlt() {
    (<HTMLInputElement>document.getElementById("showShortDesc")).style.left = 0 + 'px';
    (<HTMLInputElement>document.getElementById("showShortDesc")).style.top = 1200 + 'px';
    (<HTMLInputElement>document.getElementById("showShortDesc")).style.display = 'none';
  }




  collapse(dep: string, id: string) {

    this.isExpanded = false;
    this.params.setDepartment(dep);

    this.treeParams.updateSelectedPrmsModel();
    this.treeParams.getTreeParameters = [];
    this.treeParams.zoomMemory = [];

    
    (<HTMLInputElement>document.getElementById(id)).style.color = "#fff";


    if (this.pageParams.currentShop != '') {
      (<HTMLInputElement>document.getElementById(this.pageParams.currentShop)).style.color = "#999";
    }

    (<HTMLInputElement>document.getElementById("SOUP_HCZ")).style.color = "#999";


    this.pageParams.currentShop = id;

  }


  gotoSOUP_HCZ() {

    (<HTMLInputElement>document.getElementById(this.pageParams.page)).style.background = "#fff";

    if (this.pageParams.currentShop != '') {
      (<HTMLInputElement>document.getElementById(this.pageParams.currentShop)).style.color = "#999";
    }
    (<HTMLInputElement>document.getElementById("SOUP_HCZ")).style.color = "#fff";
    this.pageParams.currentShop = 'SOUP_HCZ';

    this.pageParams.page = 'mnemoscheme';
  }


  overStyle(hover) {
    (<HTMLInputElement>document.getElementById(hover)).style.color = "#fff";
  }
  outStyle(out) {
    if (this.pageParams.currentShop != out) {
      (<HTMLInputElement>document.getElementById(out)).style.color = "#999";
    }
  }



  toggle() {
    this.isExpanded = !this.isExpanded;
  }



  ngOnInit() {
    if (this.pageParams.currentShop != '') {
      (<HTMLInputElement>document.getElementById(this.pageParams.currentShop)).style.color = "#fff";
    }


  }



  private extractData<T>(res: Response) {
    if (res.status < 200 || res.status >= 300) {
      throw new Error('Bad response status: ' + res.status);
    }
    const body = res.json ? res.json() : null;
    return <T>(body || {});
  }

}



interface Value {
  id: number;
  abbreviation: string;
  areas: any;
}

export class Auth {
  isAdmin: boolean;
  isUser: boolean;
}
