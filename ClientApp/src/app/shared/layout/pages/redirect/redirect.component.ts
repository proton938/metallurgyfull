import { Component, OnInit, Input, Output } from '@angular/core';
import { PageParams } from "../../../../core/models/PageParams"
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';

// import * as fs from 'fs';


@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.css']
})
export class RedirectComponent implements OnInit {

  private pageParams: PageParams;

  constructor(private route: ActivatedRoute, private router: Router, private params: PageParams) {
    this.pageParams = params;
  }

  ngOnInit() {
  }

  redirect() {
    let page = this.route.snapshot.paramMap.get('page');
    let area = this.route.snapshot.paramMap.get('area');
    let curArea = this.pageParams.getDepartment();
    let curPage = this.pageParams.getPage();
   // let fs = require('fs');
    if (page != undefined || page != '') {
      this.router.navigate([page, area]);
    }
    else {
      this.router.navigate(['\mnemoscheme', area]);
    }



    /*
    else if (curArea == undefined)
      this.router.navigate([curPage, 'Main']);
    else if (curPage == undefined)
      this.router.navigate(['\mnemoscheme', curArea]);
   //  document.getElementById('crren_url').value = page + '/' + area;
   */
  }
}
