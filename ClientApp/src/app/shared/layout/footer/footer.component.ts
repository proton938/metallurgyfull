import { Component, OnInit, Inject} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { TreeParams } from "../../../core/models/TreeParams";
import { PageParams } from "../../../core/models/PageParams";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})

export class FooterComponent implements OnInit {

  public treeParams: TreeParams;
  public pageParams: PageParams;
  url: string;
  svc: HttpClient;

  localArrAlarms: any[];       // локальный массив буфера result - если находимся в разделе "Алармы" и не в текущей дате модель pageParams.page не должна обновляться
  currentDate: any;
  newAlarms: boolean = false;   // ngClass отображения кнопки новых алармов
  prepNewAlarms: boolean = false;  // буфер для newAlarms
  alarmCounter: number = 0;
  allNewAlarms: number = 0;


  constructor(public tree: TreeParams, public page: PageParams, http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    this.treeParams = tree;
    this.pageParams = page;

    this.url = baseUrl;
    this.svc = http;

    http.get(baseUrl + 'api/Users/GetUser').subscribe(result => {  // конструкция для вывода массива из контроллера
      this.treeParams.user = result;
    }, error => {
        console.error(error);
    });

  }


  ngOnInit() {
    this.replayGetAlarms();

    this.circleAnimation();
  }



  replayGetAlarms() {
    var that = this;

    var currentDateTime: any = new Date();        // Выводим текущую дату
    this.currentDate = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);

    this.getAlarms(this.currentDate);

    setTimeout(function () {
      that.replayGetAlarms();
    }, 10000);
  }



  getAlarms(date) {

    this.svc.get<any[]>(this.url + 'api/alarms/GetAlarms2/' + date, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).subscribe(result => {  // конструкция для вывода массива из контроллера

      if (this.pageParams.page != 'alarms')  // если находимся в разделе "Алармы" - массив алармов не обновляется пока не появится новый аларм
      {
        this.treeParams.arrAlarms = result;  // передаем данные в модель
        this.bodyHttp();
      } else {
        var curDate: any = (<HTMLInputElement>document.getElementById('curDate')).value;
        curDate = curDate.split('-');
        curDate = curDate[0] + '-' + curDate[1] + '-' + curDate[2];
        if (curDate == this.currentDate) {                           // если в разделе "Алармы" выбрана текущая дата
          this.treeParams.arrAlarms = result;  // передаем данные в модель
          this.bodyHttp();
        } else {
          this.localArrAlarms = result;
          this.prepNewAlarms = false;
          this.alarmCounter = 0;
          for (let i = 0; i < this.localArrAlarms.length; i++) {
            if (this.localArrAlarms[i].ack == false) {
              this.prepNewAlarms = true;
              this.alarmCounter++;
            }
          }
          this.newAlarms = this.prepNewAlarms;
          this.allNewAlarms = this.alarmCounter;
        }
      } 

    }, error => {
      console.error(error);
    });

  }



  bodyHttp() {
    console.log(this.treeParams.arrAlarms);
    this.prepNewAlarms = false;
    this.alarmCounter = 0;
    for (let i = 0; i < this.treeParams.arrAlarms.length; i++) {
      var date: any = new Date(this.treeParams.arrAlarms[i].eventTime);
      var dateString: string = ('0' + date.getDate()).slice(-2) + ' - ' + ('0' + (date.getMonth() + 1)).slice(-2) + ' - ' + date.getFullYear();
      this.treeParams.arrAlarms[i].viewTime = dateString;
      if (this.treeParams.arrAlarms[i].ack == false) {
        this.prepNewAlarms = true;
        this.alarmCounter++;
      }
    }
    this.newAlarms = this.prepNewAlarms;
    this.allNewAlarms = this.alarmCounter;
  }



  circleAnimation() {
    var that = this;

    var circle1 = (<HTMLInputElement>document.getElementById("circle1")).style.opacity;
    if (circle1 == '1') {
      (<HTMLInputElement>document.getElementById("circle1")).style.opacity = '0.3';
      (<HTMLInputElement>document.getElementById("circle2")).style.opacity = '1';
    } else {
      (<HTMLInputElement>document.getElementById("circle1")).style.opacity = '1';
      (<HTMLInputElement>document.getElementById("circle2")).style.opacity = '0.3';
    }

    setTimeout(function () {
      that.circleAnimation();
    }, 500);
  }


}
