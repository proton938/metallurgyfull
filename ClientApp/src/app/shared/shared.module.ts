import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { HeaderComponent, FooterComponent } from './layout';
import { RedirectComponent } from './layout/pages/redirect/redirect.component';
import { SharedRoutingModule } from './shared-routing.module';
import { ToolsComponent } from './layout/pages/tools/tools.component';
import { LoginComponent } from './layout/pages/login/login.component';
import { ChartToolbarComponent } from './layout/chart-toolbar/chart-toolbar.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    SharedRoutingModule,
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    RedirectComponent,
    ToolsComponent,
    LoginComponent,
    ChartToolbarComponent],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    HeaderComponent,
    FooterComponent,
    ChartToolbarComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
