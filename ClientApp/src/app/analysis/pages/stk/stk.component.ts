import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3Path from 'd3-path';

@Component({
  selector: 'app-stk',
  templateUrl: './stk.component.html',
  styleUrls: ['./stk.component.css']
})
export class StkComponent implements OnInit {

  dispExcel: boolean = false;

  url: string;
  svc: HttpClient;
  public arrDates: any[] = [];
  public arrResults: ChemResult[];
  public arrSampleTypes: any[];
  code: number;
  fromdate: any;
  todate: any;

  private margin = { top: 30, right: 0, bottom: 40, left: 80 };
  private chart: any;
  private chart2: any;
  private x: any;
  private y: any;
  private width: number;
  private height: number;
  public arrayForChart: any[] = [];
  public arrayForAxis: any[] = [];
  private line: d3Shape.Line<[number, number]>; // this is line defination
  public max: number;
  public min: number;


  constructor(private route: ActivatedRoute, private router: Router,
    http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.url = baseUrl;
    this.svc = http;

    var currentDateTime: any = new Date();        // Выводим текущую дату

    var dateFrom = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + '01';
    this.fromdate = dateFrom;

    var dateTo = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
    this.todate = dateTo;

    http.get<any[]>(baseUrl + 'api/analysis/GetSampleNames2/' + this.fromdate + '/' + this.todate, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).subscribe(result => {  // конструкция для вывода массива из контроллера
      this.arrSampleTypes = result;
    }, error => console.error(error));
  }

  ngOnInit() {

    var currentDateTime: any = new Date();        // Выводим текущую дату

    var dateFrom = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + '01';
    (<HTMLInputElement>document.getElementById("from")).value = dateFrom;
    this.fromdate = dateFrom;

    var dateTo = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("to")).value = dateTo;
    this.todate = dateTo;

    
    this.buildChart();

    window.addEventListener('scroll', this.scroll, true);

  }


  scroll = (): void => {
    var obj: any = (<HTMLInputElement>document.getElementById("relativePanel"));
    var rect: any = obj.getBoundingClientRect();                                                        // абсолютная высота заголовка таблицы
    if (rect.top < 0) {
      (<HTMLInputElement>document.getElementById("fixedPanel")).style.display = 'block';
      (<HTMLInputElement>document.getElementById("fixedPanel")).style.width = rect.width + 'px';

      (<HTMLInputElement>document.getElementById("from2")).value = (<HTMLInputElement>document.getElementById("from")).value;
      (<HTMLInputElement>document.getElementById("to2")).value = (<HTMLInputElement>document.getElementById("to")).value;
    } else {
      (<HTMLInputElement>document.getElementById("fixedPanel")).style.display = 'none';
    }
  };



  onChangedCode(ev) {
    this.code = ev.target.value;
    //this.onChanged();
    this.dispExcel = true;
  }

  onChangedFromDate(ev) {
    this.fromdate = ev.target.value;
    //this.onChanged();
  }

  onChangedToDate(ev) {
    this.todate = ev.target.value;
    //this.onChanged();
  }


  exportToExcel() {
    this.svc.get(this.url + 'api/analysis/GetCompValuesToExcel/' + this.code + '/' + this.fromdate + '/' + this.todate, {
      responseType: 'blob'
    })
      .subscribe(result => {
        var fileName = 'chemical_analysis';
        const blob = new Blob([result], { type: 'application/vnd.ms.excel' });
        const file = new File([blob], fileName + '.xlsx', { type: 'application/vnd.ms.excel' });
        saveAs(file);
        console.log(result);
      }, error => {
          alert('Ошибка!');
        console.error(error);
      });
  }

  OnClickGetResults() {

    this.arrResults = [];

    this.svc.get<any[]>(this.url + 'api/analysis/GetCompValues2/' + this.code + '/' + this.fromdate + '/' + this.todate, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).subscribe(result => {
      this.arrResults = result;
      this.arrDates = [];
      for (let i = 0; i < this.arrResults[0].values.length; i++) {
        this.arrDates.push(this.arrResults[0].values[i].dt);
      }
    }, error => {
      console.error(error);
    });

    //for (let i = 0; i < this.arrResults[0].values.length; i++) {
    //  this.arrDates.push(this.arrResults[0].values[i].dt);
    //}
  }

  onCompClick(j) {

    this.max = 0;
    this.min = 1000000;

    this.chart.selectAll("*").remove();
    this.chart2.selectAll("*").remove();

    this.arrayForChart = [];
    this.arrayForAxis = [];

    for (let i = 0; i < this.arrResults[j].values.length; i++) {  // получен ассоциативный массив - по ключу обращаемся к дочернему нумерованному, для получения длины

      this.arrayForChart.push({ dt: new Date(this.arrResults[j].values[i].dt), val: this.arrResults[j].values[i].val });
      if (this.max < this.arrResults[j].values[i].val) {
        this.max = this.arrResults[j].values[i].val;
      }
      if (this.min > this.arrResults[j].values[i].val) {
        this.min = this.arrResults[j].values[i].val;
      }
    }

    if (this.arrResults[j].min < this.min) {
      this.arrayForAxis.push({ dt: this.arrayForChart[0].dt, val: this.arrResults[j].min });
    } else {
      this.arrayForAxis.push({ dt: this.arrayForChart[0].dt, val: this.min });
    }

    if (this.arrResults[j].max > this.max) {
      this.arrayForAxis.push({ dt: this.arrayForChart[this.arrayForChart.length - 1].dt, val: this.arrResults[j].max });
    } else {
      this.arrayForAxis.push({ dt: this.arrayForChart[this.arrayForChart.length - 1].dt, val: this.max });
    }

    var documentWidth: number = document.body.clientWidth;
    this.width = documentWidth/1.9;
    this.height = 200;
    this.buildChart();
    this.activeAddXandYAxis1();
    this.activeLineAndPath1();
    this.addObject1(j);
    this.lineMinMax(j);
  }



  buildChart() {
    this.chart = d3.select('.chart')
      .append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
    this.chart2 = d3.select('.chart2')
      .append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  private activeAddXandYAxis1() {
    // range of data configuring
    this.x = d3Scale.scaleTime().range([0, this.width]);
    this.y = d3Scale.scaleLinear().range([this.height, 0]);
    this.x.domain(d3Array.extent(this.arrayForAxis, (d) => d.dt));
    this.y.domain(d3Array.extent(this.arrayForAxis, (d) => d.val));

    // Configure the X Axis
    this.chart.append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .attr('style', 'stroke-opacity: 0.1')
      .call(d3Axis.axisBottom(this.x)
        .tickSize(-this.height)
        .ticks(10));

    // Configure the Y Axis
    this.chart.append('g')
      .attr('style', 'stroke-opacity: 0.2')
      .call(d3Axis.axisLeft(this.y)
        .tickSize(-this.width)
        .ticks(10))

    this.chart2.append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .attr('style', 'stroke-opacity: 0.1')
      .call(d3Axis.axisBottom(this.x)
        .tickSize(-this.height)
        .ticks(10));

    // Configure the Y Axis
    this.chart2.append('g')
      .attr('style', 'stroke-opacity: 0.2')
      .call(d3Axis.axisLeft(this.y)
        .tickSize(-this.width)
        .ticks(10))
  }




  private addObject1(n) {
    this.chart.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', -50)
      .attr('x', 0)
      .attr('text-anchor', 'end')
      .text('Величина');

    this.chart.append('text')
      .attr('transform', 'rotate(-0)')
      .attr('y', -10)
      .attr('x', this.width)
      .attr('text-anchor', 'end')
      .text('min: ' + this.arrResults[n].min + '    max:  ' + this.arrResults[n].max);

    this.chart2.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', -50)
      .attr('x', 0)
      .attr('text-anchor', 'end')
      .text('Величина');

    this.chart2.append('text')
      .attr('transform', 'rotate(-0)')
      .attr('y', -10)
      .attr('x', this.width)
      .attr('text-anchor', 'end')
      .text('min: ' + this.arrResults[n].min + '    max:  ' + this.arrResults[n].max);
  }


  private activeLineAndPath1() {        // вывод графиков
    this.line = d3Shape.line()
      .x((d: any) => this.x(d.dt))
      .y((d: any) => this.y(d.val));
    // Configuring line path
    this.chart.append('path')
      .datum(this.arrayForChart)
      .attr('stroke', '#feb41b')
      .attr('fill', 'rgba(255, 255, 255, 0)')
      .attr('stroke-width', '2')
      .attr('d', this.line)

    this.chart2.append('path')
      .datum(this.arrayForChart)
      .attr('stroke', '#feb41b')
      .attr('fill', 'rgba(255, 255, 255, 0)')
      .attr('stroke-width', '2')
      .attr('d', this.line)
  }

  public yMin: any  = 0;
  private lineMinMax(n) {

    var min = this.height - this.height * ((this.arrayForAxis[1].val - this.arrayForAxis[0].val) - (this.arrayForAxis[1].val - this.arrResults[n].min)) / (this.arrayForAxis[1].val - this.arrayForAxis[0].val);
    var max = this.height - this.height * ((this.arrayForAxis[1].val - this.arrayForAxis[0].val) - (this.arrayForAxis[1].val - this.arrResults[n].max)) / (this.arrayForAxis[1].val - this.arrayForAxis[0].val);

    if (this.arrResults[n].min != this.arrResults[n].max) {
      this.chart.append("rect")
        .attr('x', 0)
        .attr('y', max)
        .attr('width', this.width)
        .attr('height', min - max)
        .attr('fill', 'rgba(0, 0, 0, 0.02)');
      this.chart2.append("rect")
        .attr('x', 0)
        .attr('y', max)
        .attr('width', this.width)
        .attr('height', min - max)
        .attr('fill', 'rgba(0, 0, 0, 0.02)');

        this.chart.append("line")                 // линия максимума
          .attr('stroke', 'rgb(255, 0, 0)')
          .attr('stroke-width', '1')
          .attr("x1", 0)
          .attr("y1", max)
          .attr("x2", this.width)
          .attr("y2", max);

        this.chart2.append("line")                 // линия максимума
          .attr('stroke', 'rgb(255, 0, 0)')
          .attr('stroke-width', '1')
          .attr("x1", 0)
          .attr("y1", max)
          .attr("x2", this.width)
          .attr("y2", max);
    }

    this.chart.append("line")                 // линия минимума
      .attr('stroke', 'rgb(0, 255, 0)')
      .attr('stroke-width', '1')
      .attr("x1", 0)
      .attr("y1", min)
      .attr("x2", this.width)
      .attr("y2", min);

    this.chart2.append("line")                 // линия минимума
      .attr('stroke', 'rgb(0, 255, 0)')
      .attr('stroke-width', '1')
      .attr("x1", 0)
      .attr("y1", min)
      .attr("x2", this.width)
      .attr("y2", min);
   }

}

interface ChemResult {
  name: string;
  min: number;
  max: number;
  eu: string;
  shift: number;
  furnance: number;
  values: Tag[];
}

interface Tag {
  name: string;
  dt: string;
  qual: number;
  val: number;
}

