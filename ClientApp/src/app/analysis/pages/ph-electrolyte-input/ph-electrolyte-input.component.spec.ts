import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhElectrolyteInputComponent } from './ph-electrolyte-input.component';

describe('PhElectrolyteInputComponent', () => {
  let component: PhElectrolyteInputComponent;
  let fixture: ComponentFixture<PhElectrolyteInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhElectrolyteInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhElectrolyteInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
