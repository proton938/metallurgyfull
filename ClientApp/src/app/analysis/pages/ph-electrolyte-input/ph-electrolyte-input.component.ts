import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-ph-electrolyte-input',
  templateUrl: './ph-electrolyte-input.component.html',
  styleUrls: ['./ph-electrolyte-input.component.css']
})
export class PhElectrolyteInputComponent implements OnInit {


  http: HttpClient;
  url: string;
  success: boolean = true;
  counter: number = 0;

  tagsPh: any[] = [
    { tagname: 'Zinc_Electrolysis_Shop.Ph_In_Zn_Electrolyte_1', description: 'Поступающий цинковый электролит, 1 серия' },
    { tagname: 'Zinc_Electrolysis_Shop.Ph_In_Zn_Electrolyte_2', description: 'Поступающий цинковый электролит, 2 серия' },
    { tagname: 'Zinc_Electrolysis_Shop.Ph_Waste_Zn_Electrolyte_A', description: 'Отработанный цинковый электролит, ряд A' },
    { tagname: 'Zinc_Electrolysis_Shop.Ph_Waste_Zn_Electrolyte_B', description: 'Отработанный цинковый электролит, ряд B' },
    { tagname: 'Zinc_Electrolysis_Shop.Ph_Waste_Zn_Electrolyte_C', description: 'Отработанный цинковый электролит, ряд C' },
    { tagname: 'Zinc_Electrolysis_Shop.Ph_Waste_Zn_Electrolyte_D', description: 'Отработанный цинковый электролит, ряд D' },
  ];

  tagsZn: any[] = [
    { tagname: 'Zinc_Electrolysis_Shop.Zn_In_Zn_Electrolyte_1', description: 'Поступающий цинковый электролит, 1 серия' },
    { tagname: 'Zinc_Electrolysis_Shop.Zn_In_Zn_Electrolyte_2', description: 'Поступающий цинковый электролит, 2 серия' },
    { tagname: 'Zinc_Electrolysis_Shop.Zn_Waste_Zn_Electrolyte_A', description: 'Отработанный цинковый электролит, ряд A' },
    { tagname: 'Zinc_Electrolysis_Shop.Zn_Waste_Zn_Electrolyte_B', description: 'Отработанный цинковый электролит, ряд B' },
    { tagname: 'Zinc_Electrolysis_Shop.Zn_Waste_Zn_Electrolyte_C', description: 'Отработанный цинковый электролит, ряд C' },
    { tagname: 'Zinc_Electrolysis_Shop.Zn_Waste_Zn_Electrolyte_D', description: 'Отработанный цинковый электролит, ряд D' },
  ];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http = http;
    this.url = baseUrl + 'api/parameters/SetTagValueDT/';
  }


  ngOnInit() {
    this.getNeedTime();
  }

  needTime: any;

  getNeedTime() {
    var that = this;

    var currentTime = new Date();
    this.needTime = Number(currentTime.getHours()) % 2;
    if (this.needTime != 1) {
      if (Number(currentTime.getHours()) > 1) {
        this.needTime = Number(currentTime.getHours()) - 1;
      } else {
        this.needTime = 23;
      }
    } else {
      this.needTime = Number(currentTime.getHours());
    }
    (<HTMLInputElement>document.getElementById('curTime')).value = this.needTime;

    setTimeout(function () {
      that.getNeedTime();
    }, 600000);
  }

  replaceNeedTime() {
    this.needTime = (<HTMLInputElement>document.getElementById('curTime')).value;
  }


  cycleGets() {
    for (let i = 0; i < this.tagsPh.length; i++) {
      this.saveValuePh(this.tagsPh[i].tagname);
    }
    for (let i = 0; i < this.tagsZn.length; i++) {
      this.saveValuePh(this.tagsZn[i].tagname);
    }
    if (this.success == true) {
      alert('Данные успешно сохранены!');
    } else {
      alert('Ошибка сохранения!');
    }
  }



  saveValuePh(tagname) {

    var value = (<HTMLInputElement>document.getElementById(tagname)).value;

    if (value != '') {

      var dt: any = new Date();
      
      this.http.get(this.url + tagname + '/' + value + '/' + dt.getFullYear() + '-' + (dt.getMonth()+1) + '-' + dt.getDate() + ' ' + this.needTime + ':00:00' , {
        responseType: 'blob'
      }).subscribe(result => {
        console.log(result);
        this.success = true;
        this.counter++;
      }, error => {
          console.log(error);
          this.success = false;
      });
    } 
  }



}
