import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PredictiveAnalyticsComponent } from './pages';
import { AnalyticsComponent } from './pages';
import { StkComponent } from './pages';
import { PhElectrolyteInputComponent } from './pages' ;

const routes: Routes = [
  { path: 'predictiveanalytics', component: PredictiveAnalyticsComponent },
  { path: 'predictiveanalytics/:id', component: PredictiveAnalyticsComponent },
  { path: 'analytics', component: AnalyticsComponent },
  { path: 'analytics/:id', component: AnalyticsComponent },
  { path: 'stk', component: StkComponent },
  { path: 'stk/:id', component: StkComponent },
  { path: 'ph-electrolyte-input', component: PhElectrolyteInputComponent },
  { path: 'ph-electrolyte-input/:id', component: PhElectrolyteInputComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalysisRoutingModule { }
