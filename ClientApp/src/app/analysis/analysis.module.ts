import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { AnalysisRoutingModule } from './analysis-routing.module';
import { PredictiveAnalyticsComponent } from './pages/predictive-analytics/predictive-analytics.component';
import { AnalyticsComponent } from './pages/analytics/analytics.component';

import { MatTabsModule } from '@angular/material';
import { GoogleChartsModule } from 'angular-google-charts';

import { FormsModule } from '@angular/forms';
import { StkComponent } from './pages/stk/stk.component';
import { PhElectrolyteInputComponent } from './pages/ph-electrolyte-input/ph-electrolyte-input.component';

@NgModule({
  imports: [
    CommonModule,
    AnalysisRoutingModule,
    MatTabsModule,
    GoogleChartsModule.forRoot('AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'),
    BrowserModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [PredictiveAnalyticsComponent, AnalyticsComponent, StkComponent, PhElectrolyteInputComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AnalysisModule { }
